﻿2000.1.1 = {

	create_field_marshal = {
		name = "Ahmed Gaid Salah"
		picture = "ahmed_gaid_salah.dds"
		traits = { old_guard organisational_leader }
		id = 161
		skill = 3
		attack_skill = 3
		defense_skill = 1
		planning_skill = 2
		logistics_skill = 4
	}
	
	create_corps_commander = { 
		name = "Kaidi Mohamed" 
		picture = "Kaidi_Mohamed.dds"		#WIP-GFX
		traits = { panzer_leader }
		id = 162
		skill = 3
		attack_skill = 1
		defense_skill = 4
		planning_skill = 4
		logistics_skill = 1
	}		

	create_corps_commander = {
		name = "Ahcène Tafer"
		picture = "Portrait_Ahcene_Tafer.dds"
		traits = { panzer_leader desert_fox }
		id = 163
		skill = 3
		attack_skill = 2
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 2
	}

	create_navy_leader = {
		name = "Mohammed Larbi Haouli"
		picture = "Portrait_Mohammed_Larbi_Haouli.dds"
		traits = { superior_tactician }
		id = 164
	}
}