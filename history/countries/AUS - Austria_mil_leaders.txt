﻿2000.1.1 = {

	create_corps_commander = {
		name = "Franz Reissner"
		picture = "Portrait_Franz_Reissner.dds"
		traits = { trickster }
		id = 281
		skill = 5
		attack_skill = 5
		defense_skill = 2
		planning_skill = 5
		logistics_skill = 4
	}

}

2017.1.1 = {

	create_field_marshal = {
		name = "Othmar Commenda"
		picture = "Portrait_Othmar_Commenda.dds"
		traits = { old_guard organisational_leader }
		id = 282
		skill = 5
		attack_skill = 2
		defense_skill = 6
		planning_skill = 5
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "Horst Hofer"
		picture = "Portrait_Horst_Hofer.dds"		#WIP-gfx
		traits = { commando }
		id = 283
		skill = 6
		attack_skill = 5
		defense_skill = 5
		planning_skill = 5
		logistics_skill = 4
	}
	
}