﻿2000.1.1 = {

	create_field_marshal = {
		name = "David Hurley"
		picture = "David_Hurley.dds"		#wip-gfx
		traits = { old_guard thorough_planner }
		id = 261
		skill = 6
		attack_skill = 4
		defense_skill = 5
		planning_skill = 5
		logistics_skill = 5
	}

	create_corps_commander = {
		name = "Simone Wilkie"
		picture = "Portrait_Simone_Wilkie.dds"
		traits = { trait_engineer }
		id = 262
		skill = 5
		attack_skill = 4
		defense_skill = 2
		planning_skill = 5
		logistics_skill = 5
	}

	create_corps_commander = {
		name = "Shane Caughey"
		picture = "Portrait_Shane_Coughey.dds"
		traits = { hill_fighter }
		id = 263
		skill = 5
		attack_skill = 6
		defense_skill = 4
		planning_skill = 3
		logistics_skill = 3
	}
	
	create_navy_leader = {
		name = "Tim Barret"
		picture = "adm_Tim_Barret.dds"
		traits = {  }
		id = 264
	}
	create_navy_leader = {
		name = "Ray Griggs"
		picture = "adm_Ray_Griggs.dds"
		traits = {  }
		id = 265
	}

	create_navy_leader = {
		name = "Ray Griggs"
		picture = "Portrait_Ray_Griggs.dds"
		traits = { old_guard_navy superior_tactician }
		id = 266
	}

	create_navy_leader = {
		name = "Tim Barrett"
		picture = "Portrait_Tim_Barrett.dds"
		traits = { ironside }
		id = 267
	}

	create_navy_leader = {
		name = "Michael Noonan"
		picture = "Portrait_Michael_Noonan.dds"
		traits = { seawolf }
		id = 268
	}

	create_navy_leader = {
		name = "Stuart Mayer"
		picture = "Portrait_Stuart_Mayer.dds"
		traits = { spotter }
		id = 269
	}

	create_navy_leader = {
		name = "David Johnston"
		picture = "Portrait_David_Johnston.dds"
		traits = { air_controller }
		id = 270
	}

	create_navy_leader = {
		name = "Jonathan Mead"
		picture = "Portrait_Jonathan_Mead.dds"
		traits = { fly_swatter }
		id = 271
	}

	create_navy_leader = {
		name = "Michael Uzzell"
		picture = "Portrait_Michael_Uzzell.dds"
		traits = { blockade_runner }
		id = 272
	}

	create_navy_leader = {
		name = "Brett Brace"
		picture = "Portrait_Brett_Brace.dds"
		traits = { spotter }
		id = 273
	}

	create_navy_leader = {
		name = "Bruce Kafer"
		picture = "Portrait_Bruce_Kafer.dds"
		traits = { blockade_runner }
		id = 274
	}

	create_navy_leader = {
		name = "Gary Wight"
		picture = "Portrait_Gary_Wight.dds"
		traits = {  }
		id = 275
	}
}