﻿2000.1.1 = {

	create_corps_commander = {
		name = "Alvin Quintyne"
		picture = "Portrait_Alvin_Quintyne.dds"
		traits = { inspirational_leader }
		id = 381
		skill = 4
		attack_skill = 5
		defense_skill = 1
		planning_skill = 5
		logistics_skill = 2
	}
	
	create_navy_leader = {
		name = "Aquinas Clarke"
		picture = "admirals/Portrait_Aquinas_Clarke.dds"
		traits = { blockade_runner }
		id = 382
	}

	create_navy_leader = {
		name = "Errington Shurland"
		picture = "admirals/Portrait_Errington_Shurland.dds"
		traits = { spotter }
		id = 383
	}

	create_navy_leader = {
		name = "Sean Reece"
		picture = "admirals/Portrait_Sean_Reece.dds"
		traits = { superior_tactician }
		id = 384
	}

	create_navy_leader = {
		name = "David Harewood"
		picture = "admirals/Portrait_David_Harewood.dds"
		traits = { seawolf }
		id = 385
	}
}

2017.1.1 = {

	remove_unit_leader = 381
		
	create_corps_commander = {
		name = "Glyne Grannum"
		picture = "Portrait_Glyne_Grannum.dds"
		traits = { organisational_leader }
		id = 386
		skill = 4
		attack_skill = 4
		defense_skill = 4
		planning_skill = 3
		logistics_skill = 2
	}
	
}