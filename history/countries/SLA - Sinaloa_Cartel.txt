2000.1.1 = {
	capital = 831 # Culiacan
	# oob = "MEX_2000"

	set_convoys = 80

	set_country_flag = enthusiastic_industrial_conglomerates
	set_country_flag = positive_small_medium_business_owners
	set_country_flag = positive_landowners

	add_opinion_modifier = { target = COL modifier = spanish_speaking }
	reverse_add_opinion_modifier = { target = COL modifier = spanish_speaking }
	add_opinion_modifier = { target = ARG modifier = spanish_speaking }
	reverse_add_opinion_modifier = { target = ARG modifier = spanish_speaking }
	add_opinion_modifier = { target = PRU modifier = spanish_speaking }
	reverse_add_opinion_modifier = { target = PRU modifier = spanish_speaking }
	add_opinion_modifier = { target = VEN modifier = spanish_speaking }
	reverse_add_opinion_modifier = { target = VEN modifier = spanish_speaking }
	add_opinion_modifier = { target = CHL modifier = spanish_speaking }
	reverse_add_opinion_modifier = { target = CHL modifier = spanish_speaking }
	add_opinion_modifier = { target = ECU modifier = spanish_speaking }
	reverse_add_opinion_modifier = { target = ECU modifier = spanish_speaking }
	add_opinion_modifier = { target = GUA modifier = spanish_speaking }
	reverse_add_opinion_modifier = { target = GUA modifier = spanish_speaking }
	add_opinion_modifier = { target = BOL modifier = spanish_speaking }
	reverse_add_opinion_modifier = { target = BOL modifier = spanish_speaking }
	add_opinion_modifier = { target = DOM modifier = spanish_speaking }
	reverse_add_opinion_modifier = { target = DOM modifier = spanish_speaking }
	add_opinion_modifier = { target = HON modifier = spanish_speaking }
	reverse_add_opinion_modifier = { target = HON modifier = spanish_speaking }
	add_opinion_modifier = { target = PAR modifier = spanish_speaking }
	reverse_add_opinion_modifier = { target = PAR modifier = spanish_speaking }
	add_opinion_modifier = { target = ELS modifier = spanish_speaking }
	reverse_add_opinion_modifier = { target = ELS modifier = spanish_speaking }
	add_opinion_modifier = { target = NIC modifier = spanish_speaking }
	reverse_add_opinion_modifier = { target = NIC modifier = spanish_speaking }
	add_opinion_modifier = { target = COS modifier = spanish_speaking }
	reverse_add_opinion_modifier = { target = COS modifier = spanish_speaking }
	add_opinion_modifier = { target = CUB modifier = spanish_speaking }
	reverse_add_opinion_modifier = { target = CUB modifier = spanish_speaking }
	add_opinion_modifier = { target = PAN modifier = spanish_speaking }
	reverse_add_opinion_modifier = { target = PAN modifier = spanish_speaking }
	add_opinion_modifier = { target = URG modifier = spanish_speaking }
	reverse_add_opinion_modifier = { target = URG modifier = spanish_speaking }

	# Starting tech
	set_technology = {
		legacy_doctrines = 1
		modern_blitzkrieg = 1
		forward_defense = 1
		encourage_nco_iniative = 1
		air_land_battle = 1

		infantry_weapons = 1

		#HWK_1
		Early_APC = 1
		IFV_1 = 1

		#DN-V Toro
		Rec_tank_0 = 1
		Rec_tank_1 = 1
		Rec_tank_2 = 1

		night_vision_1 = 1
		night_vision_2 = 1

		command_control_equipment = 1
		command_control_equipment1 = 1

		#For templates
		combat_eng_equipment = 1
		Anti_tank_0 = 1
		Heavy_Anti_tank_0 = 1
		artillery_0 = 1
		Anti_Air_0 = 1
		APC_1 = 1
		util_vehicle_0 = 1

		landing_craft = 1

		frigate_1 = 1

		body_armor_1980 = 1

		basic_computing = 1
		integrated_circuit = 1
		computing1 = 1
		decryption1 = 1
		encryption1 = 1

		radar = 1
		internet1 = 1 	#1G
		fuel_silos = 1
	}

	add_ideas = {

	    #GDP
		gdp_5
		#Economic Cycle
		stagnation
        #Corruption
		unrestrained_corruption
        #Faction 1
		industrial_conglomerates
        #Faction 2
		small_medium_business_owners
        #Faction 3
		landowners

        #Bureacracy
		bureau_02
        #Military Spending
		defence_01
        #Internal Security
		police_04
        #Education budget
		edu_03
        #Health budget
		health_02
        #Social spending
		social_03

		#Trade Law
		semi_consumption_economy
        #Conscription Law
		partial_draft_army
        #Women in the military
		volunteer_women
        #Foreign Intervention Law

        #Officer Training
		officer_military_school

        #Other
		christian
		narcos_state

	}

	#Influence system
	init_influence = yes
	set_variable = { domestic_influence_amount = 200 }
	startup_influence = yes

	### Economy
	set_variable = { var = debt value = 418 }
	set_variable = { var = treasury value = 52 }
	set_variable = { var = tax_rate value = 10 }
	#set_variable = { var = int_investments value = ##idk### }
	#initial_money_setup = yes

	set_popularities = {
		democratic = 0
		communism = 0
		fascism = 0
		neutrality = 100
		nationalist = 0
	}
	set_politics = {

		ruling_party = neutrality
		last_election = "1999.7.1"
		election_frequency = 48
		elections_allowed = yes
	}

	start_politics_input = yes

	### Party Popularities
	set_variable = { party_pop_array^0 = 0 } #Western_Autocracy
	set_variable = { party_pop_array^1 = 0 } #conservatism
	set_variable = { party_pop_array^2 = 0 } #liberalism
	set_variable = { party_pop_array^3 = 0 } #socialism
	set_variable = { party_pop_array^4 = 0 } #Communist-State
	set_variable = { party_pop_array^5 = 0 } #anarchist_communism
	set_variable = { party_pop_array^6 = 0 } #Conservative
	set_variable = { party_pop_array^7 = 0 } #Autocracy
	set_variable = { party_pop_array^8 = 0 } #Mod_Vilayat_e_Faqih
	set_variable = { party_pop_array^9 = 0 } #Vilayat_e_Faqih
	set_variable = { party_pop_array^10 = 0 } #Kingdom
	set_variable = { party_pop_array^11 = 0 } #Caliphate
	set_variable = { party_pop_array^12 = 0 } #Neutral_Muslim_Brotherhood
	set_variable = { party_pop_array^13 = 0 } #Neutral_Autocracy
	set_variable = { party_pop_array^14 = 0 } #Neutral_conservatism
	set_variable = { party_pop_array^15 = 1 } #oligarchism
	set_variable = { party_pop_array^16 = 0 } #Neutral_Libertarian
	set_variable = { party_pop_array^17 = 0 } #Neutral_green
	set_variable = { party_pop_array^18 = 0 } #neutral_Social
	set_variable = { party_pop_array^19 = 0 } #Neutral_Communism
	set_variable = { party_pop_array^20 = 0 } #Nat_Populism
	set_variable = { party_pop_array^21 = 0 } #Nat_Fascism
	set_variable = { party_pop_array^22 = 0 } #Nat_Autocracy
	set_variable = { party_pop_array^23 = 0 } #Monarchist

	### Ruling Party
	add_to_array = { ruling_party = 15 }

	startup_politics = yes

	create_country_leader = {
		name = "Joaquín Guzmán"
		picture = "El_Chapo.dds"
		ideology = oligarchism
		traits = {
			legendary_guerrilla_leader
		}
	}
}