﻿2000.1.1 = {

	create_corps_commander = {
		name = "Gilbert Diendere"
		picture = "Portrait_Gilbert_Diendere_army.dds"
		traits = { old_guard offensive_doctrine }
		id = 441
		skill = 2
		attack_skill = 1
		defense_skill = 1
		planning_skill = 3
		logistics_skill = 2
	}
	
}