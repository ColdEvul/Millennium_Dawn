﻿2000.1.1 = {
	
	create_corps_commander = {
		name = "Laurent Amoussou"
		picture = "Portrait_Laurent_Amoussou.dds"
		traits = {  }
		id = 421
		skill = 2
		attack_skill = 1
		defense_skill = 2
		planning_skill = 1
		logistics_skill = 3
	}

	create_navy_leader = {
		name = "Albert Badou"
		picture = "Portrait_Albert_Badou.dds"
		traits = {  }
		id = 422
	}
	
}