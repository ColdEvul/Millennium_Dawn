capital = 840
2000.1.1 = {
	capital = 840
	set_country_flag = dynamic_flag
	add_ideas = {
		#GDP
		gdp_5
		#Economic Cycle
		stagnation
		#Corruption
		unrestrained_corruption
		#Internal Factions
		industrial_conglomerates
		small_medium_business_owners
		landowners

		#Laws
		bureau_02
		defence_01
		police_04
		edu_03
		health_02
		social_03

		#Other Misc Laws
		semi_consumption_economy
		partial_draft_army
		volunteer_women
		officer_military_school
		christian
	}

	#Influence
	init_influence = yes
	set_variable = { domestic_influence_amount = 95 }
	add_to_array = { influence_array = MEX.id }
	add_to_array = { influence_array_val = 5 }
	startup_influence = yes

	#Economy
	set_variable = { var = treasury value = 2 }
	set_variable = { var = treasury value = 10 }

	#Politics
	set_popularities = {
		democratic = 70.0
		communism = 10.0
		fascism = 0.0
		neutrality = 10.0
		nationalist = 10.0
	}
	set_politics = {
		ruling_party =democratic
		last_election = "1999.7.1"
		election_frequency = 48
		elections_allowed = yes
	}
	start_politics_input = yes

	##Party Popularities
	start_politics_input = yes
	set_variable = { party_pop_array^1 = 0.10 }
	set_variable = { party_pop_array^2 = 0.15 }
	set_variable = { party_pop_array^3 = 0.10 }
	set_variable = { party_pop_array^4 = 0.10 }
	set_variable = { party_pop_array^16 = 0.10 }
	set_variable = { party_pop_array^20 = 0.10 }

	add_to_array = { ruling_party = 2 } #Liberalism
	startup_politics = yes
}
2017.1.1 = {
	set_technology = {
		legacy_doctrines = 1
		modern_blitzkrieg = 1
		forward_defense = 1
		encourage_nco_iniative = 1
		air_land_battle = 1

		#FX-08
		infantry_weapons = 1
		infantry_weapons1 = 1
		infantry_weapons2 = 1
		infantry_weapons3 = 1
		infantry_weapons4 = 1
		support_weapons = 1
		support_weapons2 = 1
		tandem_charge_warheads = 1
		squad_automatic_weapon = 1
		support_weapons3 = 1
		special_forces = 1
		#HWK_1
		Early_APC = 1
		IFV_1 = 1

		#DN-V Toro
		Rec_tank_0 = 1
		Rec_tank_1 = 1
		Rec_tank_2 = 1

		night_vision_1 = 1
		night_vision_2 = 1
		night_vision_3 = 1

		command_control_equipment = 1
		command_control_equipment1 = 1
		command_control_equipment2 = 1

		#For templates
		combat_eng_equipment = 1
		Anti_tank_0 = 1
		Heavy_Anti_tank_0 = 1
		artillery_0 = 1
		Anti_Air_0 = 1
		APC_1 = 1
		util_vehicle_0 = 1

		landing_craft = 1

		frigate_1 = 1

		body_armor_2000 = 1

		microprocessors = 1
		computing2 = 1
		decryption2 = 1
		encryption2 = 1
		computing3 = 1
		decryption3 = 1
		encryption3 = 1
		computing4 = 1
		decryption4 = 1
		encryption4 = 1
		neural_networks_revival = 1
		data_mining = 1
		industrial_electrospun_polymeric_nanofibers = 1
		stereolitography = 1
		DNA_fingerprinting = 1
		modern_gmo = 1
		gene_therapy = 1

		internet2 = 1 	#2G
		gprs = 1
		wifi = 1
		internet3 = 1	#3G
		edge = 1
		umts = 1
		hsupa = 1
		internet4 = 1	#4G
		lte = 1

		construction1 = 1
		construction2 = 1
		excavation1 = 1
		excavation2 = 1
	}
}