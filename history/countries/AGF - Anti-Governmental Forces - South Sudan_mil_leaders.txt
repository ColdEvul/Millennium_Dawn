﻿2017.1.1 = {

	create_field_marshal = {
		name = "Gabriel 'Long Pipe' Tang"
		picture = "Gabriel_Tang.dds"	#WIP-GFX
		traits = { old_guard }
		id = 81
		skill = 2
		attack_skill = 1
		defense_skill = 2
		planning_skill = 1
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "Peter Gadet"
		picture = "SSU_Peter_Gadet.dds"
		id = 82
		skill = 2
		attack_skill = 2
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "David Yau Yau"
		picture = "David_Yau_Yau.dds"	#WIP-GFX
		id = 83
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}
	
}