﻿2000.1.1 = {

	create_field_marshal = {
		name = "Hubert de Vos"
		picture = "Portrait_Hubert_de_Vos.dds"
		traits = {  }
		id = 401
		skill = 5
		attack_skill = 4
		defense_skill = 6
		planning_skill = 3
		logistics_skill = 3
	}
	
	create_corps_commander = {
		name = "Pierre Neirinckx"
		picture = "Portrait_Pierre_Neirinckx.dds"
		traits = {  }
		id = 402
		skill = 5
		attack_skill = 3
		defense_skill = 6
		planning_skill = 5
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Eddy Testelmans"
		picture = "Portrait_Eddy_Testelmans.dds"
		traits = {  }
		id = 403
		skill = 5
		attack_skill = 5
		defense_skill = 4
		planning_skill = 4
		logistics_skill = 3
	}
	
	create_navy_leader = {
		name = "M. Hoffmans"
		picture = "M_Hoffmans.dds"
		id = 404
	}
	create_navy_leader = {
		name = "Georges Heerlen"
		picture = "Portrait_Georges_Heerlen.dds"
		traits = { blockade_runner }
		id = 405
	}

}

2017.1.1 = {
	
	create_field_marshal = {
		name = "Marc Compernol"
		picture = "Portrait_Marc_Compernol.dds"
		traits = { old_guard thorough_planner }
		id = 406
		skill = 5
		attack_skill = 5
		defense_skill = 5
		planning_skill = 5
		logistics_skill = 1
	}
	
	create_navy_leader = {
		name = "Wim Robberecht"
		picture = "Portrait_Wim_Robberecht.dds"
		traits = { old_guard_navy superior_tactician }
		id = 407
	}
}