﻿2000.1.1 = {
	set_country_flag = dynamic_flag
	set_country_flag = dynamic_rebel_flag
	
	add_ideas = {
        #GDP
        gdp_5
        #Economic Cycle
        depression
        #Corruption
        unrestrained_corruption
        #Faction 1
        international_bankers
        #Faction 2
        small_medium_business_owners
        #Faction 3
        landowners

        #Bureacracy
        bureau_03
        #Military Spending
        defence_01
        #Internal Security
        police_05
        #Education budget
        edu_04
        #Health budget
        health_03
        #Social spending
        social_04

        #Trade Law
        export_economy
        #Conscription Law

        #Women in the military
        volunteer_army
        #Foreign Intervention Law
        volunteer_women
        #Officer Training
        officer_military_school

        #Other
		christian
		rio_pact_member
		Major_Non_NATO_Ally
		cartels_2

	}

	capital = 453
	oob = "ARG_2000"
	set_cosmetic_tag = ARG
	if = {
		limit = {
			has_dlc = "Man the Guns"
		}
		set_naval_oob = "ARG_2000_naval_mtg"
		else = {
			set_naval_oob = "ARG_2000_naval_legacy"
		}
	}
	set_convoys = 50



	#Influence system
	init_influence = yes
	set_variable = { domestic_influence_amount = 200 }
	add_to_array = { influence_array = BRA.id }
	add_to_array = { influence_array_val = 50 }
	add_to_array = { influence_array = USA.id }
	add_to_array = { influence_array_val = 45 }
	add_to_array = { influence_array = CHI.id }
	add_to_array = { influence_array_val = 15 }
	startup_influence = yes

	set_technology = {
		legacy_doctrines = 1
		modern_blitzkrieg = 1
		forward_defense = 1
		encourage_nco_iniative = 1
		air_land_battle = 1

		#FARA 83
		infantry_weapons = 1
		infantry_weapons1 = 1
		infantry_weapons2 = 1

		night_vision_1 = 1
		night_vision_2 = 1
		night_vision_3 = 1

		support_weapons = 1
		support_weapons2 = 1
		squad_automatic_weapon = 1
		tandem_charge_warheads = 1
		special_forces = 1
		special_forces2 = 1

		#CITER 155mm L33
		artillery_0 = 1
		Arty_upgrade_1 = 1

		#Cañón 155mm L45 CALA 30
		artillery_1 = 1

		#Argentinian mortars

		#TAM
		MBT_1 = 1
		MBT_2 = 1
		MBT_3 = 1

		#VCTM
		SP_arty_0 = 1
		SP_arty_1 = 1

		#Pampero
		SP_R_arty_0 = 1
		SP_R_arty_1 = 1

		Rec_tank_0 = 1

		util_vehicle_0 = 1
		util_vehicle_1 = 1
		util_vehicle_2 = 1
		util_vehicle_3 = 1

		#Lipán M3
		land_Drone_equipment = 1
		land_Drone_equipment1 = 1

		#AMC VCI
		Early_APC = 1
		APC_1 = 1

		#VCTP
		IFV_1 = 1
		IFV_2 = 1
		IFV_3 = 1

		#A-4AR Fightinhawk
		early_fighter = 1
		Strike_fighter1 = 1
		Strike_fighter2 = 1

		#IA 58 Pucara
		L_Strike_fighter1 = 1
		L_Strike_fighter2 = 1

		#Azopardo-Class
		frigate_1 = 1

		#Espora Class
		corvette_1 = 1
		corvette_2 = 1
		missile_corvette_1 = 1

		#Malvinas
		missile_corvette_2 = 1
		missile_corvette_3 = 1

		#For templates
		combat_eng_equipment = 1
		command_control_equipment = 1
		command_control_equipment1 = 1
		command_control_equipment2 = 1
		Anti_tank_0 = 1
		Heavy_Anti_tank_0 = 1
		Anti_Air_0 = 1
		SP_Anti_Air_0 = 1


		landing_craft = 1

		destroyer_1 = 1
		diesel_attack_submarine_1 = 1

		body_armor_1980 = 1
		camouflage = 1
		camouflage2 = 1

		basic_computing = 1
		integrated_circuit = 1
		computing1 = 1
		decryption1 = 1
		encryption1 = 1
		radar = 1
		fuel_silos = 1
	}

	set_variable = { var = debt value = 190 }
	set_variable = { var = int_investments value = 0 }
	set_variable = { var = treasury value = 37 }
	set_variable = { var = tax_rate value = 15 }
	#initial_money_setup = yes


	add_opinion_modifier = { target = PRU modifier = spanish_speaking }
	reverse_add_opinion_modifier = { target = PRU modifier = spanish_speaking }
	add_opinion_modifier = { target = VEN modifier = spanish_speaking }
	reverse_add_opinion_modifier = { target = VEN modifier = spanish_speaking }
	add_opinion_modifier = { target = CHL modifier = spanish_speaking }
	reverse_add_opinion_modifier = { target = CHL modifier = spanish_speaking }
	add_opinion_modifier = { target = ECU modifier = spanish_speaking }
	reverse_add_opinion_modifier = { target = ECU modifier = spanish_speaking }
	add_opinion_modifier = { target = GUA modifier = spanish_speaking }
	reverse_add_opinion_modifier = { target = GUA modifier = spanish_speaking }
	add_opinion_modifier = { target = BOL modifier = spanish_speaking }
	reverse_add_opinion_modifier = { target = BOL modifier = spanish_speaking }
	add_opinion_modifier = { target = DOM modifier = spanish_speaking }
	reverse_add_opinion_modifier = { target = DOM modifier = spanish_speaking }
	add_opinion_modifier = { target = HON modifier = spanish_speaking }
	reverse_add_opinion_modifier = { target = HON modifier = spanish_speaking }
	add_opinion_modifier = { target = PAR modifier = spanish_speaking }
	reverse_add_opinion_modifier = { target = PAR modifier = spanish_speaking }
	add_opinion_modifier = { target = ELS modifier = spanish_speaking }
	reverse_add_opinion_modifier = { target = ELS modifier = spanish_speaking }
	add_opinion_modifier = { target = NIC modifier = spanish_speaking }
	reverse_add_opinion_modifier = { target = NIC modifier = spanish_speaking }
	add_opinion_modifier = { target = COS modifier = spanish_speaking }
	reverse_add_opinion_modifier = { target = COS modifier = spanish_speaking }
	add_opinion_modifier = { target = CUB modifier = spanish_speaking }
	reverse_add_opinion_modifier = { target = CUB modifier = spanish_speaking }
	add_opinion_modifier = { target = PAN modifier = spanish_speaking }
	reverse_add_opinion_modifier = { target = PAN modifier = spanish_speaking }
	add_opinion_modifier = { target = URG modifier = spanish_speaking }
	reverse_add_opinion_modifier = { target = URG modifier = spanish_speaking }

	set_popularities = {
		democratic = 63.0
		communism = 2.0
		fascism = 0.0
		neutrality = 35.0
		nationalist = 0.0
	}
	set_politics = {
		ruling_party = democratic
		last_election = "1999.10.24"
		election_frequency = 48
		elections_allowed = yes
	}

	start_politics_input = yes

	set_variable = { party_pop_array^0 = 0 } #Western_Autocracy
	set_variable = { party_pop_array^1 = 0.14 } #conservatism
	set_variable = { party_pop_array^2 = 0.14 } #liberalism
	set_variable = { party_pop_array^3 = 0.35 } #socialism
	set_variable = { party_pop_array^4 = 0.005 } #Communist-State
	set_variable = { party_pop_array^5 = 0.01 } #anarchist_communism
	set_variable = { party_pop_array^6 = 0 } #Conservative
	set_variable = { party_pop_array^7 = 0.005 } #Autocracy
	set_variable = { party_pop_array^8 = 0 } #Mod_Vilayat_e_Faqih
	set_variable = { party_pop_array^9 = 0 } #Vilayat_e_Faqih
	set_variable = { party_pop_array^10 = 0 } #Kingdom
	set_variable = { party_pop_array^11 = 0 } #Caliphate
	set_variable = { party_pop_array^12 = 0 } #Neutral_Muslim_Brotherhood
	set_variable = { party_pop_array^13 = 0.34 } #Neutral_Autocracy
	set_variable = { party_pop_array^14 = 0 } #Neutral_conservatism
	set_variable = { party_pop_array^15 = 0 } #oligarchism
	set_variable = { party_pop_array^16 = 0 } #Neutral_Libertarian
	set_variable = { party_pop_array^17 = 0 } #Neutral_green
	set_variable = { party_pop_array^18 = 0.005 } #neutral_Social
	set_variable = { party_pop_array^19 = 0.005 } #Neutral_Communism
	set_variable = { party_pop_array^20 = 0 } #Nat_Populism
	set_variable = { party_pop_array^21 = 0 } #Nat_Fascism
	set_variable = { party_pop_array^22 = 0 } #Nat_Autocracy
	set_variable = { party_pop_array^23 = 0 } #Monarchist
	add_to_array = { ruling_party = 3 }
	startup_politics = yes

	set_variable = { election_threshold = 0.03 }


	create_country_leader = {
		name = "Fernando de la Rua"
		picture = "Fernando_de_la_Rua.dds"
		ideology = socialism
		traits = {
			western_socialism
		}
	}
}

2017.1.1 = {
	capital = 453
	oob = "ARG_2017"
	if = {
		limit = {
			has_dlc = "Man the Guns"
		}
		set_naval_oob = "ARG_2017_naval_mtg"
		else = {
			set_naval_oob = "ARG_2017_naval_legacy"
		}
	}


	remove_ideas = {
		gdp_5
		#tax_cost_15
		depression
	}

	add_ideas = {
		stable_growth
		gdp_7
		#tax_cost_31
	}

	#Influence system
	init_influence = yes
	set_variable = { domestic_influence_amount = 200 }
	add_to_array = { influence_array = BRA.id }
	add_to_array = { influence_array_val = 50 }
	add_to_array = { influence_array = USA.id }
	add_to_array = { influence_array_val = 35 }
	add_to_array = { influence_array = CHI.id }
	add_to_array = { influence_array_val = 28 }
	startup_influence = yes

	#set_country_flag = gdp_6
	set_country_flag = Major_Non_NATO_Ally
	set_country_flag = positive_international_bankers
	set_country_flag = positive_small_medium_business_owners
	set_country_flag = positive_landowners

	set_variable = { var = debt value = 335 }
	set_variable = { var = int_investments value = 0 }
	set_variable = { var = treasury value = 38 }
	set_variable = { var = tax_rate value = 31 }

	#set_variable = { var = size_modifier value = 3.38 } #18 CIC
	#initial_money_setup = yes

	#Nat focus







	# Starting tech
	set_technology = {

		#Patagon
		Rec_tank_1 = 1

		#VLEGA Gaucho

		util_vehicle_4 = 1

		body_armor_2000 = 1

		microprocessors = 1
		computing2 = 1
		decryption2 = 1
		encryption2 = 1
		computing3 = 1
		decryption3 = 1
		encryption3 = 1
		computing4 = 1
		decryption4 = 1
		encryption4 = 1
		neural_networks_revival = 1
		data_mining = 1
		industrial_electrospun_polymeric_nanofibers = 1
		stereolitography = 1
		DNA_fingerprinting = 1
		modern_gmo = 1
		gene_therapy = 1
		internet1 = 1 	#1G
	}

	set_popularities = {
		democratic = 41.0
		communism = 24.0
		fascism = 0.0
		neutrality = 35.0
		nationalist = 0.0
	}
	set_politics = {
		ruling_party = democratic
		last_election = "2015.10.25"
		election_frequency = 48
		elections_allowed = yes
	}

	start_politics_input = yes

	set_variable = { party_pop_array^0 = 0 } #Western_Autocracy
	set_variable = { party_pop_array^1 = 0.21 } #conservatism
	set_variable = { party_pop_array^2 = 0.04 } #liberalism
	set_variable = { party_pop_array^3 = 0.16 } #socialism
	set_variable = { party_pop_array^4 = 0 } #Communist-State
	set_variable = { party_pop_array^5 = 0.01 } #anarchist_communism
	set_variable = { party_pop_array^6 = 0 } #Conservative
	set_variable = { party_pop_array^7 = 0.23 } #Autocracy
	set_variable = { party_pop_array^8 = 0 } #Mod_Vilayat_e_Faqih
	set_variable = { party_pop_array^9 = 0 } #Vilayat_e_Faqih
	set_variable = { party_pop_array^10 = 0 } #Kingdom
	set_variable = { party_pop_array^11 = 0 } #Caliphate
	set_variable = { party_pop_array^12 = 0 } #Neutral_Muslim_Brotherhood
	set_variable = { party_pop_array^13 = 0.34 } #Neutral_Autocracy
	set_variable = { party_pop_array^14 = 0 } #Neutral_conservatism
	set_variable = { party_pop_array^15 = 0 } #oligarchism
	set_variable = { party_pop_array^16 = 0 } #Neutral_Libertarian
	set_variable = { party_pop_array^17 = 0 } #Neutral_green
	set_variable = { party_pop_array^18 = 0.01 } #neutral_Social
	set_variable = { party_pop_array^19 = 0 } #Neutral_Communism
	set_variable = { party_pop_array^20 = 0 } #Nat_Populism
	set_variable = { party_pop_array^21 = 0 } #Nat_Fascism
	set_variable = { party_pop_array^22 = 0 } #Nat_Autocracy
	set_variable = { party_pop_array^23 = 0 } #Monarchist
	add_to_array = { ruling_party = 1 }
	startup_politics = yes

	create_country_leader = {
		name = "Mauricio Macri"
		picture = "ARG_Mauricio_Macri.dds"
		ideology = conservatism
		traits = {
			western_conservatism
		}
	}
}