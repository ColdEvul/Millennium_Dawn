
state = {
	id = 57
	name = "STATE_57"
	
	resources = {
		steel = 12
	}

	history = {
		owner = FRA
		victory_points = {
			678 1 
		}
		victory_points = {
			11488 1 
		}
		victory_points = {
			9559 2
		}
		victory_points = {
			11502 5
		}
		victory_points = {
			9503 5 
		}
		buildings = {
			infrastructure = 6
			internet_station = 2
			industrial_complex = 2
			synthetic_refinery = 1
			air_base = 5

		}
		add_core_of = FRA
		2017.1.1 = {
			buildings = {
				internet_station = 4
			}
			add_manpower = 166546

		}

	}

	provinces = {
		521 678 3629 6529 9503 9559 11502 
	}
	manpower = 2757592
	buildings_max_level_factor = 1.000
	state_category = state_04
}
