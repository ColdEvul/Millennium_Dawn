
state = {
	id = 979
	name = "STATE_979"

	history = {
		owner = PER
		victory_points = {
			1636 1
		}
		victory_points = {
			1822 1
		}
		victory_points = {
			7604 5
		}
		buildings = {
			infrastructure = 2
			internet_station = 0
			arms_factory = 1
		}
		add_core_of = PER
		add_core_of = KUR
		2017.1.1 = {
			add_manpower = 754009
		}

	}

	provinces = {
		1636 1822 4618 6070 7604
	}
	manpower = 2992103
	buildings_max_level_factor = 1.000
	state_category = state_04
}
