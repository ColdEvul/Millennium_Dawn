
state = {
	id = 404
	name = "STATE_404"

	history = {
		owner = PER
		victory_points = {
			1982 4
		}
		victory_points = {
			8040 5
		}
		victory_points = {
			9074 1 
		}
		victory_points = {
			10922 5
		}
		buildings = {
			infrastructure = 4
			internet_station = 1
			arms_factory = 1
			industrial_complex = 1

		}
		add_core_of = PER
		2017.1.1 = {
			add_manpower = 1010042
			buildings = {
				internet_station = 3
				arms_factory = 1
				industrial_complex = 1
			}

		}

	}

	provinces = {
		1982 3988 8040 9074 9816 10922
	}
	manpower = 4008111
	buildings_max_level_factor = 1.000
	state_category = state_05
}
