
state = {
	id = 977
	name = "STATE_977"
	resources = {
	}

	history = {
		owner = PER
		buildings = {
			infrastructure = 4
			internet_station = 1
			industrial_complex = 1

		}
		add_core_of = PER
		2017.1.1 = {
			add_manpower = 2068391
			buildings = {
				internet_station = 3
				industrial_complex = 1
				arms_factory = 2
			}
		}

	}

	provinces = {
		1963 3870 3952 3982 8031 10759 10828 10873 10913 11249 12809 12852
	}
	manpower = 8091521
	buildings_max_level_factor = 1.000
	state_category = state_07
}
