﻿units = {

	### Naval OOB ###
	fleet = {
	   name = "Armada uruguaya"
	   naval_base = 10362
		task_force = {
			name = "Armada uruguaya"
			location = 10362
			ship = { name = "Uruguay" definition = frigate experience = 30 equipment = { frigate_2 = { amount = 1 owner = URG creator = POR } } }
			ship = { name = "Cte. Pedro Campbell" definition = frigate experience = 30 equipment = { frigate_2 = { amount = 1 owner = URG creator = POR } } }
		}
	}
}
