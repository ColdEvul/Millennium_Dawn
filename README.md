Welcome to Millennium Dawn's Gitlabs.

If your new visit the [WIKi] (https://gitlab.com/truemikesmith/Modern_Day_4_HOI/wikis/home) for tutorials & resources.

---



People who join the project, but who are totally inactive will be removed.  

Inactive profiles of people who have made contributions before will be kept for a long time, because they are always welcome to return.

This is not a full 'rules' for the mod project. Hopefully we never have to actually spell them out. Just use common sense and decency and all will be fine.

