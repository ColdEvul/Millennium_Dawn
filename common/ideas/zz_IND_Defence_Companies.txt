ideas = {

	Vehicle_Company = {
	
		designer = yes
		
		IND_pt_pindad_vehicle_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IND_pt_pindad_vehicle_company" }
		
			picture = PT_Pindad_IND
			

			available = {
				OR = {
					original_tag = IND
					controls_state = 631
				}
			}
			visible = {
				OR = {
					original_tag = IND
					# controls_state = 631
					AND = {
						controls_state = 631
						has_better_than_AFV_5 = no
					}
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_AFV = 0.155
			}
			
			traits = {
				Cat_AFV_5
			
			}
			ai_will_do = {
				factor = 0.5 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 10 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_AFV_5 = yes
					factor = 0
				}
				modifier = {
					is_researching_afv = yes
					factor = 4000
				}
			}
			
		}
	}
	
	Infantry_Weapon_Company = {
	
		designer = yes
		
		IND_pt_pindad_infantry_weapon_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IND_pt_pindad_infantry_weapon_company" }
		
			picture = PT_Pindad_IND
			

			available = {
				OR = {
					original_tag = IND
					controls_state = 631
				}
			}
			visible = {
				OR = {
					original_tag = IND
					# controls_state = 631
					AND = {
						controls_state = 631
						has_better_than_INF_WEP_5 = no
					}
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_INF_WEP = 0.155
			}
			
			traits = {
				Cat_INF_WEP_5
			
			}
			ai_will_do = {
				factor = 0.5 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 5 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_INF_WEP_5 = yes
					factor = 0
				}
				modifier = {
					is_researching_infantry_weapons = yes
					factor = 4000
				}
			}
			
		}
	}
	
	Aircraft_Company = {
	
		designer = yes
		
		IND_indonesian_aerospace_aircraft_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IND_indonesian_aerospace_aircraft_company" }
		
			picture = Indonesian_Aerospace_IND
			

			available = {
				OR = {
					original_tag = IND
					controls_state = 631
				}
			}
			visible = {
				OR = {
					original_tag = IND
					# controls_state = 631
					AND = {
						controls_state = 631
						has_better_than_FIXED_WING_2 = no
					}
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				CAT_FIXED_WING = 0.062
			}
			
			traits = {
				CAT_FIXED_WING_2
			
			}
			ai_will_do = {
				factor = 0.2 #Most countries don't have decent airforces
				
				modifier = {
					or = {
						has_tech = AS_Fighter2 #has semi-modern tech
						has_tech = MR_Fighter2
						has_tech = Strike_fighter2
						has_tech = L_Strike_fighter2
						has_tech = Air_UAV1
					}
					factor = 1
				}
				modifier = {
					or = {
						has_tech = strategic_bomber3 #has semi-modern tech, most countries dont have it
						has_tech = transport_plane2
						has_tech = naval_plane3
						has_tech = cas2
					}
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_FIXED_WING_2 = yes
					factor = 0
				}
				modifier = {
					is_researching_fixed_wing = yes
					factor = 1000
				}
				modifier = {
					AND = {
						is_researching_fighters = yes
						has_FIGHTER_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_heavy_air = yes
						has_H_AIR_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_light_fighter = yes
						has_L_Fighter_designer = yes
					}
					factor = 0
				}
			}
			
		}
	}
	
	Helicopter_Company = {
	
		designer = yes
		
		IND_indonesian_aerospace_helicopter_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea IND_indonesian_aerospace_helicopter_company" }
		
			picture = Indonesian_Aerospace_IND
			

			available = {
				OR = {
					original_tag = IND
					controls_state = 631
				}
			}
			visible = {
				OR = {
					original_tag = IND
					# controls_state = 631
					AND = {
						controls_state = 631
						has_better_than_HELI_2 = no
					}
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_HELI = 0.062
			}
			
			traits = {
				Cat_HELI_2
			
			}
			ai_will_do = {
				factor = 0.2 #Most countries don't have decent airforces
				
				modifier = {
					has_tech = attack_helicopter2 #has semi-modern tech, most countries dont have it
					factor = 1
				}
				modifier = {
					has_tech = transport_helicopter2 #has semi-modern tech, most countries dont have it
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_HELI_2 = yes
					factor = 0
				}
				modifier = {
					is_researching_helicopters = yes
					factor = 4000
				}
			}
			
		}
	}
	
}
