ideas = {

	Vehicle_Company = {
	
		designer = yes
		
		GEO_tbilisi_aircraft_manufacturing_vehicle_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea GEO_tbilisi_aircraft_manufacturing_vehicle_company" }
		
			picture = Tbilisi_Aircraft_Manufacturing_GEO
			

			available = {
				OR = {
					original_tag = GEO
					controls_state = 708
				}
			}
			visible = {
				OR = {
					original_tag = GEO
					# controls_state = 708
					AND = {
						controls_state = 708
						has_better_than_ARMOR_3 = no
					}
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_ARMOR = 0.093
			}
			
			traits = {
				Cat_ARMOR_3
			
			}
			ai_will_do = {
				factor = 0.3 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 10 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_ARMOR_3 = yes
					factor = 0
				}
				modifier = {
					is_researching_armor = yes
					factor = 1000
				}
				modifier = {
					AND = {
						is_researching_afv = yes
						has_AFV_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_tanks = yes
						has_TANKS_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_artillery = yes
						has_ARTILLERY_designer = yes
					}
					factor = 0
				}
			}
			
		}
	}
	
	Infantry_Weapon_Company = {
	
		designer = yes
		
		GEO_tbilisi_aircraft_manufacturing_infantry_weapon_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea GEO_tbilisi_aircraft_manufacturing_infantry_weapon_company" }
		
			picture = Tbilisi_Aircraft_Manufacturing_GEO
			

			available = {
				OR = {
					original_tag = GEO
					controls_state = 708
				}
			}
			visible = {
				OR = {
					original_tag = GEO
					# controls_state = 708
					AND = {
						controls_state = 708
						has_better_than_INF_WEP_3 = no
					}
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_INF_WEP = 0.093
			}
			
			traits = {
				Cat_INF_WEP_3
			
			}
			ai_will_do = {
				factor = 0.3 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 5 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_INF_WEP_3 = yes
					factor = 0
				}
				modifier = {
					is_researching_infantry_weapons = yes
					factor = 4000
				}
			}
			
		}
	}
	
}
