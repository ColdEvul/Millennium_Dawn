ideas = {

	Infantry_Weapon_Company = {

		designer = yes

		UAE_caracal_infantry_weapon_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea UAE_caracal_infantry_weapon_company" }

			picture = KMDB_UKR


			available = {
				OR = {
					tag = UAE
					controls_state = 180
				}
			}
			visible = {
				OR = {
					tag = UAE
					controls_state = 180
				}
			}
			cost = 150

			removal_cost = 10

			research_bonus = {
				Cat_INF_WEP = 0.217
			}

			traits = {
				Cat_INF_WEP_7

			}
			ai_will_do = {
				factor = 0.7 #All countries need a land army, vehicles are part of modern warfare

				modifier = {
					num_of_military_factories > 5 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_INF_WEP_7 = yes
					factor = 0
				}
				modifier = {
					is_researching_infantry_weapons = yes
					factor = 4000
				}
			}

		}
	}

	Vehicle_Company = {

		designer = yes

		UAE_emirates_defense_technology_vehicle_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea UAE_emirates_defense_technology_vehicle_company" }

			picture = KMDB_UKR


			available = {
				OR = {
					tag = UAE
					controls_state = 180
				}
			}
			visible = {
				OR = {
					tag = UAE
					controls_state = 180
				}
			}
			cost = 150

			removal_cost = 10

			research_bonus = {
				Cat_ARMOR = 0.217
			}

			traits = {
				Cat_ARMOR_7

			}
			ai_will_do = {
				factor = 0.7 #All countries need a land army, vehicles are part of modern warfare

				modifier = {
					num_of_military_factories > 10 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_ARMOR_7 = yes
					factor = 0
				}
				modifier = {
					is_researching_armor = yes
					factor = 1000
				}
				modifier = {
					AND = {
						is_researching_afv = yes
						has_AFV_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_tanks = yes
						has_TANKS_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_artillery = yes
						has_ARTILLERY_designer = yes
					}
					factor = 0
				}
			}

		}
	}

	Ship_Company = {

		designer = yes

		UAE_abu_dhabi_ship_building_ship_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea UAE_abu_dhabi_ship_building_ship_company" }

			picture = Nikolayev_Shipyard_UKR


			available = {
				OR = {
					tag = UAE
					controls_state = 180
				}
			}
			visible = {
				OR = {
					tag = UAE
					controls_state = 180
				}
			}
			cost = 150

			removal_cost = 10

			research_bonus = {
				Cat_NAVAL_EQP = 0.246
			}

			traits = {
				Cat_NAVAL_EQP_8
			}
			ai_will_do = {
				factor = 0.6

				modifier = {
					has_navy_size = { size > 25 } #has a large navy
					factor = 1
				}
				modifier = {
					num_of_naval_factories > 3 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					NOT = { #need to have ports
						any_owned_state = {
							is_coastal = yes
						}
					}
					factor = 10
				}
				modifier = {
					has_better_than_NAVAL_EQP_8 = yes
					factor = 0
				}
				modifier = {
					is_researching_naval_equipment = yes
					factor = 1000
				}
				modifier = {
					AND = {
						is_researching_carrier = yes
						has_CARRIER_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_surface_ship = yes
						has_SURFACE_SHIP_designer = yes
					}
					factor = 0
				}
			}
		}
	}
}