ideas = {

	Infantry_Weapon_Company = {
	
		designer = yes
		
		PHI_ferfrans_infantry_weapon_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PHI_ferfrans_infantry_weapon_company" }
		
			picture = Ferfrans_PHI
			

			available = {
				OR = {
					tag = PHI
					controls_state = 736
				}
			}
			visible = {
				OR = {
					tag = PHI
					controls_state = 736
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_INF = 0.124
			}
			
			traits = {
				Cat_INF_4
			
			}
			ai_will_do = {
				factor = 0.4 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 5 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_INF_4 = yes
					factor = 0
				}
				modifier = {
					is_researching_infantry_equipment = yes
					factor = 1000
				}
				modifier = {
					AND = {
						is_researching_aa = yes
						has_AA_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_at = yes
						has_AT_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_land_drones = yes
						has_L_DRONE_designer = yes
					}
					factor = 0
				}
			}
			
		}
	}
	
	Infantry_Weapon_Company = {
	
		designer = yes
		
		PHI_floro_international_infantry_weapon_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PHI_floro_international_infantry_weapon_company" }
		
			picture = Floro_International_PHI
			

			available = {
				OR = {
					tag = PHI
					controls_state = 736
				}
			}
			visible = {
				OR = {
					tag = PHI
					controls_state = 736
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_INF_WEP = 0.155
			}
			
			traits = {
				Cat_INF_WEP_5
			
			}
			ai_will_do = {
				factor = 0.5 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 5 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_INF_WEP_5 = yes
					factor = 0
				}
				modifier = {
					is_researching_infantry_weapons = yes
					factor = 4000
				}
			}
			
		}
	}
	
	Aircraft_Company = {
	
		designer = yes
		
		PHI_aviation_composite_technology_aircraft_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PHI_aviation_composite_technology_aircraft_company" }
		
			picture = Aviation_Composite_Technology_PHI
			

			available = {
				OR = {
					tag = PHI
					controls_state = 736
				}
			}
			visible = {
				OR = {
					tag = PHI
					controls_state = 736
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_L_Fighter = 0.124
			}
			
			traits = {
				Cat_L_Fighter_4
			
			}
			ai_will_do = {
				factor = 0.4 #Most countries don't have decent airforces
				
				modifier = {
					or = {
						has_tech = AS_Fighter2 #has semi-modern tech
						has_tech = MR_Fighter2
						has_tech = Strike_fighter2
						has_tech = L_Strike_fighter2
						has_tech = Air_UAV1
					}
					factor = 1
				}
				modifier = {
					or = {
						has_tech = strategic_bomber3 #has semi-modern tech, most countries dont have it
						has_tech = transport_plane2
						has_tech = naval_plane3
						has_tech = cas2
					}
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_L_Fighter_4 = yes
					factor = 0
				}
				modifier = {
					is_researching_light_fighter = yes
					factor = 4000
				}
			}
			
		}
	}
	
}
