ideas = {

	Vehicle_Company = {
	
		designer = yes
		
		BRA_avibras_vehicle_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BRA_avibras_vehicle_company" }
		
			picture = Avibras_BRA
			

			available = {
				OR = {
					original_tag = BRA
					controls_state = 882
				}
			}
			visible = {
				OR = {
					original_tag = BRA
					# controls_state = 882
					AND = {
						controls_state = 882
						has_better_than_ARMOR_7 = no
					}
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_ARMOR = 0.217
			}
			
			traits = {
				Cat_ARMOR_7
			
			}
			ai_will_do = {
				factor = 0.7 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 10 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_ARMOR_7 = yes
					factor = 0
				}
				modifier = {
					is_researching_armor = yes
					factor = 1000
				}
				modifier = {
					AND = {
						is_researching_afv = yes
						has_AFV_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_tanks = yes
						has_TANKS_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_artillery = yes
						has_ARTILLERY_designer = yes
					}
					factor = 0
				}
			}
			
		}
	}
	
	Infantry_Weapon_Company = {
	
		designer = yes
		
		BRA_avibras_infantry_weapon_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BRA_avibras_infantry_weapon_company" }
		
			picture = Avibras_BRA
			

			available = {
				OR = {
					original_tag = BRA
					controls_state = 882
				}
			}
			visible = {
				OR = {
					original_tag = BRA
					# controls_state = 882
					AND = {
						controls_state = 882
						has_better_than_L_DRONE_7 = no
					}
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_L_DRONE = 0.217
			}
			
			traits = {
				Cat_L_DRONE_7
			
			}
			ai_will_do = {
				factor = 0.7 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 5 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_L_DRONE_7 = yes
					factor = 0
				}
				modifier = {
					is_researching_land_drones = yes
					factor = 4000
				}
			}
			
		}
	}
	
	Infantry_Weapon_Company = {
	
		designer = yes
		
		BRA_imbel_infantry_weapon_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BRA_imbel_infantry_weapon_company" }
		
			picture = IMBEL_BRA
			

			available = {
				OR = {
					original_tag = BRA
					controls_state = 882
				}
			}
			visible = {
				OR = {
					original_tag = BRA
					# controls_state = 882
					AND = {
						controls_state = 882
						has_better_than_INF_WEP_6 = no
					}
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_INF_WEP = 0.186
			}
			
			traits = {
				Cat_INF_WEP_6
			
			}
			ai_will_do = {
				factor = 0.6 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 5 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_INF_WEP_6 = yes
					factor = 0
				}
				modifier = {
					is_researching_infantry_weapons = yes
					factor = 4000
				}
			}
			
		}
	}
	
	Helicopter_Company = {
	
		designer = yes
		
		BRA_helibras_helicopter_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BRA_helibras_helicopter_company" }
		
			picture = Helibras_BRA
			

			available = {
				OR = {
					original_tag = BRA
					controls_state = 884
				}
			}
			visible = {
				OR = {
					original_tag = BRA
					# controls_state = 884
					AND = {
						controls_state = 884
						has_better_than_HELI_6 = no
					}
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_HELI = 0.186
			}
			
			traits = {
				Cat_HELI_6
			
			}
			ai_will_do = {
				factor = 0.6 #Most countries don't have decent airforces
				
				modifier = {
					has_tech = attack_helicopter2 #has semi-modern tech, most countries dont have it
					factor = 1
				}
				modifier = {
					has_tech = transport_helicopter2 #has semi-modern tech, most countries dont have it
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_HELI_6 = yes
					factor = 0
				}
				modifier = {
					is_researching_helicopters = yes
					factor = 4000
				}
			}
			
		}
	}
	
	Aircraft_Company = {
	
		designer = yes
		
		BRA_embraer_aircraft_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BRA_embraer_aircraft_company" }
		
			picture = Embraer_BRA
			

			available = {
				OR = {
					original_tag = BRA
					controls_state = 882
				}
			}
			visible = {
				OR = {
					original_tag = BRA
					# controls_state = 882
					AND = {
						controls_state = 882
						has_better_than_FIXED_WING_5 = no
					}
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				CAT_FIXED_WING = 0.155
			}
			
			traits = {
				CAT_FIXED_WING_5
			
			}
			ai_will_do = {
				factor = 0.5 #Most countries don't have decent airforces
				
				modifier = {
					or = {
						has_tech = AS_Fighter2 #has semi-modern tech
						has_tech = MR_Fighter2
						has_tech = Strike_fighter2
						has_tech = L_Strike_fighter2
						has_tech = Air_UAV1
					}
					factor = 1
				}
				modifier = {
					or = {
						has_tech = strategic_bomber3 #has semi-modern tech, most countries dont have it
						has_tech = transport_plane2
						has_tech = naval_plane3
						has_tech = cas2
					}
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_FIXED_WING_5 = yes
					factor = 0
				}
				modifier = {
					is_researching_fixed_wing = yes
					factor = 1000
				}
				modifier = {
					AND = {
						is_researching_fighters = yes
						has_FIGHTER_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_heavy_air = yes
						has_H_AIR_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_light_fighter = yes
						has_L_Fighter_designer = yes
					}
					factor = 0
				}
			}
			
		}
	}
	
	Ship_Company = {
	
		designer = yes
		
		BRA_arsenal_do_marinha_ship_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BRA_arsenal_do_marinha_ship_company" }
		
			picture = Arsenal_do_Marinha_BRA
			

			available = {
				OR = {
					original_tag = BRA
					controls_state = 883
				}
			}
			visible = {
				OR = {
					original_tag = BRA
					# controls_state = 883
					AND = {
						controls_state = 883
						has_better_than_NAVAL_EQP_4 = no
					}
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_NAVAL_EQP = 0.124
			}
			
			traits = {
				Cat_NAVAL_EQP_4
			
			}
			ai_will_do = {
				factor = 0.4
				
				modifier = {
					has_navy_size = { size > 25 } #has a large navy
					factor = 1
				}
				modifier = {
					num_of_naval_factories > 3 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					NOT = { #need to have ports
						any_owned_state = {
							is_coastal = yes
						}
					}
					factor = 10
				}
				modifier = {
					has_better_than_NAVAL_EQP_4 = yes
					factor = 0
				}
				modifier = {
					is_researching_naval_equipment = yes
					factor = 1000
				}
				modifier = {
					AND = {
						is_researching_carrier = yes
						has_CARRIER_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_surface_ship = yes
						has_SURFACE_SHIP_designer = yes
					}
					factor = 0
				}
			}
			
		}
	}
	
}
