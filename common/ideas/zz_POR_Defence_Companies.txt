ideas = {

	Infantry_Weapon_Company = {
	
		designer = yes
		
		POR_fabrica_de_braco_de_prata_infantry_weapon_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea POR_fabrica_de_braco_de_prata_infantry_weapon_company" }
		
			picture = Fabrica_De_Braco_De_Prata_POR
			

			available = {
				OR = {
					tag = POR
					controls_state = 96
				}
			}
			visible = {
				OR = {
					tag = POR
					controls_state = 96
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_INF = 0.155
			}
			
			traits = {
				Cat_INF_5
			}
			ai_will_do = {
				factor = 0.3 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 5 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_INF_5 = yes
					factor = 0
				}
				modifier = {
					is_researching_infantry_equipment = yes
					factor = 1000
				}
				modifier = {
					AND = {
						is_researching_aa = yes
						has_AA_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_at = yes
						has_AT_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_land_drones = yes
						has_L_DRONE_designer = yes
					}
					factor = 0
				}
			}
			
		}
	}
	
	Vehicle_Company = {
	
		designer = yes
		
		POR_bravia_sarl_vehicle_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea POR_bravia_sarl_vehicle_company" }
		
			picture = Bravia_Sarl_POR
			

			available = {
				OR = {
					tag = POR
					controls_state = 96
				}
			}
			visible = {
				OR = {
					tag = POR
					controls_state = 96
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_AFV = 0.155
			}
			
			traits = {
				Cat_AFV_5
			
			}
			ai_will_do = {
				factor = 0.5 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 10 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_AFV_5 = yes
					factor = 0
				}
				modifier = {
					is_researching_afv = yes
					factor = 4000
				}
			}
			
		}
	}
	
}
