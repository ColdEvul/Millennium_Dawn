ideas = {

	Vehicle_Company = {
	
		designer = yes
		
		TUR_fnss_vehicle_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea TUR_fnss_vehicle_company" }
		
			picture = FNSS_TUR
			

			available = {
				OR = {
					tag = TUR
					controls_state = 159
				}
			}
			visible = {
				OR = {
					tag = TUR
					controls_state = 159
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_AFV = 0.124
			}
			
			traits = {
				Cat_AFV_4
			
			}
			ai_will_do = {
				factor = 0.4 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 10 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_AFV_4 = yes
					factor = 0
				}
				modifier = {
					is_researching_afv = yes
					factor = 4000
				}
			}
			
		}
	}
	
	Vehicle_Company = {
	
		designer = yes
		
		TUR_otokar_vehicle_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea TUR_otokar_vehicle_company" }
		
			picture = Otokar_TUR
			

			available = {
				OR = {
					tag = TUR
					controls_state = 159
				}
			}
			visible = {
				OR = {
					tag = TUR
					controls_state = 159
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_ARMOR = 0.155
			}
			
			traits = {
				Cat_ARMOR_5
			
			}
			ai_will_do = {
				factor = 0.5 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 10 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_ARMOR_5 = yes
					factor = 0
				}
				modifier = {
					is_researching_armor = yes
					factor = 1000
				}
				modifier = {
					AND = {
						is_researching_afv = yes
						has_AFV_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_tanks = yes
						has_TANKS_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_artillery = yes
						has_ARTILLERY_designer = yes
					}
					factor = 0
				}
			}
			
		}
	}
	
	Infantry_Weapon_Company = {
	
		designer = yes
		
		TUR_roketsan_infantry_weapon_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea TUR_roketsan_infantry_weapon_company" }
		
			picture = Roketsan_TUR
			

			available = {
				OR = {
					tag = TUR
					controls_state = 159
				}
			}
			visible = {
				OR = {
					tag = TUR
					controls_state = 159
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_AT = 0.186
			}
			
			traits = {
				Cat_AT_6
			
			}
			ai_will_do = {
				factor = 0.6 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 5 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_AT_6 = yes
					factor = 0
				}
				modifier = {
					is_researching_at = yes
					factor = 4000
				}
			}
			
		}
	}
	
	Infantry_Weapon_Company = {
	
		designer = yes
		
		TUR_mkek_infantry_weapon_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea TUR_mkek_infantry_weapon_company" }
		
			picture = MKEK_TUR
			

			available = {
				OR = {
					tag = TUR
					controls_state = 159
				}
			}
			visible = {
				OR = {
					tag = TUR
					controls_state = 159
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_INF_WEP = 0.124
			}
			
			traits = {
				Cat_INF_WEP_4
			
			}
			ai_will_do = {
				factor = 0.4 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 5 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_INF_WEP_4 = yes
					factor = 0
				}
				modifier = {
					is_researching_infantry_weapons = yes
					factor = 4000
				}
			}
			
		}
	}
	
	Vehicle_Company = {
	
		designer = yes
		
		TUR_mkek_vehicle_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea TUR_mkek_vehicle_company" }
		
			picture = MKEK_TUR
			

			available = {
				OR = {
					tag = TUR
					controls_state = 159
				}
			}
			visible = {
				OR = {
					tag = TUR
					controls_state = 159
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_ARTILLERY = 0.124
			}
			
			traits = {
				Cat_ARTILLERY_4
			
			}
			ai_will_do = {
				factor = 0.4 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 10 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_ARTILLERY_4 = yes
					factor = 0
				}
				modifier = {
					is_researching_artillery = yes
					factor = 4000
				}
			}
			
		}
	}
	
	Aircraft_Company = {
	
		designer = yes
		
		TUR_turkish_aerospace_industries_aircraft_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea TUR_turkish_aerospace_industries_aircraft_company" }
		
			picture = Turkish_Aerospace_Industries_TUR
			

			available = {
				OR = {
					tag = TUR
					controls_state = 159
				}
			}
			visible = {
				OR = {
					tag = TUR
					controls_state = 159
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_FIGHTER = 0.124
			}
			
			traits = {
				Cat_FIGHTER_4
			
			}
			ai_will_do = {
				factor = 0.4 #Most countries don't have decent airforces
				
				modifier = {
					or = {
						has_tech = AS_Fighter2 #has semi-modern tech
						has_tech = MR_Fighter2
						has_tech = Strike_fighter2
						has_tech = L_Strike_fighter2
						has_tech = Air_UAV1
					}
					factor = 1
				}
				modifier = {
					or = {
						has_tech = strategic_bomber3 #has semi-modern tech, most countries dont have it
						has_tech = transport_plane2
						has_tech = naval_plane3
						has_tech = cas2
					}
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_FIGHTER_4 = yes
					factor = 0
				}
				modifier = {
					is_researching_fighters = yes
					factor = 4000
				}
			}
			
		}
	}
	
	Helicopter_Company = {
	
		designer = yes
		
		TUR_turkish_aerospace_industries_helicopter_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea TUR_turkish_aerospace_industries_helicopter_company" }
		
			picture = Turkish_Aerospace_Industries_TUR
			

			available = {
				OR = {
					tag = TUR
					controls_state = 159
				}
			}
			visible = {
				OR = {
					tag = TUR
					controls_state = 159
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_HELI = 0.124
			}
			
			traits = {
				Cat_HELI_4
			
			}
			ai_will_do = {
				factor = 0.4 #Most countries don't have decent airforces
				
				modifier = {
					has_tech = attack_helicopter2 #has semi-modern tech, most countries dont have it
					factor = 1
				}
				modifier = {
					has_tech = transport_helicopter2 #has semi-modern tech, most countries dont have it
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_HELI_4 = yes
					factor = 0
				}
				modifier = {
					is_researching_helicopters = yes
					factor = 4000
				}
			}
			
		}
	}
	
	Ship_Company = {
	
		designer = yes
		
		TUR_golcuk_naval_shipyard_ship_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea TUR_golcuk_naval_shipyard_ship_company" }
		
			picture = Golcuk_Naval_Shipyard_TUR
			

			available = {
				OR = {
					tag = TUR
					controls_state = 159
				}
			}
			visible = {
				OR = {
					tag = TUR
					controls_state = 159
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_NAVAL_EQP = 0.093
			}
			
			traits = {
				Cat_NAVAL_EQP_3
			
			}
			ai_will_do = {
				factor = 0.3
				
				modifier = {
					has_navy_size = { size > 25 } #has a large navy
					factor = 1
				}
				modifier = {
					num_of_naval_factories > 3 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					NOT = { #need to have ports
						any_owned_state = {
							is_coastal = yes
						}
					}
					factor = 10
				}
				modifier = {
					has_better_than_NAVAL_EQP_3 = yes
					factor = 0
				}
				modifier = {
					is_researching_naval_equipment = yes
					factor = 1000
				}
				modifier = {
					AND = {
						is_researching_carrier = yes
						has_CARRIER_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_surface_ship = yes
						has_SURFACE_SHIP_designer = yes
					}
					factor = 0
				}
			}
			
		}
	}
	
	Submarine_Company = {
	
		designer = yes
		
		TUR_golcuk_naval_shipyard_submarine_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea TUR_golcuk_naval_shipyard_submarine_company" }
		
			picture = Golcuk_Naval_Shipyard_TUR
			

			available = {
				OR = {
					tag = TUR
					controls_state = 159
				}
			}
			visible = {
				OR = {
					tag = TUR
					controls_state = 159
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_D_SUB = 0.093
			}
			
			traits = {
				Cat_D_SUB_3
			
			}
			ai_will_do = {
				factor = 0.3
				
				modifier = {
					has_navy_size = { size > 25 } #has a large navy
					factor = 1
				}
				modifier = {
					num_of_naval_factories > 3 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
						factor = 1
				}
				modifier = {
					NOT = { #need to have ports
						any_owned_state = {
						is_coastal = yes
						}
					}
					factor = 10
				}
				modifier = {
					has_better_than_D_SUB_3 = yes
					factor = 0
				}
				modifier = {
					is_researching_diesel_electric_sub = yes
					factor = 4000
				}
			}
			
		}
	}
	
}
