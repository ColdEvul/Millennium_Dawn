ideas = {

	Infantry_Weapon_Company = {
	
		designer = yes
		
		AUS_steyr_mannlicher_infantry_weapon_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea AUS_steyr_mannlicher_infantry_weapon_company" }
		
			picture = Steyr_Mannlicher_AUS
			

			available = {
				OR = {
					original_tag = AUS
					controls_state = 76
				}
			}
			visible = {
				OR = {
					original_tag = AUS
					# controls_state = 76
					AND = {
						controls_state = 76
						has_better_than_INF_WEP_7 = no
					}
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_INF_WEP = 0.217
			}
			
			traits = {
				Cat_INF_WEP_7
			
			}
			ai_will_do = {
				factor = 0.7 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 5 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_INF_WEP_7 = yes
					factor = 0
				}
				modifier = {
					is_researching_infantry_weapons = yes
					factor = 4000
				}
			}
			
		}
	}
	
	Vehicle_Company = {
	
		designer = yes
		
		AUS_general_dynamics_steyr_vehicle_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea AUS_general_dynamics_steyr_vehicle_company" }
		
			picture = General_Dynamics_Steyr_AUS
			

			available = {
				OR = {
					original_tag = AUS
					controls_state = 76
				}
			}
			visible = {
				OR = {
					original_tag = AUS
					# controls_state = 76
					AND = {
						controls_state = 76
						has_better_than_AFV_7 = no
					}
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_AFV = 0.217
			}
			
			traits = {
				Cat_AFV_7
			
			}
			ai_will_do = {
				factor = 0.7 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 10 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_AFV_7 = yes
					factor = 0
				}
				modifier = {
					is_researching_afv = yes
					factor = 4000
				}
			}
			
		}
	}
	
}
