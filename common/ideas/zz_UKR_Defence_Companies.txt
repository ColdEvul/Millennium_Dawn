ideas = {

	Infantry_Weapon_Company = {

		designer = yes

		UKR_kmdb_infantry_weapon_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea UKR_kmdb_infantry_weapon_company" }

			picture = KMDB_UKR


			available = {
				OR = {
					tag = UKR
					controls_state = 697
				}
			}
			visible = {
				OR = {
					tag = UKR
					controls_state = 697
				}
			}
			cost = 150

			removal_cost = 10

			research_bonus = {
				Cat_INF_WEP = 0.217
			}

			traits = {
				Cat_INF_WEP_7

			}
			ai_will_do = {
				factor = 0.7 #All countries need a land army, vehicles are part of modern warfare

				modifier = {
					num_of_military_factories > 5 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_INF_WEP_7 = yes
					factor = 0
				}
				modifier = {
					is_researching_infantry_weapons = yes
					factor = 4000
				}
			}

		}
	}

	Vehicle_Company = {

		designer = yes

		UKR_kmdb_vehicle_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea UKR_kmdb_vehicle_company" }

			picture = KMDB_UKR


			available = {
				OR = {
					tag = UKR
					controls_state = 697
				}
			}
			visible = {
				OR = {
					tag = UKR
					controls_state = 697
				}
			}
			cost = 150

			removal_cost = 10

			research_bonus = {
				Cat_ARMOR = 0.217
			}

			traits = {
				Cat_ARMOR_7

			}
			ai_will_do = {
				factor = 0.7 #All countries need a land army, vehicles are part of modern warfare

				modifier = {
					num_of_military_factories > 10 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_ARMOR_7 = yes
					factor = 0
				}
				modifier = {
					is_researching_armor = yes
					factor = 1000
				}
				modifier = {
					AND = {
						is_researching_afv = yes
						has_AFV_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_tanks = yes
						has_TANKS_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_artillery = yes
						has_ARTILLERY_designer = yes
					}
					factor = 0
				}
			}

		}
	}

	Vehicle_Company = {

		designer = yes

		UKR_malyshev_factory_vehicle_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea UKR_malyshev_factory_vehicle_company" }

			picture = Malyshev_Factory_UKR


			available = {
				OR = {
					tag = UKR
					controls_state = 697
				}
			}
			visible = {
				OR = {
					tag = UKR
					controls_state = 697
				}
			}
			cost = 150

			removal_cost = 10

			research_bonus = {
				Cat_ARMOR = 0.186
			}

			traits = {
				Cat_ARMOR_6

			}
			ai_will_do = {
				factor = 0.6 #All countries need a land army, vehicles are part of modern warfare

				modifier = {
					num_of_military_factories > 10 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_TANKS_6 = yes
					factor = 0
				}
				modifier = {
					is_researching_tanks = yes
					factor = 4000
				}
			}

		}
	}

	Aircraft_Company = {

		designer = yes

		UKR_antonov_aircraft_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea UKR_antonov_aircraft_company" }

			picture = Antonov_UKR


			available = {
				OR = {
					tag = UKR
					controls_state = 698
				}
			}
			visible = {
				OR = {
					tag = UKR
					controls_state = 698
				}
			}
			cost = 150

			removal_cost = 10

			research_bonus = {
				Cat_H_AIR = 0.217
			}

			traits = {
				Cat_H_AIR_7

			}
			ai_will_do = {
				factor = 0.7 #Most countries don't have decent airforces

				modifier = {
					or = {
						has_tech = AS_Fighter2 #has semi-modern tech
						has_tech = MR_Fighter2
						has_tech = Strike_fighter2
						has_tech = L_Strike_fighter2
						has_tech = Air_UAV1
					}
					factor = 1
				}
				modifier = {
					or = {
						has_tech = strategic_bomber3 #has semi-modern tech, most countries dont have it
						has_tech = transport_plane2
						has_tech = naval_plane3
						has_tech = cas2
					}
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_H_AIR_7 = yes
					factor = 0
				}
				modifier = {
					is_researching_heavy_air = yes
					factor = 4000
				}
			}

		}
	}

	Ship_Company = {

		designer = yes

		UKR_nikolayev_shipyard_ship_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea UKR_nikolayev_shipyard_ship_company" }

			picture = Nikolayev_Shipyard_UKR


			available = {
				OR = {
					tag = UKR
					controls_state = 695
				}
			}
			visible = {
				OR = {
					tag = UKR
					controls_state = 695
				}
			}
			cost = 150

			removal_cost = 10

			research_bonus = {
				Cat_CARRIER = 0.186
			}

			traits = {
				Cat_CARRIER_6

			}
			ai_will_do = {
				factor = 0.6

				modifier = {
					has_navy_size = { size > 25 } #has a large navy
					factor = 1
				}
				modifier = {
					num_of_naval_factories > 3 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					NOT = { #need to have ports
						any_owned_state = {
							is_coastal = yes
						}
					}
					factor = 10
				}
				modifier = {
					has_better_than_CARRIER_6 = yes
					factor = 0
				}
				modifier = {
					is_researching_carrier = yes
					factor = 4000
				}
			}

		}
	}

}
