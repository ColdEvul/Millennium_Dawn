ideas = {

	Infantry_Weapon_Company = {
	
		designer = yes
		
		SAF_denel_land_systems_infantry_weapon_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SAF_denel_land_systems_infantry_weapon_company" }
		
			picture = Denel_Land_Systems_SAF
			

			available = {
				OR = {
					tag = SAF
					controls_state = 275
				}
			}
			visible = {
				OR = {
					tag = SAF
					controls_state = 275
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_INF_WEP = 0.155
			}
			
			traits = {
				Cat_INF_WEP_5
			
			}
			ai_will_do = {
				factor = 0.5 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 5 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_INF_WEP_5 = yes
					factor = 0
				}
				modifier = {
					is_researching_infantry_weapons = yes
					factor = 4000
				}
			}
			
		}
	}
	
	Vehicle_Company = {
	
		designer = yes
		
		SAF_denel_land_systems_vehicle_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SAF_denel_land_systems_vehicle_company" }
		
			picture = Denel_Land_Systems_SAF
			

			available = {
				OR = {
					tag = SAF
					controls_state = 275
				}
			}
			visible = {
				OR = {
					tag = SAF
					controls_state = 275
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_ARTILLERY = 0.155
			}
			
			traits = {
				Cat_ARTILLERY_5
			
			}
			ai_will_do = {
				factor = 0.5 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 10 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_ARTILLERY_5 = yes
					factor = 0
				}
				modifier = {
					is_researching_artillery = yes
					factor = 4000
				}
			}
			
		}
	}
	
	Vehicle_Company = {
	
		designer = yes
		
		SAF_land_systems_omc_vehicle_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SAF_land_systems_omc_vehicle_company" }
		
			picture = Land_Systems_OMC_SAF
			

			available = {
				OR = {
					tag = SAF
					controls_state = 275
				}
			}
			visible = {
				OR = {
					tag = SAF
					controls_state = 275
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_AFV = 0.248
			}
			
			traits = {
				Cat_AFV_8
			
			}
			ai_will_do = {
				factor = 0.8 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 10 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_AFV_8 = yes
					factor = 0
				}
				modifier = {
					is_researching_afv = yes
					factor = 4000
				}
			}
			
		}
	}
	
	Infantry_Weapon_Company = {
	
		designer = yes
		
		SAF_denel_dynamics_infantry_weapon_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SAF_denel_dynamics_infantry_weapon_company" }
		
			picture = Denel_Dynamics_SAF
			

			available = {
				OR = {
					tag = SAF
					controls_state = 275
				}
			}
			visible = {
				OR = {
					tag = SAF
					controls_state = 275
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_INF = 0.155
			}
			
			traits = {
				Cat_INF_5
			
			}
			ai_will_do = {
				factor = 0.5 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 5 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_INF_5 = yes
					factor = 0
				}
				modifier = {
					is_researching_infantry_equipment = yes
					factor = 1000
				}
				modifier = {
					AND = {
						is_researching_aa = yes
						has_AA_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_at = yes
						has_AT_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_land_drones = yes
						has_L_DRONE_designer = yes
					}
					factor = 0
				}
			}
			
		}
	}
	
}
