### New setup ###

### Active or non-active icons ###
## Column 1:
defined_text = {
   name = select_influence_action_button

	text = {
		trigger = {
			OR = {
				AND = {
					has_idea = negligible_corruption
					ROOT = { has_political_power > 140 }
				}
				AND = {
					has_idea = slight_corruption
					ROOT = { has_political_power > 130 }
				}
				AND = {
					has_idea = modest_corruption
					ROOT = { has_political_power > 120 }
				}
				AND = {
					has_idea = medium_corruption
					ROOT = { has_political_power > 110 }
				}
				AND = {
					has_idea = widespread_corruption
					ROOT = { has_political_power > 100 }
				}
				AND = {
					has_idea = systematic_corruption
					ROOT = { has_political_power > 90 }
				}
				AND = {
					has_idea = unrestrained_corruption
					ROOT = { has_political_power > 80 }
				}
				AND = {
					has_idea = rampant_corruption
					ROOT = { has_political_power > 70 }
				}
				AND = {
					has_idea = crippling_corruption
					ROOT = { has_political_power > 60 }
				}
				AND = {
					has_idea = paralyzing_corruption
					ROOT = { has_political_power > 50 }
				}
			}
		}
		localization_key = "GFX_influence_action_button"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "GFX_influence_action_button_inactive"
	}
}
defined_text = {
   name = influence_button_req

	text = {
	   trigger = {
			OR = {
				AND = {
					has_idea = negligible_corruption
					ROOT = { has_political_power > 140 }
				}
				AND = {
					has_idea = slight_corruption
					ROOT = { has_political_power > 130 }
				}
				AND = {
					has_idea = modest_corruption
					ROOT = { has_political_power > 120 }
				}
				AND = {
					has_idea = medium_corruption
					ROOT = { has_political_power > 110 }
				}
				AND = {
					has_idea = widespread_corruption
					ROOT = { has_political_power > 100 }
				}
				AND = {
					has_idea = systematic_corruption
					ROOT = { has_political_power > 90 }
				}
				AND = {
					has_idea = unrestrained_corruption
					ROOT = { has_political_power > 80 }
				}
				AND = {
					has_idea = rampant_corruption
					ROOT = { has_political_power > 70 }
				}
				AND = {
					has_idea = crippling_corruption
					ROOT = { has_political_power > 60 }
				}
				AND = {
					has_idea = paralyzing_corruption
					ROOT = { has_political_power > 50 }
				}
			}
		}
	   localization_key = influence_action_button_req_enable_TT
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = influence_action_button_req_disable_TT
	}
}
defined_text = {
   name = influence_action_gain

	text = {
	   trigger = { ROOT = { check_variable = { influence_multiplier = 2 } } }
	   localization_key = influence_gain_superpower_TT
	}
	text = {
	   trigger = { ROOT = { check_variable = { influence_multiplier = 1.6 } } }
	   localization_key = influence_gain_great_power_TT
	}
	text = {
	   trigger = { ROOT = { check_variable = { influence_multiplier = 1.4 } } }
	   localization_key = influence_gain_major_power_TT
	}
	text = {
	   trigger = { ROOT = { check_variable = { influence_multiplier = 1.2 } } }
	   localization_key = influence_gain_regional_power_TT
	}
	text = {
	   trigger = { ROOT = { check_variable = { influence_multiplier = 1 } } }
	   localization_key = influence_gain_minor_power_TT
	}
	text = {
	   trigger = { ROOT = { check_variable = { influence_multiplier < 1 } } }
	   localization_key = influence_gain_non_power_TT
	}
}
defined_text = {
   name = aid_influence_action_gain

	text = {
	   trigger = { ROOT = { check_variable = { influence_multiplier = 2 } } }
	   localization_key = aid_influence_gain_superpower_TT
	}
	text = {
	   trigger = { ROOT = { check_variable = { influence_multiplier = 1.6 } } }
	   localization_key = aid_influence_gain_great_power_TT
	}
	text = {
	   trigger = { ROOT = { check_variable = { influence_multiplier = 1.4 } } }
	   localization_key = aid_influence_gain_major_power_TT
	}
	text = {
	   trigger = { ROOT = { check_variable = { influence_multiplier = 1.2 } } }
	   localization_key = aid_influence_gain_regional_power_TT
	}
	text = {
	   trigger = { ROOT = { check_variable = { influence_multiplier = 1 } } }
	   localization_key = aid_influence_gain_minor_power_TT
	}
	text = {
	   trigger = { ROOT = { check_variable = { influence_multiplier < 1 } } }
	   localization_key = aid_influence_gain_non_power_TT
	}
}

#Economic Aid/Military Aid
defined_text = {
   name = select_aid_button

	text = {
	   trigger = { is_influencer = yes }
	   localization_key = "GFX_aid_button"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "GFX_aid_button_inactive"
	}
}

defined_text = {
   name = aid_button_req

	text = {
		trigger = {
			is_influencer = yes
			check_variable = { gdp_capita < ROOT.gdp_capita }
	   }
	   localization_key = aid_button_req_enable_TT
	}
	text = {
		trigger = {
			is_influencer = yes
			NOT = { check_variable = { gdp_capita < ROOT.gdp_capita } }
	   }
	   localization_key = aid_button_req_disable1_TT
	}
	text = {
		trigger = {
			NOT = {
				is_influencer = yes
			 }
			check_variable = { gdp_capita < ROOT.gdp_capita }
	   }

	   localization_key = aid_button_req_disable2_TT
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = aid_button_req_disable3_TT
	}
}
defined_text = {
   name = aid_button_sizemod_check

	text = {
		trigger = {
			is_influencer = yes
			check_variable = { size_modifier < ROOT.size_modifier }
		}
	   localization_key = aid_button_sizemod_check_true
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = aid_button_sizemod_check_false
	}
}
defined_text = {
   name = select_aid_military_button

	text = {
		trigger = {
			is_influencer = yes
			OR = {
				ROOT = { has_idea = defense_industry }
				ROOT = { num_of_military_factories > 10 }
			}
		}
	   localization_key = "GFX_aid_military_button"
	}
	text = {
		trigger = {
			NOT = {
				is_influencer = yes
				ROOT = { has_idea = defense_industry }
				ROOT = { num_of_military_factories > 10 }
			}
		}
	   localization_key = "GFX_aid_military_button_inactive"
	}
}

defined_text = {
	name = military_aid_is_influencer_req
	text = {
		trigger = {
			is_influencer = yes
		}
		localization_key = is_influencer_true_TT
	}
	text = {
		#trigger = { always = yes }
		localization_key = is_influencer_false_TT
	}
}
defined_text = {
	name = military_aid_military_capable
	text = {
		trigger = {
			OR = {
				ROOT = { has_idea = defense_industry }
				ROOT = { num_of_military_factories > 10 }
			}
		}
		localization_key = military_aid_military_capable_TT
	}
	text = {
		trigger = {
			NOT = {
				is_influencer = yes
				ROOT = { has_idea = defense_industry }
				ROOT = { num_of_military_factories > 10 }
			}
		}
		localization_key = military_aid_military_capable_false_TT
	}
}
defined_text = {
   name = select_target_other_button

	text = {
	   trigger = {
			OR = {
				check_variable = { influence_array^0 = ROOT }
				check_variable = { influence_array^1 = ROOT }
				check_variable = { influence_array^2 = ROOT }
			}
			OR = {
				AND = {
					has_idea = negligible_corruption
					ROOT = { has_political_power > 140 }
				}
				AND = {
					has_idea = slight_corruption
					ROOT = { has_political_power > 130 }
				}
				AND = {
					has_idea = modest_corruption
					ROOT = { has_political_power > 120 }
				}
				AND = {
					has_idea = medium_corruption
					ROOT = { has_political_power > 110 }
				}
				AND = {
					has_idea = widespread_corruption
					ROOT = { has_political_power > 100 }
				}
				AND = {
					has_idea = systematic_corruption
					ROOT = { has_political_power > 90 }
				}
				AND = {
					has_idea = unrestrained_corruption
					ROOT = { has_political_power > 80 }
				}
				AND = {
					has_idea = rampant_corruption
					ROOT = { has_political_power > 70 }
				}
				AND = {
					has_idea = crippling_corruption
					ROOT = { has_political_power > 60 }
				}
				AND = {
					has_idea = paralyzing_corruption
					ROOT = { has_political_power > 50 }
				}
			}
	   }
	   localization_key = "GFX_target_other_button"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "GFX_target_other_button_inactive"
	}
}

defined_text = {
   name = trigger_select_target_other_button

	text = {
	   trigger = {
			OR = {
				check_variable = { influence_array^0 = ROOT }
				check_variable = { influence_array^1 = ROOT }
				check_variable = { influence_array^2 = ROOT }
			}
	   }
	   localization_key = "trigger_target_other_button_true"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "trigger_target_other_button_false"
	}
}

#Column 2:
defined_text = {
   name = select_manipulate_politics_button

	text = {
		trigger = {
			OR = {
				AND = {
					check_variable = { influence_array^0 = ROOT }
					check_variable = { influence_array_calc^0 > 0.2 }
				}
				AND = {
					check_variable = { influence_array^1 = ROOT }
					check_variable = { influence_array_calc^1 > 0.2 }
				}
				AND = {
					check_variable = { influence_array^2 = ROOT }
					check_variable = { influence_array_calc^2 > 0.2 }
				}
			}
		}
	   localization_key = "GFX_manipulate_politics_button"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "GFX_manipulate_politics_button_inactive"
	}
}
defined_text = {
   name = trigger_select_manipulate_politics_button

	text = {
		trigger = {
			OR = {
				AND = {
					check_variable = { influence_array^0 = ROOT }
					check_variable = { influence_array_calc^0 > 0.2 }
				}
				AND = {
					check_variable = { influence_array^1 = ROOT }
					check_variable = { influence_array_calc^1 > 0.2 }
				}
				AND = {
					check_variable = { influence_array^2 = ROOT }
					check_variable = { influence_array_calc^2 > 0.2 }
				}
			}
		}
	   localization_key = trigger_manipulate_politics_button_true
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = trigger_manipulate_politics_button_false
	}
}
defined_text = {
   name = trigger_manipulate_politics_value

	text = {
		trigger = {	THIS = {has_idea = negligible_corruption} }
	   localization_key = trigger_manipulate_politics_value_1
	}
	text = {
		trigger = {	THIS={has_idea = slight_corruption} }
	   localization_key = trigger_manipulate_politics_value_2
	}
	text = {
		trigger = {	THIS={has_idea = modest_corruption} }
	   localization_key = trigger_manipulate_politics_value_3
	}
	text = {
		trigger = {	THIS={has_idea = medium_corruption} }
	   localization_key = trigger_manipulate_politics_value_4
	}
	text = {
		trigger = {	THIS={has_idea = widespread_corruption} }
	   localization_key = trigger_manipulate_politics_value_5
	}
	text = {
		trigger = {	THIS={has_idea = systematic_corruption} }
	   localization_key = trigger_manipulate_politics_value_6
	}
	text = {
		trigger = {	THIS={has_idea = unrestrained_corruption} }
	   localization_key = trigger_manipulate_politics_value_7
	}
	text = {
		trigger = {	THIS={has_idea = rampant_corruption} }
	   localization_key = trigger_manipulate_politics_value_8
	}
	text = {
		trigger = {	THIS={has_idea = crippling_corruption} }
	   localization_key = trigger_manipulate_politics_value_9
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = trigger_manipulate_politics_value_10
	}
}
defined_text = {
   name = influence_pp_requirement

	text = {
		trigger = {	THIS = {has_idea = negligible_corruption} }
	   localization_key = influence_pp_requirement_1
	}
	text = {
		trigger = {	THIS={has_idea = slight_corruption} }
	   localization_key = influence_pp_requirement_2
	}
	text = {
		trigger = {	THIS={has_idea = modest_corruption} }
	   localization_key = influence_pp_requirement_3
	}
	text = {
		trigger = {	THIS={has_idea = medium_corruption} }
	   localization_key = influence_pp_requirement_4
	}
	text = {
		trigger = {	THIS={has_idea = widespread_corruption} }
	   localization_key = influence_pp_requirement_5
	}
	text = {
		trigger = {	THIS={has_idea = systematic_corruption} }
	   localization_key = influence_pp_requirement_6
	}
	text = {
		trigger = {	THIS={has_idea = unrestrained_corruption} }
	   localization_key = influence_pp_requirement_7
	}
	text = {
		trigger = {	THIS={has_idea = rampant_corruption} }
	   localization_key = influence_pp_requirement_8
	}
	text = {
		trigger = {	THIS={has_idea = crippling_corruption} }
	   localization_key = influence_pp_requirement_9
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = influence_pp_requirement_10
	}
}
defined_text = {
   name = influence_pp_requirement2

	text = {
		trigger = {	THIS = {has_idea = negligible_corruption} }
	   localization_key = influence_pp_requirement_17
	}
	text = {
		trigger = {	THIS={has_idea = slight_corruption} }
	   localization_key = influence_pp_requirement_16
	}
	text = {
		trigger = {	THIS={has_idea = modest_corruption} }
	   localization_key = influence_pp_requirement_15
	}
	text = {
		trigger = {	THIS={has_idea = medium_corruption} }
	   localization_key = influence_pp_requirement_14
	}
	text = {
		trigger = {	THIS={has_idea = widespread_corruption} }
	   localization_key = influence_pp_requirement_13
	}
	text = {
		trigger = {	THIS={has_idea = systematic_corruption} }
	   localization_key = influence_pp_requirement_12
	}
	text = {
		trigger = {	THIS={has_idea = unrestrained_corruption} }
	   localization_key = influence_pp_requirement_11
	}
	text = {
		trigger = {	THIS={has_idea = rampant_corruption} }
	   localization_key = influence_pp_requirement_1
	}
	text = {
		trigger = {	THIS={has_idea = crippling_corruption} }
	   localization_key = influence_pp_requirement_3
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = influence_pp_requirement_5
	}
}
defined_text = {
   name = select_coup_button

	text = {
	   trigger = {
			NOT = { is_same_government = yes }
			NOT = { is_puppet_of = ROOT }
			NOT = { has_war = yes }
			OR = {
				AND = {
					check_variable = { influence_array^0 = ROOT }
					check_variable = { influence_array_calc^0 > 0.3 }
				}
				AND = {
					check_variable = { influence_array^1 = ROOT }
					check_variable = { influence_array_calc^1 > 0.3 }
				}
			}
		}
	   localization_key = "GFX_stage_coup_button"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "GFX_stage_coup_button_inactive"
	}
}
defined_text = {
   name = trigger1_select_coup_button

	text = {
		trigger = {
			OR = {
				AND = {
					check_variable = { influence_array^0 = ROOT }
					check_variable = { influence_array_calc^0 > 0.3 }
				}
				AND = {
					check_variable = { influence_array^1 = ROOT }
					check_variable = { influence_array_calc^1 > 0.3 }
				}
			}
		}
	   localization_key = trigger1_coup_button_true
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = trigger1_coup_button_false
	}
}
defined_text = {
   name = trigger2_select_coup_button

	text = {
		trigger = {
			NOT = { is_same_government = yes }
		}
	   localization_key = trigger2_coup_button_true
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = trigger2_coup_button_false
	}
}
defined_text = {
   name = trigger3_select_coup_button

	text = {
		trigger = {
			OR = {
				AND = {
					has_idea = negligible_corruption
					ROOT = { has_political_power > 280 }
				}
				AND = {
					has_idea = slight_corruption
					ROOT = { has_political_power > 260 }
				}
				AND = {
					has_idea = modest_corruption
					ROOT = { has_political_power > 240 }
				}
				AND = {
					has_idea = medium_corruption
					ROOT = { has_political_power > 220 }
				}
				AND = {
					has_idea = widespread_corruption
					ROOT = { has_political_power > 200 }
				}
				AND = {
					has_idea = systematic_corruption
					ROOT = { has_political_power > 180 }
				}
				AND = {
					has_idea = unrestrained_corruption
					ROOT = { has_political_power > 160 }
				}
				AND = {
					has_idea = rampant_corruption
					ROOT = { has_political_power > 140 }
				}
				AND = {
					has_idea = crippling_corruption
					ROOT = { has_political_power > 120 }
				}
				AND = {
					has_idea = paralyzing_corruption
					ROOT = { has_political_power > 100 }
				}
			}
		}
	   localization_key = trigger3_coup_button_true
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = trigger3_coup_button_false
	}
}
defined_text = {
   name = select_economic_exploitation_button

	text = {
	   trigger = {
			check_variable = { influence_array^0 = ROOT }
			check_variable = { influence_array_calc^0 > 0.5 } #50%
			NOT = { is_puppet_of = ROOT }
			#not twice
			NOT = { has_country_flag = country_exploited }
	   }
	   localization_key = "GFX_economic_exploitation_button"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "GFX_economic_exploitation_button_inactive"
	}
}

defined_text = {
   name = trigger1_select_economic_exploitation_button

	text = {
		trigger = {
			check_variable = { influence_array^0 = ROOT }
			check_variable = { influence_array_calc^0 > 0.5 } #50%
		}
	   localization_key = trigger1_economic_exploitation_button_true
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = trigger1_economic_exploitation_button_false
	}
}

defined_text = {
   name = root_tag_loc

	text = {
	   localization_key = root_tag_loc_txt
	}
}

defined_text = {
   name = select_make_puppet_button

	text = {
	   trigger = {
			check_variable = { influence_array^0 = ROOT }
			check_variable = { influence_array_calc^0 > 0.8 } #80%
			is_same_government = yes
			NOT = { is_puppet_of = ROOT }
			NOT = { has_war = yes }
	   }
	   localization_key = "GFX_make_puppet_button"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "GFX_make_puppet_button_inactive"
	}
}

defined_text = {
   name = trigger1_select_make_puppet_button

	text = {
		trigger = {
			check_variable = { influence_array^0 = ROOT }
			check_variable = { influence_array_calc^0 > 0.8 } #80%
		}
	   localization_key = trigger1_make_puppet_button_true
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = trigger1_make_puppet_button_false
	}
}

defined_text = {
   name = trigger2_select_make_puppet_button

	text = {
		trigger = {
			is_same_government = yes
		}
	   localization_key = trigger2_make_puppet_button_true
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = trigger2_make_puppet_button_false
	}
}

defined_text = {
   name = trigger3_select_make_puppet_button

	text = {
		trigger = {
			NOT = { has_war = yes }
		}
	   localization_key = trigger3_make_puppet_button_true
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = trigger3_make_puppet_button_false
	}
}

defined_text = {
   name = trigger4_select_make_puppet_button

	text = {
		trigger = {
			NOT = { is_subject_of = ROOT }
		}
	   localization_key = trigger4_make_puppet_button_true
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = trigger4_make_puppet_button_false
	}
}

defined_text = {
	name = new_position_1
	text = {
		trigger = {
			has_variable = influence_array^0
		}
		localization_key = "[?var:influence_array^0.GetFlag] [?var:influence_array^0.GetName]"
	}
	text = {
	   localization_key = ""
	}
}
defined_text = {
	name = new_position_2
	text = {
		trigger = {
			has_variable = influence_array^1
		}
		localization_key = "[?var:influence_array^1.GetFlag] [?var:influence_array^1.GetName]"
	}
	text = {
	   localization_key = ""
	}
}
defined_text = {
	name = new_position_3
	text = {
		trigger = {
			has_variable = influence_array^2
		}
		localization_key = "[?var:influence_array^2.GetFlag] [?var:influence_array^2.GetName]"
	}
	text = {
	   localization_key = ""
	}
}
defined_text = {
	name = new_position_4
	text = {
		trigger = {
			has_variable = influence_array^3
		}
		localization_key = "[?var:influence_array^3.GetFlag] [?var:influence_array^3.GetName]"
	}
	text = {
	   localization_key = ""
	}
}
defined_text = {
	name = new_position_5
	text = {
		trigger = {
			has_variable = influence_array^4
		}
		localization_key = "[?var:influence_array^4.GetFlag] [?var:influence_array^4.GetName]"
	}
	text = {
	   localization_key = ""
	}
}
defined_text = {
	name = new_position_6
	text = {
		trigger = {
			has_variable = influence_array^5
		}
		localization_key = "[?var:influence_array^5.GetFlag] [?var:influence_array^5.GetName]"
	}
	text = {
	   localization_key = ""
	}
}
defined_text = {
	name = new_position_7
	text = {
		trigger = {
			has_variable = influence_array^6
		}
		localization_key = "[?var:influence_array^6.GetFlag] [?var:influence_array^6.GetName]"
	}
	text = {
	   localization_key = ""
	}
}


### For targeted desicions ###

defined_text = {
	name = position_1_var_integer

	text = { trigger = { has_variable = influence_array^0 }
	   localization_key = "([?influence_array_val^0])"
	}
	text = {
	   localization_key = ""
	}
}

defined_text = {
	name = position_2_var_integer

	text = { trigger = { has_variable = influence_array^1 }
	   localization_key = "([?influence_array_val^1])"
	}
	text = {
	   localization_key = ""
	}
}

defined_text = {
	name = position_3_var_integer

	text = { trigger = { has_variable = influence_array^2 }
	   localization_key = "([?influence_array_val^2])"
	}
	text = {
	   localization_key = ""
	}
}

defined_text = {
	name = position_4_var_integer

	text = { trigger = { has_variable = influence_array^3 }
	   localization_key = "([?influence_array_val^3])"
	}
	text = {
	   localization_key = ""
	}
}

defined_text = {
	name = position_5_var_integer

	text = { trigger = { has_variable = influence_array^4 }
	   localization_key = "([?influence_array_val^4])"
	}
	text = {
	   localization_key = ""
	}
}

defined_text = {
	name = position_6_var_integer

	text = { trigger = { has_variable = influence_array^5 }
	   localization_key = "([?influence_array_val^5])"
	}
	text = {
	   localization_key = ""
	}
}

defined_text = {
	name = position_7_var_integer

	text = { trigger = { has_variable = influence_array^6 }
	   localization_key = "([?influence_array_val^6])"
	}
	text = {
	   localization_key = ""
	}
}

### Pole pos:
defined_text = {
	name = position_1_var_n

	text = { trigger = { has_variable = influence_array^0 }
	   localization_key = "1st"
	}
	text = {
	   localization_key = ""
	}
}

defined_text = {
	name = position_2_var_n

	text = { trigger = { has_variable = influence_array^1 }
	   localization_key = "2nd"
	}
	text = {
	   localization_key = ""
	}
}

defined_text = {
	name = position_3_var_n

	text = { trigger = { has_variable = influence_array^2 }
	   localization_key = "3rd"
	}
	text = {
	   localization_key = ""
	}
}

defined_text = {
	name = position_4_var_n

	text = { trigger = { has_variable = influence_array^3 }
	   localization_key = "4th"
	}
	text = {
	   localization_key = ""
	}
}

defined_text = {
	name = position_5_var_n

	text = { trigger = { has_variable = influence_array^4 }
	   localization_key = "5th"
	}
	text = {
	   localization_key = ""
	}
}

defined_text = {
	name = position_6_var_n

	text = { trigger = { has_variable = influence_array^5 }
	   localization_key = "6th"
	}
	text = {
	   localization_key = ""
	}
}

defined_text = {
	name = position_7_var_n

	text = { trigger = { has_variable = influence_array^6 }
	   localization_key = "7th"
	}
	text = {
	   localization_key = ""
	}
}

### Display ###

defined_text = {
	name = position_1_var

	text = { trigger = { has_variable = influence_array^0 }
	   localization_key = "[?influence_array_calc^0|%0]"
	}
	text = {
	   localization_key = ""
	}
}

defined_text = {
	name = position_2_var

	text = { trigger = { has_variable = influence_array^1 }
	   localization_key = "[?influence_array_calc^1|%0]"
	}
	text = {
	   localization_key = ""
	}
}

defined_text = {
	name = position_3_var

	text = { trigger = { has_variable = influence_array^2 }
	   localization_key = "[?influence_array_calc^2|%0]"
	}
	text = {
	   localization_key = ""
	}
}

defined_text = {
	name = position_4_var

	text = { trigger = { has_variable = influence_array^3 }
	   localization_key = "[?influence_array_calc^3|%0]"
	}
	text = {
	   localization_key = ""
	}
}

defined_text = {
	name = position_5_var

	text = { trigger = { has_variable = influence_array^4 }
	   localization_key = "[?influence_array_calc^4|%0]"
	}
	text = {
	   localization_key = ""
	}
}

defined_text = {
	name = position_6_var

	text = { trigger = { has_variable = influence_array^5 }
	   localization_key = "[?influence_array_calc^5|%0]"
	}
	text = {
	   localization_key = ""
	}
}

defined_text = {
	name = position_7_var

	text = { trigger = { has_variable = influence_array^6 }
	   localization_key = "[?influence_array_calc^6|%0]"
	}
	text = {
	   localization_key = ""
	}
}

defined_text = {
	name = influence_multiplier_modifier
	text = {
		trigger = { check_variable = { influence_multiplier = 0.75 } }
		localization_key = influence_multiplier_0_75_tt
	}
	text = {
		trigger = { check_variable = { influence_multiplier = 1.1 } }
		localization_key = influence_multiplier_1_1_tt
	}
	text = {
		trigger = { check_variable = { influence_multiplier = 1.2 } }
		localization_key = influence_multiplier_1_2_tt
	}
	text = {
		trigger = { check_variable = { influence_multiplier = 1.3 } }
		localization_key = influence_multiplier_1_3_tt
	}
	text = {
		trigger = { check_variable = { influence_multiplier = 1.4 } }
		localization_key = influence_multiplier_1_4_tt
	}
	text = {
		trigger = { check_variable = { influence_multiplier = 1.5 } }
		localization_key = influence_multiplier_1_5_tt
	}
	text = {
		localization_key = ""
	}
}