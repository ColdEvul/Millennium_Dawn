
## Remove Money From Treasury:
### "SE_remove_[X]_treasury"
SE_remove_1_treasury = {
	subtract_from_variable = { treasury = 1 }
	custom_effect_tooltip = 1_billion_expense_tt
}
SE_remove_2_treasury = {
	subtract_from_variable = { treasury = 2 }
	custom_effect_tooltip = 2_billion_expense_tt
}
SE_remove_3_treasury = {
	subtract_from_variable = { treasury = 3 }
	custom_effect_tooltip = 3_billion_expense_tt
}
SE_remove_4_treasury = {
	subtract_from_variable = { treasury = 4 }
	custom_effect_tooltip = 4_billion_expense_tt
}
SE_remove_5_treasury = {
	subtract_from_variable = { treasury = 5 }
	custom_effect_tooltip = 5_billion_expense_tt
}
SE_remove_6_treasury = {
	subtract_from_variable = { treasury = 6 }
	custom_effect_tooltip = 6_billion_expense_tt
}
SE_remove_7_treasury = {
	subtract_from_variable = { treasury = 7 }
	custom_effect_tooltip = 7_billion_expense_tt
}
SE_remove_8_treasury = {
	subtract_from_variable = { treasury = 8 }
	custom_effect_tooltip = 8_billion_expense_tt
}
SE_remove_9_treasury = {
	subtract_from_variable = { treasury = 9 }
	custom_effect_tooltip = 9_billion_expense_tt
}
SE_remove_10_treasury = {
	subtract_from_variable = { treasury = 10 }
	custom_effect_tooltip = 10_billion_expense_tt
}
SE_remove_11_treasury = {
	subtract_from_variable = { treasury = 11 }
	custom_effect_tooltip = 11_billion_expense_tt
}
SE_remove_12_treasury = {
	subtract_from_variable = { treasury = 12 }
	custom_effect_tooltip = 12_billion_expense_tt
}
SE_remove_13_treasury = {
	subtract_from_variable = { treasury = 13 }
	custom_effect_tooltip = 13_billion_expense_tt
}
SE_remove_14_treasury = {
	subtract_from_variable = { treasury = 14 }
	custom_effect_tooltip = 14_billion_expense_tt
}
SE_remove_15_treasury = {
	subtract_from_variable = { treasury = 15 }
	custom_effect_tooltip = 15_billion_expense_tt
}
SE_remove_15_treasury = {
	subtract_from_variable = { treasury = 15 }
	custom_effect_tooltip = 15_billion_expense_tt
}
SE_remove_16_treasury = {
	subtract_from_variable = { treasury = 16 }
	custom_effect_tooltip = 16_billion_expense_tt
}
SE_remove_17_treasury = {
	subtract_from_variable = { treasury = 17 }
	custom_effect_tooltip = 17_billion_expense_tt
}
SE_remove_18_treasury = {
	subtract_from_variable = { treasury = 18 }
	custom_effect_tooltip = 18_billion_expense_tt
}
SE_remove_19_treasury = {
	subtract_from_variable = { treasury = 19 }
	custom_effect_tooltip = 19_billion_expense_tt
}
SE_remove_20_treasury = {
	subtract_from_variable = { treasury = 20 }
	custom_effect_tooltip = 20_billion_expense_tt
}
SE_remove_25_treasury = {
	subtract_from_variable = { treasury = 25 }
	custom_effect_tooltip = 25_billion_expense_tt
}

##propaganda Cost Scripted Effect
propaganda_cost_scripted_effect = {
	if = { limit = { check_variable = { num_of_factories < 4 } }
		subtract_from_variable = { treasury = 0.05 }
	}
	if = {
		limit = {
			check_variable = { num_of_factories > 3 }
			check_variable = { num_of_factories < 8 }
		}
		subtract_from_variable = { treasury = 0.1 }
	}
	if = {
		limit = {
			check_variable = { num_of_factories > 7 }
			check_variable = { num_of_factories < 16 }
		}
		subtract_from_variable = { treasury = 0.2 }
	}
	if = {
		limit = {
			check_variable = { num_of_factories > 15 }
			check_variable = { num_of_factories < 26 }
		}
		subtract_from_variable = { treasury = 0.35 }
	}
	if = {
		limit = {
			check_variable = { num_of_factories > 25 }
			check_variable = { num_of_factories < 39 }
		}
		subtract_from_variable = { treasury = 0.5 }
	}
	if = {
		limit = {
			check_variable = { num_of_factories > 38 }
			check_variable = { num_of_factories < 54 }
		}
		subtract_from_variable = { treasury = 0.75 }
	}
	if = {
		limit = {
			check_variable = { num_of_factories > 53 }
			check_variable = { num_of_factories < 82 }
		}
		subtract_from_variable = { treasury = 1.0 }
	}
	if = {
		limit = {
			check_variable = { num_of_factories > 81 }
			check_variable = { num_of_factories < 120 }
		}
		subtract_from_variable = { treasury = 1.5 }
	}
	if = {
		limit = {
			check_variable = { num_of_factories > 119 }
			check_variable = { num_of_factories < 164 }
		}
		subtract_from_variable = { treasury = 2.5 }
	}
	if = {
		limit = {
			check_variable = { num_of_factories > 163 }
			check_variable = { num_of_factories < 250 }
		}
		subtract_from_variable = { treasury = 4.0 }
	}
	if = {
		limit = {
			check_variable = { num_of_factories > 249 }
			check_variable = { num_of_factories < 370 }
		}
		subtract_from_variable = { treasury = 5.5 }
	}
	if = {
		limit = {
			check_variable = { num_of_factories > 369 }
			check_variable = { num_of_factories < 500 }
		}
		subtract_from_variable = { treasury = 7.5 }
	}
	if = {
		limit = {
			check_variable = { num_of_factories > 499 }
		}
		subtract_from_variable = { treasury = 10.0 }
	}
}

lease_military_factories_cost = {
	if = { limit = { check_variable = { num_of_factories < 8 } }
		subtract_from_variable = { treasury = 1.50 }
	}
	if = {
		limit = {
			check_variable = { num_of_factories > 8 }
			check_variable = { num_of_factories < 15 }
		}
		subtract_from_variable = { treasury = 3.50 }
	}
	if = {
		limit = {
			check_variable = { num_of_factories > 15 }
			check_variable = { num_of_factories < 26 }
		}
		subtract_from_variable = { treasury = 7.0 }
	}
	if = {
		limit = {
			check_variable = { num_of_factories > 25 }
			check_variable = { num_of_factories < 40 }
		}
		subtract_from_variable = { treasury = 9.0 }
	}
	if = {
		limit = {
			check_variable = { num_of_factories > 39 }
			check_variable = { num_of_factories < 50 }
		}
		subtract_from_variable = { treasury =  12.0 }
	}
	if = {
		limit = {
			check_variable = { num_of_factories > 49 }
			check_variable = { num_of_factories < 75 }
		}
		subtract_from_variable = { treasury = 15.0 }
	}
	if = {
		limit = {
			check_variable = { num_of_factories > 74 }
			check_variable = { num_of_factories < 100 }
		}
		subtract_from_variable = { treasury = 18.0 }
	}
	if = {
		limit = {
			check_variable = { num_of_factories > 99 }
			check_variable = { num_of_factories < 125 }
		}
		subtract_from_variable = { treasury = 22.5 }
	}
	if = {
		limit = {
			check_variable = { num_of_factories > 124 }
			check_variable = { num_of_factories < 150 }
		}
		subtract_from_variable = { treasury = 27.50 }
	}
	if = {
		limit = {
			check_variable = { num_of_factories > 149 }
			check_variable = { num_of_factories < 200 }
		}
		subtract_from_variable = { treasury = 30.0 }
	}
	if = {
		limit = {
			check_variable = { num_of_factories > 199 }
			check_variable = { num_of_factories < 250 }
		}
		subtract_from_variable = { treasury = 35.0 }
	}
	if = {
		limit = {
			check_variable = { num_of_factories > 249 }
			check_variable = { num_of_factories < 300 }
		}
		subtract_from_variable = { treasury = 40.0 }
	}
	if = {
		limit = {
			check_variable = { num_of_factories > 299 }
		}
		subtract_from_variable = { treasury = 50.0 }
	}
}

lease_civilian_complex_cost = {
	if = { limit = { check_variable = { num_of_factories < 8 } }
		subtract_from_variable = { treasury = 1.00 }
	}
	if = {
		limit = {
			check_variable = { num_of_factories > 8 }
			check_variable = { num_of_factories < 15 }
		}
		subtract_from_variable = { treasury = 2.0 }
	}
	if = {
		limit = {
			check_variable = { num_of_factories > 15 }
			check_variable = { num_of_factories < 26 }
		}
		subtract_from_variable = { treasury = 5.0 }
	}
	if = {
		limit = {
			check_variable = { num_of_factories > 25 }
			check_variable = { num_of_factories < 40 }
		}
		subtract_from_variable = { treasury = 7.5 }
	}
	if = {
		limit = {
			check_variable = { num_of_factories > 39 }
			check_variable = { num_of_factories < 50 }
		}
		subtract_from_variable = { treasury =  10.0 }
	}
	if = {
		limit = {
			check_variable = { num_of_factories > 49 }
			check_variable = { num_of_factories < 75 }
		}
		subtract_from_variable = { treasury = 12.5 }
	}
	if = {
		limit = {
			check_variable = { num_of_factories > 74 }
			check_variable = { num_of_factories < 100 }
		}
		subtract_from_variable = { treasury = 15.0 }
	}
	if = {
		limit = {
			check_variable = { num_of_factories > 99 }
			check_variable = { num_of_factories < 125 }
		}
		subtract_from_variable = { treasury = 17.5 }
	}
	if = {
		limit = {
			check_variable = { num_of_factories > 124 }
			check_variable = { num_of_factories < 150 }
		}
		subtract_from_variable = { treasury = 20.0 }
	}
	if = {
		limit = {
			check_variable = { num_of_factories > 149 }
			check_variable = { num_of_factories < 200 }
		}
		subtract_from_variable = { treasury = 22.50 }
	}
	if = {
		limit = {
			check_variable = { num_of_factories > 199 }
			check_variable = { num_of_factories < 250 }
		}
		subtract_from_variable = { treasury = 25.0 }
	}
	if = {
		limit = {
			check_variable = { num_of_factories > 249 }
			check_variable = { num_of_factories < 300 }
		}
		subtract_from_variable = { treasury = 27.5 }
	}
	if = {
		limit = {
			check_variable = { num_of_factories > 299 }
		}
		subtract_from_variable = { treasury = 30.0 }
	}
}