ai_update_build_units = { #default is not to build units
	if = { #Stop building units
		limit = {
			NOT = { has_country_flag = ai_yes_unit_build }
			OR = {
				has_war = yes
				threat > 0.30
				is_bad_salafist = yes
				any_other_country = {
					OR = {
						is_justifying_wargoal_against = THIS
						has_wargoal_against = THIS
					}
				}
			}
		}
		set_country_flag = ai_yes_unit_build
		add_to_variable = { var = ai_wants_divisions value = 500 }
	}
	else_if = {
		limit = {
			has_country_flag = ai_yes_unit_build #check will end here if they don't have the flag thus avoiding the expensive triggers below
			has_war = no
			threat < 0.31
			is_bad_salafist = no
			any_other_country = {
				NOT = {
					is_justifying_wargoal_against = this
					has_wargoal_against = this
				}
			}
		}
		clr_country_flag = ai_yes_unit_build
		add_to_variable = { var = ai_wants_divisions value = -500 } #maybe causing a crash? You can edit vanilla vars? idk who did this
	}

	if = {
		limit = {
			has_country_flag = AI_is_threatened
			has_war = no
			threat < 0.31
			is_bad_salafist = no
			any_other_country = {
				NOT = {
					is_justifying_wargoal_against = this
					has_wargoal_against = this
				}
			}
		}
		clr_country_flag = AI_is_threatened
	}
}