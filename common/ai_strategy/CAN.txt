CAN_befriend_other_north_americans = {
	allowed = { original_tag = CAN }
	enable = {
		OR = {
			country_exists = USA
			country_exists = MEX
		}
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = befriend id = "USA" value = 100 }
	ai_strategy = { type = influence id = "USA" value = 100 }
	ai_strategy = { type = support id = "USA" value = 100 }
	ai_strategy = { type = send_volunteers_desire id = "USA" value = 25 }
	ai_strategy = { type = befriend id = "MEX" value = 100 }
	ai_strategy = { type = influence id = "MEX" value = 100 }
	ai_strategy = { type = support id = "MEX" value = 100 }
	ai_strategy = { type = send_volunteers_desire id = "MEX" value = 25 }
}

CAN_prepare_for_war_against_america = {
	allowed = { original_tag = CAN }
	enable = { USA = { has_completed_focus = USA_political_revolution } }
	abort_when_not_enabled = yes

	ai_strategy = { type = prepare_for_war id = "USA" value = 200 }
	ai_strategy = { type = force_build_armies value = 100 }
}