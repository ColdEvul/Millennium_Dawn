##Written by Bird
SAU_rival_iran = {
	allowed = { original_tag = SAU }
	enable = { country_exists = PER }
	abort_when_not_enabled = yes

	ai_strategy = { type = antagonize id = "PER" value = 200 }
	ai_strategy = { type = contain id = "PER" value = 200 }
	ai_strategy = { type = consider_weak id = "PER" value = 200 }
}