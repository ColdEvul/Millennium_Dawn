FRA_french_space_decision_category = {

	FRA_run_support_campaigns_short = {
		allowed = { original_tag = FRA }
		cost = 75
		icon = political_actions

		days_remove = 45
		days_re_enable = 7
		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision FRA_run_support_campaigns_short"
			add_to_variable = { FRA_peoples_support = 2.5 }
			subtract_from_variable = { treasury = 5.0 }
			custom_effect_tooltip = FRA_peoples_support_campaigns_short_TT
		}

		ai_will_do = {
			factor = 5

			modifier = {
				add = 0
				is_historical_focus_on = Yes
			}
		}
	}
	FRA_run_support_campaigns_long = {
		allowed = { original_tag = FRA }
		cost = 75
		icon = political_actions

		days_remove = 90
		days_re_enable = 14
		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision FRA_run_support_campaigns_long"
			add_to_variable = { FRA_peoples_support = 5 }
			subtract_from_variable = { treasury = 10.0 }
			custom_effect_tooltip = FRA_peoples_support_campaigns_long_TT
		}

		ai_will_do = {
			factor = 5

			modifier = {
				add = 0
				is_historical_focus_on = Yes
			}
		}
	}
	FRA_rebalance_the_cnes_budget = {
		allowed = { original_tag = FRA }
		cost = 100
		icon = Rebalance_CNES_Budget

		days_remove = 45
		fire_only_once = yes

		available = {
			has_completed_focus = FRA_cnes_budgeting
			custom_trigger_tooltip = {
				tooltip = fra_has_enough_space_support_tt
				NOT = { check_variable = { FRA_peoples_support < 35 } }
			}
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision FRA_rebalance_the_cnes_budget"
			subtract_from_variable = { treasury = 1.0 }
			add_to_variable = { FRA_space_income_var = 1.0 }
			add_to_variable = { FRA_peoples_support = 5 }
			custom_effect_tooltip = FRA_rebalance_the_cnes_budget_TT
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove FRA_rebalance_the_cnes_budget"
			remove_ideas = FRA_idea_cnes_failing
		}

		ai_will_do = {
			factor = 5

			modifier = {
				add = 20
				is_historical_focus_on = yes
			}
		}
	}
	FRA_launch_commercial_satellite = {
		allowed = { original_tag = FRA }
		cost = 150
		icon = Launch_Commercial_Satellite

		days_remove = 120
		days_re_enable = 25

		available = {
			has_completed_focus = FRA_french_space
			custom_trigger_tooltip = {
				tooltip = fra_has_enough_space_support_tt
				NOT = { check_variable = { FRA_peoples_support < 35 } }
			}
		}

		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove FRA_launch_commercial_satellite"
			subtract_from_variable = { treasury = 3.0 }
			add_to_variable = { FRA_space_income_var = 1.0 }
			subtract_from_variable = { FRA_peoples_support = 5.0 }
			custom_effect_tooltip = FRA_launch_commercial_satellite_TT
		}
	}
	FRA_prep_international_space_station = {
		allowed = { original_tag = FRA }
		cost = 100
		icon = Prepare_an_ISS_Launch

		days_remove = 30
		days_re_enable = 120

		available = {
			has_completed_focus = FRA_french_space
			NOT = { has_idea = FRA_idea_space_research }
			custom_trigger_tooltip = {
				tooltip = fra_has_enough_space_support_tt
				NOT = { check_variable = { FRA_peoples_support < 35 } }
			}
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision FRA_prep_international_space_station"
			add_timed_idea = {
				idea = FRA_idea_prep_the_launch
				days = 120
			}
			subtract_from_variable = { FRA_peoples_support = 5.0 }
		}
	}
	FRA_french_iss_launch = {
		allowed = { original_tag = FRA }
		cost = 100
		icon = French_ISS_Launch

		days_remove = 30
		days_re_enable = 120

		available = {
			has_completed_focus = FRA_french_space
			NOT = { has_idea = FRA_idea_prep_the_launch }
			NOT = {	has_idea = FRA_idea_space_research }
			custom_trigger_tooltip = {
				tooltip = fra_has_enough_space_support_tt
				NOT = { check_variable = { FRA_peoples_support < 35 } }
			}
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision FRA_french_iss_launch"
			add_timed_idea = {
				idea = FRA_idea_space_research
				days = 365
			}
			subtract_from_variable = { FRA_peoples_support = 5.0 }
			add_to_variable = { FRA_space_income_var = 1.0 }
		}
	}

	FRA_collect_income_mission = {
		allowed = { original_tag = FRA }
		icon = political_actions
		available = {
			always = no
		}
		activation = {
			has_completed_focus = FRA_french_space
		}
		is_good = yes
		days_mission_timeout = 365
		timeout_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision timeout FRA_collect_income_mission"
			country_event = france_md.12
		}
		fire_only_once = no
		days_remove = 0
	}
}

FRA_francosphere_decision_category = {
	FRA_decision_align_algeria = {
		allowed = { original_tag = FRA }
		icon = political_actions
		cost = 50
		available = {
			has_completed_focus = FRA_north_africa_sphere
		}
		visible = {
			has_completed_focus = FRA_africa_fr
		}
		days_remove = 60
		days_re_enable = 15

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision FRA_decision_align_algeria"
			ALG = {
				generic_grant_4_percent_influence_of_ROOT = yes
			}
		}

		ai_will_do = {
			factor = 10
		}
	}

	FRA_decision_align_morroco = {
		allowed = { original_tag = FRA }
		icon = political_actions
		cost = 50
		available = {
			has_completed_focus = FRA_north_africa_sphere
		}
		visible = {
			has_completed_focus = FRA_africa_fr
		}
		days_remove = 60
		days_re_enable = 15

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision FRA_decision_align_morroco"
			MOR = {
				generic_grant_4_percent_influence_of_ROOT = yes
			}
		}

		ai_will_do = {
			factor = 10
		}
	}

	FRA_decision_align_tunisia = {
		allowed = { original_tag = FRA }
		icon = political_actions
		cost = 50
		available = {
			has_completed_focus = FRA_north_africa_sphere
		}
		visible = {
			has_completed_focus = FRA_africa_fr
		}
		days_remove = 60
		days_re_enable = 15


		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision FRA_decision_align_tunisia"
			TUN = {
				generic_grant_4_percent_influence_of_ROOT = yes
			}
		}

		ai_will_do = {
			factor = 10
		}
	}

	FRA_decision_align_senegal = {
		allowed = { original_tag = FRA }
		icon = political_actions
		cost = 50
		available = {
			has_completed_focus = FRA_senegal
		}
		visible = {
			has_completed_focus = FRA_africa_fr
		}
		days_remove = 60
		days_re_enable = 15

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision FRA_decision_align_senegal"
			SEN = {
				generic_grant_4_percent_influence_of_ROOT = yes
			}
		}

		ai_will_do = {
			factor = 10
		}
	}

	FRA_decision_align_ivory_coast = {
		allowed = { original_tag = FRA }
		icon = political_actions
		cost = 50
		available = {
			has_completed_focus = FRA_ivory_coast
		}
		visible = {
			has_completed_focus = FRA_africa_fr
		}
		days_remove = 60
		days_re_enable = 15

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision FRA_decision_align_ivory_coast"
			CDI = {
				generic_grant_4_percent_influence_of_ROOT = yes
			}
		}

		ai_will_do = {
			factor = 10
		}
	}

	FRA_decision_align_madagascar = {
		allowed = { original_tag = FRA }
		icon = political_actions
		cost = 50
		available = {
			has_completed_focus = FRA_madagascar
		}
		visible = {
			has_completed_focus = FRA_africa_fr
		}
		days_remove = 60
		days_re_enable = 15


		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision FRA_decision_align_madagascar"
			MAD = {
				generic_grant_4_percent_influence_of_ROOT = yes
			}
		}

		ai_will_do = {
			factor = 10
		}
	}

	FRA_decision_align_lebanon = {
		allowed = { original_tag = FRA }
		icon = political_actions
		cost = 50
		available = {
			has_completed_focus = FRA_lebanon
		}
		visible = {
			has_completed_focus = FRA_the_middle_east
		}
		days_remove = 60
		days_re_enable = 15


		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision FRA_decision_align_lebanon"
			LEB = {
				generic_grant_4_percent_influence_of_ROOT = yes
			}
		}

		ai_will_do = {
			factor = 10
		}
	}

	FRA_decision_align_syria = {
		allowed = { original_tag = FRA }
		icon = political_actions
		cost = 50
		available = {
			has_completed_focus = FRA_syria
		}
		visible = {
			has_completed_focus = FRA_the_middle_east
		}
		days_remove = 60
		days_re_enable = 15


		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision FRA_decision_align_syria"
			SYR = {
				generic_grant_4_percent_influence_of_ROOT = yes
			}
		}

		ai_will_do = {
			factor = 10
		}
	}

	FRA_decision_align_indochina = {
		allowed = { original_tag = FRA }
		icon = political_actions
		cost = 100
		available = {
			has_completed_focus = FRA_indochine_francaise
		}
		visible = {
			has_completed_focus = FRA_asia
		}
		days_remove = 120
		days_re_enable = 30


		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision FRA_decision_align_indochina"
			VIE = {
				generic_grant_4_percent_influence_of_ROOT = yes
			}
			LAO = {
				generic_grant_4_percent_influence_of_ROOT = yes
			}
			CBD = {
				generic_grant_4_percent_influence_of_ROOT = yes
			}

		}

		ai_will_do = {
			factor = 10
		}
	}
}