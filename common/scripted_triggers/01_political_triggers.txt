#Scope is selected country
# Saved for convenience
# is_in_array = { ruling_party = 0  } #Western_Autocracy
# is_in_array = { ruling_party = 1  } #conservatism
# is_in_array = { ruling_party = 2  } #liberalism
# is_in_array = { ruling_party = 3  } #socialism

# is_in_array = { ruling_party = 4  } #Communist-State
# is_in_array = { ruling_party = 5  } #anarchist_communism
# is_in_array = { ruling_party = 6  } #Conservative
# is_in_array = { ruling_party = 7  } #Autocracy
# is_in_array = { ruling_party = 8  } #Mod_Vilayat_e_Faqih
# is_in_array = { ruling_party = 9  } #Vilayat_e_Faqih

# is_in_array = { ruling_party = 10 } #Kingdom
# is_in_array = { ruling_party = 11 } #Caliphate

# is_in_array = { ruling_party = 12 } #Neutral_Muslim_Brotherhood
# is_in_array = { ruling_party = 13 } #Neutral_Autocracy
# is_in_array = { ruling_party = 14 } #Neutral_conservatism
# is_in_array = { ruling_party = 15 } #oligarchism
# is_in_array = { ruling_party = 16 } #Neutral_Libertarian
# is_in_array = { ruling_party = 17 } #Neutral_green
# is_in_array = { ruling_party = 18 } #neutral_Social
# is_in_array = { ruling_party = 19 } #Neutral_Communism

# is_in_array = { ruling_party = 20 } #Nat_Populism
# is_in_array = { ruling_party = 21 } #Nat_Fascism
# is_in_array = { ruling_party = 22 } #Nat_Autocracy
# is_in_array = { ruling_party = 23 } #Monarchist

# is_in_array = { gov_coalition_array = 0  } #Western_Autocracy
# is_in_array = { gov_coalition_array = 1  } #conservatism
# is_in_array = { gov_coalition_array = 2  } #liberalism
# is_in_array = { gov_coalition_array = 3  } #socialism
# is_in_array = { gov_coalition_array = 4  } #Communist-State
# is_in_array = { gov_coalition_array = 5  } #anarchist_communism
# is_in_array = { gov_coalition_array = 6  } #Conservative
# is_in_array = { gov_coalition_array = 7  } #Autocracy
# is_in_array = { gov_coalition_array = 8  } #Mod_Vilayat_e_Faqih
# is_in_array = { gov_coalition_array = 9  } #Vilayat_e_Faqih
# is_in_array = { gov_coalition_array = 10 } #Kingdom
# is_in_array = { gov_coalition_array = 11 } #Caliphate
# is_in_array = { gov_coalition_array = 12 } #Neutral_Muslim_Brotherhood
# is_in_array = { gov_coalition_array = 13 } #Neutral_Autocracy
# is_in_array = { gov_coalition_array = 14 } #Neutral_conservatism
# is_in_array = { gov_coalition_array = 15 } #oligarchism
# is_in_array = { gov_coalition_array = 16 } #Neutral_Libertarian
# is_in_array = { gov_coalition_array = 17 } #Neutral_green
# is_in_array = { gov_coalition_array = 18 } #neutral_Social
# is_in_array = { gov_coalition_array = 19 } #Neutral_Communism
# is_in_array = { gov_coalition_array = 20 } #Nat_Populism
# is_in_array = { gov_coalition_array = 21 } #Nat_Fascism
# is_in_array = { gov_coalition_array = 22 } #Nat_Autocracy
# is_in_array = { gov_coalition_array = 23 } #Monarchist

#X Party is in Power
western_autocrats_are_in_power = {
	custom_trigger_tooltip = {
		tooltip = has_pro_western_autocrat_government_TT
		is_in_array = { ruling_party = 0 } #Western Autocracy
	}
}

western_conservatism_are_in_power = {
	custom_trigger_tooltip = {
		tooltip = has_conservatism_government_TT
		is_in_array = { ruling_party = 1 } #Western Conservatism
	}
}

western_liberals_are_in_power = {
	custom_trigger_tooltip = {
		tooltip = has_liberalism_government_TT
		is_in_array = { ruling_party = 2 } #Western Conservatism
	}
}

western_social_democrats_are_in_power = {
	custom_trigger_tooltip = {
		tooltip = has_socialism_government_TT
		is_in_array = { ruling_party = 3 }
	}
}

emerging_communist_state_are_in_power = {
	custom_trigger_tooltip = {
		tooltip = has_communist_state_government_TT
		is_in_array = { ruling_party = 4 }
	}
}

emerging_anarchist_communism_are_in_power = {
	custom_trigger_tooltip = {
		tooltip = has_anarchist_communism_government_TT
		is_in_array = { ruling_party = 5 }
	}
}

emerging_reactionaries_are_in_power = {
	custom_trigger_tooltip = {
		tooltip = has_conserv_reactionary_government_TT
		is_in_array = { ruling_party = 6 }
	}
}

emerging_autocracy_are_in_power = {
	custom_trigger_tooltip = {
		tooltip = has_autocracy_government_TT
		is_in_array = { ruling_party = 7 }
	}
}

emerging_moderate_shiite_are_in_power = {
	custom_trigger_tooltip = {
		tooltip = has_mod_vilayat_e_faqih_government_TT
		is_in_array = { ruling_party = 8 }
	}
}

emerging_hardline_shiite_are_in_power = {
	custom_trigger_tooltip = {
		tooltip = has_vilayat_e_faqih_government_TT
		is_in_array = { ruling_party = 9 }
	}
}

salafist_kingdom_are_in_power = {
	custom_trigger_tooltip = {
		tooltip = has_kingdom_government_TT
		is_in_array = { ruling_party = 10 }
	}
}

salafist_caliphate_are_in_power = {
	custom_trigger_tooltip = {
		tooltip = has_caliphate_government_TT
		is_in_array = { ruling_party = 11 }
	}
}

neutrality_neutral_muslim_brotherhood_are_in_power = {
	custom_trigger_tooltip = {
		tooltip = has_neutral_muslim_brotherhood_government_TT
		is_in_array = { ruling_party = 12 }
	}
}

neutrality_neutral_autocracy_are_in_power = {
	custom_trigger_tooltip = {
		tooltip = has_neutral_autocracy_government_TT
		is_in_array = { ruling_party = 13 }
	}
}

neutrality_neutral_conservatism_are_in_power = {
	custom_trigger_tooltip = {
		tooltip = has_neutral_conservatism_government_TT
		is_in_array = { ruling_party = 14 }
	}
}

neutrality_neutral_oligarch_are_in_power = {
	custom_trigger_tooltip = {
		tooltip = has_oligarchism_government_TT
		is_in_array = { ruling_party = 15 }
	}
}

neutrality_neutral_libertarians_are_in_power = {
	custom_trigger_tooltip = {
		tooltip = has_neutral_libertarian_government_TT
		is_in_array = { ruling_party = 16 }
	}
}

neutrality_neutral_green_are_in_power = {
	custom_trigger_tooltip = {
		tooltip = has_neutral_green_government_TT
		is_in_array = { ruling_party = 17 }
	}
}

neutrality_neutral_social_are_in_power = {
	custom_trigger_tooltip = {
		tooltip = has_neutral_social_government_TT
		is_in_array = { ruling_party = 18 }
	}
}

neutrality_neutral_communism_are_in_power = {
	custom_trigger_tooltip = {
		tooltip = has_neutral_communism_government_TT
		is_in_array = { ruling_party = 19 }
	}
}

nationalist_right_wing_populists_are_in_power = {
	custom_trigger_tooltip = {
		tooltip = has_nat_populism_TT
		is_in_array = { ruling_party = 20 }
	}
}

nationalist_fascist_are_in_power = {
	custom_trigger_tooltip = {
		tooltip = has_fascist_government_TT
		is_in_array = { ruling_party = 21 }
	}
}

nationalist_military_junta_are_in_power = {
	custom_trigger_tooltip = {
		tooltip = has_nat_autocracy_government_TT
		is_in_array = { ruling_party = 22 }
	}
}

nationalist_monarchists_are_in_power = {
	custom_trigger_tooltip = {
		tooltip = has_monarchist_government_TT
		is_in_array = { ruling_party = 23 }
	}
}

#X Party is not in Power
western_autocrats_are_not_in_power = {
	custom_trigger_tooltip = {
		tooltip = not_has_pro_western_autocrat_government_TT
		NOT = { is_in_array = { ruling_party = 0 } } #Western Autocracy
	}
}

western_conservatism_are_not_in_power = {
	custom_trigger_tooltip = {
		tooltip = not_has_conservatism_government_TT
		NOT = { is_in_array = { ruling_party = 1 } } #Western Conservatism
	}
}

western_liberals_are_not_in_power = {
	custom_trigger_tooltip = {
		tooltip = not_has_liberalism_government_TT
		NOT = { is_in_array = { ruling_party = 2 } } #Western Conservatism
	}
}

western_social_democrats_are_not_in_power = {
	custom_trigger_tooltip = {
		tooltip = not_has_socialism_government_TT
		NOT = { is_in_array = { ruling_party = 3 } }
	}
}

emerging_communist_state_are_not_in_power = {
	custom_trigger_tooltip = {
		tooltip = not_has_communist_state_government_TT
		NOT = { is_in_array = { ruling_party = 4 } }
	}
}

emerging_anarchist_communism_are_not_in_power = {
	custom_trigger_tooltip = {
		tooltip = not_has_anarchist_communism_government_TT
		NOT = { is_in_array = { ruling_party = 5 } }
	}
}

emerging_reactionaries_are_not_in_power = {
	custom_trigger_tooltip = {
		tooltip = not_has_conserv_reactionary_government_TT
		NOT = { is_in_array = { ruling_party = 6 } }
	}
}

emerging_autocracy_are_not_in_power = {
	custom_trigger_tooltip = {
		tooltip = not_has_autocracy_government_TT
		NOT = { is_in_array = { ruling_party = 7 } }
	}
}

emerging_moderate_shiite_are_not_in_power = {
	custom_trigger_tooltip = {
		tooltip = not_has_mod_vilayat_e_faqih_government_TT
		NOT = { is_in_array = { ruling_party = 8 } }
	}
}

emerging_hardline_shiite_are_not_in_power = {
	custom_trigger_tooltip = {
		tooltip = not_has_vilayat_e_faqih_government_TT
		NOT = { is_in_array = { ruling_party = 9 } }
	}
}

salafist_kingdom_are_not_in_power = {
	custom_trigger_tooltip = {
		tooltip = not_has_kingdom_government_TT
		NOT = { is_in_array = { ruling_party = 10 } }
	}
}

salafist_caliphate_are_not_in_power = {
	custom_trigger_tooltip = {
		tooltip = not_has_caliphate_government_TT
		NOT = { is_in_array = { ruling_party = 11 } }
	}
}

neutrality_neutral_muslim_brotherhood_are_not_in_power = {
	custom_trigger_tooltip = {
		tooltip = not_has_neutral_muslim_brotherhood_government_TT
		NOT = { is_in_array = { ruling_party = 12 } }
	}
}

neutrality_neutral_autocracy_are_not_in_power = {
	custom_trigger_tooltip = {
		tooltip = not_has_neutral_autocracy_government_TT
		NOT = { is_in_array = { ruling_party = 13 } }
	}
}

neutrality_neutral_conservatism_are_not_in_power = {
	custom_trigger_tooltip = {
		tooltip = not_has_neutral_conservatism_government_TT
		NOT = { is_in_array = { ruling_party = 14 } }
	}
}

neutrality_neutral_oligarch_are_not_in_power = {
	custom_trigger_tooltip = {
		tooltip = not_has_oligarchism_government_TT
		NOT = { is_in_array = { ruling_party = 15 } }
	}
}

neutrality_neutral_libertarians_are_not_in_power = {
	custom_trigger_tooltip = {
		tooltip = not_has_neutral_libertarian_government_TT
		NOT = { is_in_array = { ruling_party = 16 } }
	}
}

neutrality_neutral_green_are_not_in_power = {
	custom_trigger_tooltip = {
		tooltip = not_has_neutral_green_government_TT
		NOT = { is_in_array = { ruling_party = 17 } }
	}
}

neutrality_neutral_social_are_not_in_power = {
	custom_trigger_tooltip = {
		tooltip = not_has_neutral_social_government_TT
		NOT = { is_in_array = { ruling_party = 18 } }
	}
}

neutrality_neutral_communism_are_not_in_power = {
	custom_trigger_tooltip = {
		tooltip = not_has_neutral_communism_government_TT
		NOT = { is_in_array = { ruling_party = 19 } }
	}
}

nationalist_right_wing_populists_are_not_in_power = {
	custom_trigger_tooltip = {
		tooltip = not_has_nat_populism_TT
		NOT = { is_in_array = { ruling_party = 20 } }
	}
}

nationalist_fascist_are_not_in_power = {
	custom_trigger_tooltip = {
		tooltip = not_has_fascist_government_TT
		NOT = { is_in_array = { ruling_party = 21 } }
	}
}

nationalist_military_junta_are_not_in_power = {
	custom_trigger_tooltip = {
		tooltip = not_has_nat_autocracy_government_TT
		NOT = { is_in_array = { ruling_party = 22 } }
	}
}

nationalist_monarchists_are_not_in_power = {
	custom_trigger_tooltip = {
		tooltip = not_has_monarchist_government_TT
		NOT = { is_in_array = { ruling_party = 23 } }
	}
}

##Has X Type of Government
has_socialist_government = {
	custom_trigger_tooltip = {
		tooltip = has_socialist_government_TT
		OR = {
			is_in_array = { ruling_party = 3  } #socialism
			is_in_array = { ruling_party = 4  } #Communist-State
			is_in_array = { ruling_party = 5  } #anarchist_communism
			is_in_array = { ruling_party = 18 } #neutral_Social
			is_in_array = { ruling_party = 19 } #Neutral_Communism
		}
	}
}
has_communist_government = {
	custom_trigger_tooltip = {
		tooltip = has_communist_government_TT
		OR = {
			is_in_array = { ruling_party = 4  } #Communist-State
			is_in_array = { ruling_party = 5  } #anarchist_communism
			is_in_array = { ruling_party = 19 } #Neutral_Communism
		}
	}
}
has_environmentalist_government = {
	custom_trigger_tooltip = {
		tooltip = has_environmentalist_government_TT
		is_in_array = { ruling_party = 17 } #Neutral_green
	}
}
has_liberal_government = {
	custom_trigger_tooltip = {
		tooltip = has_liberal_government_TT
		OR = {
			is_in_array = { ruling_party = 2  } #liberalism
			is_in_array = { ruling_party = 16 } #Neutral_Libertarian
			is_in_array = { ruling_party = 17 } #Neutral_green
		}
	}
}
has_conservative_government = {
	custom_trigger_tooltip = {
		tooltip = has_conservative_government_TT
		OR = {
			is_in_array = { ruling_party = 0  } #Western_Autocracy
			is_in_array = { ruling_party = 1  } #conservatism
			is_in_array = { ruling_party = 6  } #Conservative
			is_in_array = { ruling_party = 7  } #Autocracy
			is_in_array = { ruling_party = 10 } #Kingdom
			is_in_array = { ruling_party = 11 } #Caliphate
			is_in_array = { ruling_party = 12 } #Neutral_Muslim_Brotherhood
			is_in_array = { ruling_party = 13 } #Neutral_Autocracy
			is_in_array = { ruling_party = 14 } #Neutral_conservatism
			is_in_array = { ruling_party = 15 } #oligarchism
			is_in_array = { ruling_party = 20 } #Nat_Populism
			is_in_array = { ruling_party = 21 } #Nat_Fascism
			is_in_array = { ruling_party = 22 } #Nat_Autocracy
			is_in_array = { ruling_party = 23 } #Monarchist
		}
	}
}
has_reactionary_government = {
	custom_trigger_tooltip = {
		tooltip = has_reactionary_government_TT
		OR = {
			is_in_array = { ruling_party = 0  } #Western_Autocracy
			is_in_array = { ruling_party = 7  } #Autocracy
			is_in_array = { ruling_party = 10 } #Kingdom
			is_in_array = { ruling_party = 11 } #Caliphate
			is_in_array = { ruling_party = 13 } #Neutral_Autocracy
			is_in_array = { ruling_party = 15 } #oligarchism
			is_in_array = { ruling_party = 20 } #Nat_Populism
			is_in_array = { ruling_party = 21 } #Nat_Fascism
			is_in_array = { ruling_party = 22 } #Nat_Autocracy
			is_in_array = { ruling_party = 23 } #Monarchist
		}
	}
}
has_autocratic_government = {
	custom_trigger_tooltip = {
		tooltip = has_autocratic_government_TT
		OR = {
			is_in_array = { ruling_party = 0  } #Western_Autocracy
			is_in_array = { ruling_party = 4  } #Communist-State
			is_in_array = { ruling_party = 9  } #Vilayat_e_Faqih
			is_in_array = { ruling_party = 10 } #Kingdom
			is_in_array = { ruling_party = 11 } #Caliphate
			is_in_array = { ruling_party = 13 } #Neutral_Autocracy
			is_in_array = { ruling_party = 15 } #oligarchism
			is_in_array = { ruling_party = 19 } #Neutral_Communism
			is_in_array = { ruling_party = 21 } #Nat_Fascism
			is_in_array = { ruling_party = 22 } #Nat_Autocracy
			is_in_array = { ruling_party = 23 } #Monarchist
		}
	}
}
has_totalitarian_government = {
	custom_trigger_tooltip = {
		tooltip = has_totalitarian_government_TT
		OR = {
			is_in_array = { ruling_party = 4 } # Communist State
			is_in_array = { ruling_party = 11 } # Caliphate
			is_in_array = { ruling_party = 21 } # Nat_Fascism
		}
	}
}

has_socialist_in_coalition = {
	custom_trigger_tooltip = {
		tooltip = has_socialist_in_coalition_TT
		OR = {
			is_in_array = { gov_coalition_array = 3  } #socialism
			is_in_array = { gov_coalition_array = 4  } #Communist-State
			is_in_array = { gov_coalition_array = 5  } #anarchist_communism
			is_in_array = { gov_coalition_array = 18 } #neutral_Social
			is_in_array = { gov_coalition_array = 19 } #Neutral_Communism
		}
	}
}
has_communist_in_coalition = {
	custom_trigger_tooltip = {
		tooltip = has_communist_in_coalition_TT
		OR = {
			is_in_array = { gov_coalition_array = 4  } #Communist-State
			is_in_array = { gov_coalition_array = 5  } #anarchist_communism
			is_in_array = { gov_coalition_array = 19 } #Neutral_Communism
		}
	}
}

has_environmentalist_in_coalition = {
	custom_trigger_tooltip = {
		tooltip = has_environmentalist_in_coalition_TT
		is_in_array = { gov_coalition_array = 17 } #Neutral_green
	}
}
has_liberal_in_coalition = {
	custom_trigger_tooltip = {
		tooltip = has_liberal_in_coalition_TT
		OR = {
			is_in_array = { gov_coalition_array = 2  } #liberalism
			is_in_array = { gov_coalition_array = 16 } #Neutral_Libertarian
			is_in_array = { gov_coalition_array = 17 } #Neutral_green
		}
	}
}
has_conservative_in_coalition = {
	custom_trigger_tooltip = {
		tooltip = has_conservative_in_coalition_TT
		OR = {
			is_in_array = { gov_coalition_array = 0  } #Western_Autocracy
			is_in_array = { gov_coalition_array = 1  } #conservatism
			is_in_array = { gov_coalition_array = 6  } #Conservative
			is_in_array = { gov_coalition_array = 7  } #Autocracy
			is_in_array = { gov_coalition_array = 10 } #Kingdom
			is_in_array = { gov_coalition_array = 11 } #Caliphate
			is_in_array = { gov_coalition_array = 12 } #Neutral_Muslim_Brotherhood
			is_in_array = { gov_coalition_array = 13 } #Neutral_Autocracy
			is_in_array = { gov_coalition_array = 14 } #Neutral_conservatism
			is_in_array = { gov_coalition_array = 15 } #oligarchism
			is_in_array = { gov_coalition_array = 20 } #Nat_Populism
			is_in_array = { gov_coalition_array = 21 } #Nat_Fascism
			is_in_array = { gov_coalition_array = 22 } #Nat_Autocracy
			is_in_array = { gov_coalition_array = 23 } #Monarchist
		}
	}
}
has_reactionary_in_coalition = {
	custom_trigger_tooltip = {
		tooltip = has_reactionary_in_coalition_TT
		OR = {
			is_in_array = { gov_coalition_array = 0  } #Western_Autocracy
			is_in_array = { gov_coalition_array = 7  } #Autocracy
			is_in_array = { gov_coalition_array = 10 } #Kingdom
			is_in_array = { gov_coalition_array = 11 } #Caliphate
			is_in_array = { gov_coalition_array = 13 } #Neutral_Autocracy
			is_in_array = { gov_coalition_array = 15 } #oligarchism
			is_in_array = { gov_coalition_array = 20 } #Nat_Populism
			is_in_array = { gov_coalition_array = 21 } #Nat_Fascism
			is_in_array = { gov_coalition_array = 22 } #Nat_Autocracy
			is_in_array = { gov_coalition_array = 23 } #Monarchist
		}
	}
}
has_autocratic_in_coalition = {
	custom_trigger_tooltip = {
		tooltip = has_autocratic_in_coalition_TT
		OR = {
			is_in_array = { gov_coalition_array = 0  } #Western_Autocracy
			is_in_array = { gov_coalition_array = 4  } #Communist-State
			is_in_array = { gov_coalition_array = 9  } #Vilayat_e_Faqih
			is_in_array = { gov_coalition_array = 10 } #Kingdom
			is_in_array = { gov_coalition_array = 11 } #Caliphate
			is_in_array = { gov_coalition_array = 13 } #Neutral_Autocracy
			is_in_array = { gov_coalition_array = 15 } #oligarchism
			is_in_array = { gov_coalition_array = 19 } #Neutral_Communism
			is_in_array = { gov_coalition_array = 21 } #Nat_Fascism
			is_in_array = { gov_coalition_array = 22 } #Nat_Autocracy
			is_in_array = { gov_coalition_array = 23 } #Monarchist
		}
	}
}
has_totalitarian_in_coalition = {
	custom_trigger_tooltip = {
		tooltip = has_totalitarian_in_coalition_TT
		OR = {
			is_in_array = { gov_coalition_array = 4 } # Communist State
			is_in_array = { gov_coalition_array = 11 } # Caliphate
			is_in_array = { gov_coalition_array = 21 } # Nat_Fascism
		}
	}
}

has_socialist_governemnt_or_in_coalition = {
	custom_trigger_tooltip = {
		tooltip = has_socialist_government_or_in_coalition_TT
		OR = {
			is_in_array = { ruling_party = 3  } #socialism
			is_in_array = { ruling_party = 4  } #Communist-State
			is_in_array = { ruling_party = 5  } #anarchist_communism
			is_in_array = { ruling_party = 18 } #neutral_Social
			is_in_array = { ruling_party = 19 } #Neutral_Communism
			is_in_array = { gov_coalition_array = 3  } #socialism
			is_in_array = { gov_coalition_array = 4  } #Communist-State
			is_in_array = { gov_coalition_array = 5  } #anarchist_communism
			is_in_array = { gov_coalition_array = 18 } #neutral_Social
			is_in_array = { gov_coalition_array = 19 } #Neutral_Communism
		}
	}
}
has_communist_governemnt_or_in_coalition = {
	custom_trigger_tooltip = {
		tooltip = has_communist_government_or_in_coalition_TT
		OR = {
			is_in_array = { ruling_party = 4  } #Communist-State
			is_in_array = { ruling_party = 5  } #anarchist_communism
			is_in_array = { ruling_party = 19 } #Neutral_Communism
			is_in_array = { gov_coalition_array = 4  } #Communist-State
			is_in_array = { gov_coalition_array = 5  } #anarchist_communism
			is_in_array = { gov_coalition_array = 19 } #Neutral_Communism
		}
	}
}
has_environmentalist_governemnt_or_in_coalition = {
	custom_trigger_tooltip = {
		tooltip = has_environmentalist_government_or_in_coalition_TT
		OR = {
			is_in_array = { ruling_party = 17 } #Neutral_green
			is_in_array = { gov_coalition_array = 17 } #Neutral_green
		}
	}
}
has_liberal_governemnt_or_in_coalition = {
	custom_trigger_tooltip = {
		tooltip = has_liberal_government_or_in_coalition_TT
		OR = {
			is_in_array = { ruling_party = 2  } #liberalism
			is_in_array = { ruling_party = 16 } #Neutral_Libertarian
			is_in_array = { ruling_party = 17 } #Neutral_green
			is_in_array = { gov_coalition_array = 2  } #liberalism
			is_in_array = { gov_coalition_array = 16 } #Neutral_Libertarian
			is_in_array = { gov_coalition_array = 17 } #Neutral_green
		}
	}
}
has_conservative_governemnt_or_in_coalition = {
	custom_trigger_tooltip = {
		tooltip = has_conservative_governemnt_or_in_coalition_TT
		OR = {
			is_in_array = { ruling_party = 0  } #Western_Autocracy
			is_in_array = { ruling_party = 1  } #conservatism
			is_in_array = { ruling_party = 6  } #Conservative
			is_in_array = { ruling_party = 7  } #Autocracy
			is_in_array = { ruling_party = 10 } #Kingdom
			is_in_array = { ruling_party = 11 } #Caliphate
			is_in_array = { ruling_party = 12 } #Neutral_Muslim_Brotherhood
			is_in_array = { ruling_party = 13 } #Neutral_Autocracy
			is_in_array = { ruling_party = 14 } #Neutral_conservatism
			is_in_array = { ruling_party = 15 } #oligarchism
			is_in_array = { ruling_party = 20 } #Nat_Populism
			is_in_array = { ruling_party = 21 } #Nat_Fascism
			is_in_array = { ruling_party = 22 } #Nat_Autocracy
			is_in_array = { ruling_party = 23 } #Monarchist
			is_in_array = { gov_coalition_array = 0  } #Western_Autocracy
			is_in_array = { gov_coalition_array = 1  } #conservatism
			is_in_array = { gov_coalition_array = 6  } #Conservative
			is_in_array = { gov_coalition_array = 7  } #Autocracy
			is_in_array = { gov_coalition_array = 10 } #Kingdom
			is_in_array = { gov_coalition_array = 11 } #Caliphate
			is_in_array = { gov_coalition_array = 12 } #Neutral_Muslim_Brotherhood
			is_in_array = { gov_coalition_array = 13 } #Neutral_Autocracy
			is_in_array = { gov_coalition_array = 14 } #Neutral_conservatism
			is_in_array = { gov_coalition_array = 15 } #oligarchism
			is_in_array = { gov_coalition_array = 20 } #Nat_Populism
			is_in_array = { gov_coalition_array = 21 } #Nat_Fascism
			is_in_array = { gov_coalition_array = 22 } #Nat_Autocracy
			is_in_array = { gov_coalition_array = 23 } #Monarchist
		}
	}
}
has_reactionary_governemnt_or_in_coalition = {
	custom_trigger_tooltip = {
		tooltip = has_reactionary_governemnt_or_in_coalition_TT
		OR = {
			is_in_array = { ruling_party = 0  } #Western_Autocracy
			is_in_array = { ruling_party = 7  } #Autocracy
			is_in_array = { ruling_party = 10 } #Kingdom
			is_in_array = { ruling_party = 11 } #Caliphate
			is_in_array = { ruling_party = 13 } #Neutral_Autocracy
			is_in_array = { ruling_party = 15 } #oligarchism
			is_in_array = { ruling_party = 20 } #Nat_Populism
			is_in_array = { ruling_party = 21 } #Nat_Fascism
			is_in_array = { ruling_party = 22 } #Nat_Autocracy
			is_in_array = { ruling_party = 23 } #Monarchist
			is_in_array = { gov_coalition_array = 0  } #Western_Autocracy
			is_in_array = { gov_coalition_array = 7  } #Autocracy
			is_in_array = { gov_coalition_array = 10 } #Kingdom
			is_in_array = { gov_coalition_array = 11 } #Caliphate
			is_in_array = { gov_coalition_array = 13 } #Neutral_Autocracy
			is_in_array = { gov_coalition_array = 15 } #oligarchism
			is_in_array = { gov_coalition_array = 20 } #Nat_Populism
			is_in_array = { gov_coalition_array = 21 } #Nat_Fascism
			is_in_array = { gov_coalition_array = 22 } #Nat_Autocracy
			is_in_array = { gov_coalition_array = 23 } #Monarchist
		}
	}
}
has_autocratic_governemnt_or_in_coalition = {
	custom_trigger_tooltip = {
		tooltip = has_totalitarian_government_or_in_coalition_TT
		OR = {
			is_in_array = { ruling_party = 0  } #Western_Autocracy
			is_in_array = { ruling_party = 4  } #Communist-State
			is_in_array = { ruling_party = 9  } #Vilayat_e_Faqih
			is_in_array = { ruling_party = 10 } #Kingdom
			is_in_array = { ruling_party = 11 } #Caliphate
			is_in_array = { ruling_party = 13 } #Neutral_Autocracy
			is_in_array = { ruling_party = 15 } #oligarchism
			is_in_array = { ruling_party = 19 } #Neutral_Communism
			is_in_array = { ruling_party = 21 } #Nat_Fascism
			is_in_array = { ruling_party = 22 } #Nat_Autocracy
			is_in_array = { ruling_party = 23 } #Monarchist
			is_in_array = { gov_coalition_array = 0  } #Western_Autocracy
			is_in_array = { gov_coalition_array = 4  } #Communist-State
			is_in_array = { gov_coalition_array = 9  } #Vilayat_e_Faqih
			is_in_array = { gov_coalition_array = 10 } #Kingdom
			is_in_array = { gov_coalition_array = 11 } #Caliphate
			is_in_array = { gov_coalition_array = 13 } #Neutral_Autocracy
			is_in_array = { gov_coalition_array = 15 } #oligarchism
			is_in_array = { gov_coalition_array = 19 } #Neutral_Communism
			is_in_array = { gov_coalition_array = 21 } #Nat_Fascism
			is_in_array = { gov_coalition_array = 22 } #Nat_Autocracy
			is_in_array = { gov_coalition_array = 23 } #Monarchist
		}
	}
}
has_totalitarian_government_or_in_coalition = {
	custom_trigger_tooltip = {
		tooltip = has_totalitarian_government_or_in_coalition_TT
		OR = {
			is_in_array = { ruling_party = 4 } # Communist State
			is_in_array = { ruling_party = 11 } # Caliphate
			is_in_array = { ruling_party = 21 } # Nat_Fascism
			is_in_array = { gov_coalition_array = 4 } # Communist State
			is_in_array = { gov_coalition_array = 11 } # Caliphate
			is_in_array = { gov_coalition_array = 21 } # Nat_Fascism
		}
	}
}

# Factions
has_economic_faction = {
	custom_trigger_tooltip = {
		tooltip = has_economic_faction_TT
		OR = {
			has_idea = small_medium_business_owners
			has_idea = international_bankers
			has_idea = fossil_fuel_industry
			has_idea = industrial_conglomerates

			has_idea = landowners

			has_idea = maritime_industry
			has_idea = defense_industry

			has_idea = wall_street
			has_idea = chaebols
		}
	}
}
has_religious_faction = {
	custom_trigger_tooltip = {
		tooltip = has_religious_faction_TT
		OR = {
			has_idea = the_priesthood
			has_idea = The_Ulema
			has_idea = The_Clergy
			has_idea = wahabi_ulema
		}
	}
}