	Small Arms: done

        GER_infantry_weapons4:0 "HK 416"
        BEL_infantry_weapons4:0 "FN SCAR"
        ISR_infantry_weapons4:0 "TAR-21"
        SOV_infantry_weapons3:0 "AK-103" #2000
        USA_infantry_weapons4:0 "ACR"

	C&C Equip: done

        #China
        #Israel
        #Norway
        #USA

	APC: done

        AUS_APC_5:0 "Pandur II"
        CAN_APC_4:0 "LAV-III Bison"
        CHI_APC_5:0 "VP-10"
        FIN_APC_5:0 "Patria AMV"
        FRA_APC_5:0 "Véhicule à l'Avant Blindé MkII"
        GER_APC_6:0 "GTK Boxer"
        SAF_APC_5:0 "Paramount Mbombe"
        SIN_APC_5:0 "Terrex AV-81"
        SOV_APC_5:0 "BTR-90"
        SWI_APC_5:0 "Mowag Piranha V"
        TUR_APC_5:0 "FNSS Pars"
        UKR_APC_5:0 "BTR-4"

	IFV: done

        CHI_IFV_5:0 "ZBD-08"
        FIN_IFV_5:0 "Patria AMV P2"
        FRA_IFV_5:0 "Véhicule Blindé de Combat d'Infanterie"
        GER_IFV_6:0 "Puma 1"
        ITA_IFV_5:0 "Freccia"
        KOR_IFV_5:0 "K21"
        SER_IFV_5:0 "Lazar III"
        SIN_IFV_5:0 "Bionix II"
        SPR_IFV_5:0 "ASCOD Pizarro"
        SWE_IFV_5:0 "Combat Vehicle 90 MkIII"

	Anti-Air: done

        CHI_Anti_Air_2:0 "FN-6"
        ENG_Anti_Air_2:0 "Starstreak HVM"
        SOV_Anti_Air_2:0 "SA-24 Igla-S"
        USA_Anti_Air_2:0 "FIM-92E Stinger"

	Anti Tank: done

        CHI_Anti_tank_2:0 "HJ-12"
        ENG_Anti_tank_2:0 "MBT LAW"
        FRA_Anti_tank_2:0 "Missile Moyenne Portée"
        ISR_Anti_tank_2:0 "Spike-MR"
        SOV_Anti_tank_2:0 "AT-13 Metis-M"
        USA_Anti_tank_2:0 "FGM-148 Javelin"

	Heavy Anti Tank: done

        CHI_Heavy_Anti_tank_2:0 "HJ-10"
        FRA_Heavy_Anti_tank_2:0 "PARS 3 LR"
        ISR_Heavy_Anti_tank_2:0 "Spike-LR"
        PER_Heavy_Anti_tank_2:0 "Dehlavie"
        SAF_Heavy_Anti_tank_2:0 "ZT3 Iklwa"
        SOV_Heavy_Anti_tank_2:0 "AT-14 Kornet"

	Artillery: done

        CHI_artillery_2:0 "WAC-21"
        FIN_artillery_2:0 "155 K 98"
        FRA_artillery_2:0 "GIAT LG1"
        ISR_artillery_2:0 "ATHOS 2052"
        SAF_artillery_2:0 "G8" ?
        SER_artillery_2:0 "M-08"
        SIN_artillery_2:0 "SLWH Pegasus"
        USA_artillery_2:0 "M777 Howitzer"

	#Armored

	Recon tank: done

        AUS_Rec_tank_2:0 "Ulan LT 105"
        BRA_Rec_tank_2:0 "VBR-MR Guaraní"
        CHI_Rec_tank_2:0 "ZTD-05"
        ENG_Rec_tank_2:0 "FV107 Scimitar"
        ITA_Rec_tank_2:0 "B1 Centauro"
        POL_Rec_tank_3:0 "PL-1"
        SAF_Rec_tank_2:0 "Rooikat 76"
        SWE_Rec_tank_2:0 "CV-90 105"

	MBT:

        FRA_MBT_4:0 "AMX-56 Leclerc"
        GER_MBT_4:0 "Leopard 2A4"
        KOR_MBT_5:0 "K2 Black Panther"
        SOV_MBT_4:0 "T-90"
        USA_MBT_4:0 "M1A2 Abrams"


	SP-Arty:

        CHI_SP_arty_2:0 "PLZ-05"
        FRA_SP_arty_2:0 "CAESAR"
        GER_SP_arty_2:0 "Panzerhaubitze 2000"
        KOR_SP_arty_2:0 "K9 Thunder"
        SER_SP_arty_2:0 "MGS-25 Aleksandar"
        SOV_SP_arty_2:0 "2S19M2 Msta-SM"
        SWE_SP_arty_2:0 "FH77BW L52 Archer"
        USA_SP_arty_2:0 "M109A6 Paladin"

	SP-Rocket Artillery:

        CHI_SP_R_arty_2:0 "PHZ-10"
        PER_SP_R_arty_2:0 "Fajr-5"
        POL_SP_R_arty_2:0 "WR-40 Langusta"
        SAF_SP_R_arty_2:0 "Valkiri Mk II MARS Bataleur"
        SOV_SP_R_arty_2:0 "9A52-4 Tornado"
        TUR_SP_R_arty_2:0 "TR-300"
        USA_SP_R_arty_2:0 "M270A1"
        BRA_SP_R_arty_2:0 "Astros 2020"

	SP-AA:

        CHI_SP_Anti_Air_2:0 "HQ-17"
        GER_SP_Anti_Air_2:0 "IRIS-T SL"
        NOR_SP_Anti_Air_2:0 "NASAMS"
        SOV_SP_Anti_Air_2:0 "SA-19 Tunguska"

	Country list, alphabetical: 25 countries

		austria
		belgium
		brazil
		canada
		china
		england
		finland
		france
		germany
		iran
		israel
		italy
		korea
		norway
		poland
		russia
		serbia
		singapore
		south africa
		spain
		sweden
		switzerland
		turkey
		ukraine
		usa

	My notes on the subject

							western block and neutrals				eastern block						1200 guns for every purchase
		infantry equi_2		custom_cost_text = cost_0_1				custom_cost_text = cost_0_08
		infantry equi_3		custom_cost_text = cost_0_125			custom_cost_text = cost_0_1
		infantry equi_4		custom_cost_text = cost_0_15			custom_cost_text = cost_0_12
		infantry equi_5		custom_cost_text = cost_0_175			custom_cost_text = cost_0_14
		infantry equi_6		custom_cost_text = cost_0_2				custom_cost_text = cost_0_16
		infantry equi_7		custom_cost_text = cost_0_25			custom_cost_text = cost_0_2






