﻿#########################################
Using Internal Factions v2.0 (Angriest Bird/Birdy)
#########################################

Credit for the Orginial System go to Killerrabbit.

Welcome to the Using Internal Faction v2.0! 

The system has undergone a large backend change and has cleaned up some of its coding to allow for a more seamless transition. The effect remain almost entirely the same as they did before. Meaning the way they are interacted with is the same as before.

Changes in v2.0:
-Modifiers are now dynamically scripted. Meaning they are adjusted based on opinion of the faction now rather then remaining stagnant. 
-Opinions are now clearly visible
-Localization has been updated with grammar fixes and clear concise explanation of the faction

The following is if you are setting it in init:
###### Increasing or decreasing opinion #######
There are two main effects to use:

decrease_internal_faction_opinion = yes #decreases internal faction opinion
increase_internal_faction_opinion = yes #increases internal faction opinion

They update everything needed for any internal factions, and include scripts to show the changes visually for the player. The dynamic modifiers are included within these two functions so there is nothing additional you need to do here with these. DO NOT TRY TO ADD TO THE VARIABLES. The code will break and it will not work well anymore! Its just a small thing that happens when you mess with them. 

Now the increase_internal_faction_opinion and decrease_internal_faction_opinion need to know what exact faction that is in order to raise or lower their opinion.

To do this, use an command to set flag(s) first:

set_country_flag = current_small_medium_business_owners
set_country_flag = current_the_donju
set_country_flag = current_the_bazaar
set_country_flag = current_international_bankers
set_country_flag = current_wall_street
increase_internal_faction_opinion = yes

This will make it increase those opinions (if they are active) and list the changes for the player. It will not change opinions of non-active factions.
increase_internal_faction_opinion will auto-delete current_factionname type flags, so no need to do that afterwards. This is also possible:

set_country_flag = current_international_bankers
increase_internal_faction_opinion = yes
set_country_flag = current_labour_unions
decrease_internal_faction_opinion = yes

It will increase bankers opinions and decrease labor union opinions.

#Available factions:
set_country_flag = current_small_medium_business_owners
set_country_flag = current_the_donju
set_country_flag = current_the_bazaar
set_country_flag = current_IRGC
set_country_flag = current_foreign_jihadis
set_country_flag = current_iranian_quds_force
set_country_flag = current_saudi_royal_family
set_country_flag = current_wahabi_ulema
set_country_flag = current_the_priesthood
set_country_flag = current_The_Clergy
set_country_flag = current_The_Ulema
set_country_flag = current_communist_cadres
set_country_flag = current_farmers
set_country_flag = current_landowners
set_country_flag = current_labour_unions
set_country_flag = current_VEVAK
set_country_flag = current_isi_pakistan
set_country_flag = current_intelligence_community
set_country_flag = current_the_military
set_country_flag = current_defense_industry
set_country_flag = current_maritime_industry
set_country_flag = current_oligarchs
set_country_flag = current_chaebols
set_country_flag = current_industrial_conglomerates
set_country_flag = current_fossil_fuel_industry
set_country_flag = current_wall_street
set_country_flag = current_international_bankers

###### Using opinions as triggers #######

Each active internal faction has it's own opinion-flags.

hostile_
negative_
indifferent_
positive_
enthusiastic_

Is the 5 possible prefixes. Use it like this:

has_country_flag = hostile_wall_street

I will probably add generalized checks that can look for any hostile etc active faction in the future. 
Or functions that returns a flagvalue of overall active faction's happiness. 

##### Having opinion change in different event options ######

Since the game does not know which internal faction to change until a set_country_flag = current_ has been set, if this flag is set in a option, it will not display a tooltip since the effect has not processed yet!

To remedy this, use: 
visually_display_opinion_fall_faction_name = yes
Example: visually_display_opinion_fall_small_medium_business_owners = yes

For a rise use:
visually_display_opinion_rise_small_medium_business_owners = yes

Example of use (together with sanity check of actually having the faction):

if = { 
	limit = { has_idea = small_medium_business_owners }
	visually_display_opinion_fall_small_medium_business_owners = yes
	set_country_flag = current_small_medium_business_owners
	decrease_internal_faction_opinion = yes
}

If you are just affecting a single internal faction, or treat all affected factions the same way; the current_ flag can just be set in immediate, and then having increases or decreases in the event options. 

So the general idea with the new and improved system is the intial way of conducting business and rising and lowering opinions all remain much the same. The effects are still in the same five tiered system. Therefore, the main effects have not changed while interacting with the system. The only thing now is that effects dynamically changed based on whatever x level of popularity you have with that group. It is in an effort to ensure the internal factions are not looked over and more important to the country.
