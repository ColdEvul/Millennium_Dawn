﻿capital = 4

oob = "AUS_2000"

set_convoys = 150
set_stability = 0.5

set_country_flag = country_language_german

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_stagnation
	austrian_neutrality
	idea_eu_member
	free_trade
	limited_conscription
}

set_politics = {

	parties = {
		reactionary = {
			popularity = 27
		}
		conservative = {
			popularity = 27
		}
		market_liberal = {
			popularity = 4
		}
		social_democrat = {
			popularity = 33
		}
		progressive = {
			popularity = 8
		}
		communist = {
			popularity = 1
		}
	}
	
	ruling_party = social_democrat
	last_election = "1999.10.3"
	election_frequency = 48
	elections_allowed = yes
}

create_country_leader = {
	name = "Viktor Klima"
	picture = "Viktor_Klima.dds"
	ideology = social_democrat_ideology
}
create_country_leader = {
	name = "Wolfgang Schüssel"
	picture = "Wolfgang_Schussel.dds"
	ideology = christian_democrat 
}
create_country_leader = {
	name = "Susanne Riess"
	picture = "Susanne_Riess.dds"
	ideology = counter_progressive_democrat
}
create_country_leader = {
	name = "Christian Köck"
	picture = "Christian_Kock.dds"
	ideology = libertarian
}
create_country_leader = {
	name = "Alexander Van Der Bellen"
	picture = "Alexander_Van_Der_Bellen.dds"
	ideology = green
}
create_country_leader = {
	name = "Sonja Grusch"
	picture = "Sonja_Grusch.dds"
	ideology = democratic_socialist_ideology
}
create_country_leader = {
	name = "Walter Baier"
	picture = "Walter_Baier.dds"
	ideology = marxist
}
create_country_leader = {
	name = "Karl II"
	picture = "Karl_II.dds"
	ideology = absolute_monarchist
}

add_namespace = {
	name = "aus_unit_leader"
	type = unit_leader
}

create_field_marshal = {
	name = "Othmar Commenda"
	picture = "generals/Othmar_Commenda.dds"
	traits = { old_guard }
	skill = 2
}

create_field_marshal = {
	name = "Horst Hofer"
	picture = "generals/Horst_Hofer.dds"
	traits = { }
	skill = 1
}

create_corps_commander = {
	name = "Jürgen Wörgötter"
	picture = "generals/Juergen_Woergoetter.dds"
	traits = { hill_fighter }
	skill = 3
}

create_corps_commander = {
	name = "Franz Reissner"
	picture = "generals/Franz_Reissner.dds"
	traits = { trickster }
	skill = 1
}

create_corps_commander = {
	name = "Dieter Heidecker"
	picture = "generals/Dieter_Heidecker.dds"
	traits = { fortress_buster }
	skill = 1
}

create_corps_commander = {
	name = "Othmar Wohlkönig"
	picture = "generals/Othmar_Wohlkoenig.dds"
	traits = {  }
	skill = 2
}

create_corps_commander = {
	name = "Heinrich Winkelmayer"
	picture = "generals/Heinrich_Winkelmayer.dds"
	traits = { urban_assault_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Gerhard Christiner"
	picture = "generals/Gerhard_Christiner.dds"
	traits = { }
	skill = 1
}

create_corps_commander = {
	name = "Christian Platzner"
	picture = "generals/Christian_Platzner.dds"
	traits = { }
	skill = 1
}

create_corps_commander = {
	name = "Karl Gruber"
	picture = "generals/Karl_Gruber.dds"
	traits = { }
	skill = 1
}

create_corps_commander = {
	name = "Hermann Kaponig"
	picture = "generals/Hermann_Kaponig.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Rudolf Striedinger"
	picture = "generals/Rudolf_Striedinger.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Edwin Potocnik"
	picture = "generals/Edwin_Potocnik.dds"
	traits = {  }
	skill = 2
}

create_corps_commander = {
	name = "Christian Habersatter"
	picture = "generals/Christian_Habersatter.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Christian Riemer"
	picture = "generals/Christian_Riemer.dds"
	traits = { panzer_leader }
	skill = 1
}
	
create_corps_commander = {
	name = "Peter Grünwald"
	picture = "generals/Peter_Gruenwald.dds"
	traits = { hill_fighter }
	skill = 1
}

2002.1.1 = {
	add_ideas = the_euro
}

2012.10.17 = {
	set_party_name = {
		ideology = market_liberal
		long_name = AUS_market_liberal_party_2012_long
		name = AUS_market_liberal_party_2012
	}
}

2016.1.1 = {
	create_country_leader = {
		name = "Christian Kern"
		picture = "Christian_Kern.dds"
		ideology = social_democrat_ideology
	}
	create_country_leader = {
		name = "Reinhold Mitterlehner"
		picture = "Reinhold_Mitterlehner.dds"
		ideology = christian_democrat 
	}
	create_country_leader = {
		name = "Heinz Christian Strache"
		picture = "Heinz_Christian_Strache.dds"
		ideology = counter_progressive_democrat
	}
	create_country_leader = {
		name = "Matthias Strolz"
		picture = "Matthias_Strolz.dds"
		ideology = libertarian
	}
	create_country_leader = {
		name = "Eva Glawischnig"
		picture = "Eva_Glawischnig.dds"
		ideology = green
	}
}
2017.1.1={

oob = "AUS_2017"

set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1

	Early_APC = 1
	
	IFV_1 = 1
	IFV_2 = 1
	IFV_3 = 1
	IFV_4 = 1
	IFV_5 = 1
	
	Rec_tank_0 = 1
	Rec_tank_1 = 1
	Rec_tank_2 = 1

	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	
	 #2005
	
	combat_eng_equipment = 1
	
	night_vision_1 = 1
	night_vision_2 = 1
	night_vision_3 = 1 #1985
	
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1 #2005
	command_control_equipment4 = 1 #2015
	
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	
	APC_1 = 1
	APC_2 = 1
	APC_3 = 1
	APC_4 = 1
	APC_5 = 1

	MBT_1 = 1
	
	ENG_MBT_1 = 1
	
	util_vehicle_equipment_0 = 1
	
	gw_artillery = 1
	SP_arty_equipment_0 = 1
	
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	
	
	landing_craft = 1
	

}

add_ideas = {
	pop_050

	The_Clergy
	small_medium_business_owners
	Labour_Unions
	christian
	modest_corruption
	gdp_9
	EU_member
	    stable_growth
		defence_01
	edu_05
	health_05
	social_06
	bureau_02
	police_03
	draft_army
	volunteer_women
	western_country
	large_far_right_movement
	labour_unions
	small_medium_business_owners
	landowners
	civil_law
	}
set_country_flag = gdp_9
set_country_flag = negative_labour_unions
set_country_flag = positive_small_medium_business_owners
set_country_flag = positive_landowners

#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_15K_GDPC_slot
	complete_national_focus = Generic_30K_GDPC_slot
	complete_national_focus = Generic_4_IC_slot
	complete_national_focus = Generic_8_IC_slot

	set_politics = {

	parties = {
		democratic = { 
			popularity = 63
		}

		fascism = {
			popularity = 0
		}
		
		communism = {
			popularity = 4
		}
		
		neutrality = {
			popularity = 12
		}
		
		nationalist = {
			popularity = 21
		}
	}
	
	ruling_party = democratic
	last_election = "2013.9.29"
	election_frequency = 60
	elections_allowed = yes
}

create_country_leader = {
	name = "Reinhold Mitterlehner"
	desc = "POLITICS_WILLIAM_DUDLEY_PELLEY_DESC"
	picture = "reinhold_mitterlehner.dds"
	expire = "2065.1.1"
	ideology = conservatism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Matthias Strolz"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "matthias_strolz.dds"
	expire = "2065.1.1"
	ideology = liberalism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Frank Stronach"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "frank_stronach.dds"
	expire = "2065.1.1"
	ideology = Conservative
	traits = {
		#
	}
}

create_country_leader = {
	name = "Eva Glawischnig"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "eva_glawischnig.dds"
	expire = "2065.1.1"
	ideology = Neutral_green
	traits = {
		#
	}
}

create_country_leader = {
	name = "Heinz-Christian Strache"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "heinz_christian_strache.dds"
	expire = "2065.1.1"
	ideology = Nat_Fascism
	traits = {
		#
	}
}


create_country_leader = {
	name = "Christian Kern"
	desc = "POLITICS_EARL_BROWDER_DESC"
	picture = "Christian_Kern.dds"
	expire = "2065.1.1"
	ideology = socialism
	traits = {
		#
	}
}
create_field_marshal = {
	name = "Othmar Commenda"
	picture = "Portrait_Othmar_Commenda.dds"
	traits = { old_guard organisational_leader }
	skill = 4
}

create_corps_commander = {
	name = "Horst Hofer"
	picture = "Portrait_Horst_Hofer.dds"
	traits = { commando }
	skill = 1
}

create_corps_commander = {
	name = "Franz Reissner"
	picture = "Portrait_Franz_Reissner.dds"
	traits = { trickster }
	skill = 3
}

create_corps_commander = {
	name = "Dieter Heidecker"
	picture = "Portrait_Dieter_Heidecker.dds"
	traits = { fortress_buster }
	skill = 2
}

create_corps_commander = {
	name = "Othmar Wohlkönig"
	picture = "Portrait_Othmar_Wohlkoenig.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Heinrich Winkelmayer"
	picture = "Portrait_Heinrich_Winkelmayer.dds"
	traits = { urban_assault_specialist }
	skill = 2
}

create_corps_commander = {
	name = "Gerhard Christiner"
	picture = "Portrait_Gerhard_Christiner.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Christian Platzner"
	picture = "Portrait_Christian_Platzner.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Karl Gruber"
	picture = "Portrait_Karl_Gruber.dds"
	traits = { commando }
	skill = 1
}

create_corps_commander = {
	name = "Hermann Kaponig"
	picture = "Portrait_Hermann_Kaponig.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Rudolf Striedinger"
	picture = "Portrait_Rudolf_Striedinger.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Edwin Potocnik"
	picture = "Portrait_Edwin_Potocnik.dds"
	traits = {  }
	skill = 2
}

create_corps_commander = {
	name = "Christian Habersatter"
	picture = "Portrait_Christian_Habersatter.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Christian Riemer"
	picture = "Portrait_Christian_Riemer.dds"
	traits = { panzer_leader }
	skill = 1
}
	
create_corps_commander = {
	name = "Peter Grünwald"
	picture = "Portrait_Peter_Gruenwald.dds"
	traits = { hill_fighter }
	skill = 1
}

create_corps_commander = {
	name = "Jürgen Wörgötter"
	picture = "Portrait_Juergen_Woergoetter.dds"
	traits = { hill_fighter }
	skill = 1
}

}