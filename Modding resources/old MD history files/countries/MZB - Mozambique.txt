﻿capital = 544

oob = "MOZ_2000"

set_convoys = 120
set_stability = 0.5

set_country_flag = country_language_portuguese

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_explosion
	african_union_member
	commonwealth_of_nations_member
}

set_politics = {

	parties = {
		fascist = {
			popularity = 1
		}
		nationalist = {
			popularity = 1
		}
		reactionary = {
			popularity = 37
		}
		conservative = {
			popularity = 4
		}
		market_liberal = {
			popularity = 2
		}
		social_liberal = {
			popularity = 3
		}
		social_democrat = {
			popularity = 3
		}
		democratic_socialist = {
			popularity = 49
		}

	}
	
	ruling_party = democratic_socialist
	last_election = "1999.12.5"
	election_frequency = 60
	elections_allowed = yes
}

create_country_leader = {
	name = "Joaquim Chissano"
	picture = "Joaquim_Chissano.dds"
	ideology = democratic_socialist_ideology
}

create_country_leader = {
	name = "Alfonso Dhlakama"
	picture = "Alfonso_Dhlakama.dds"
	ideology = counter_progressive_democrat
}

create_country_leader = {
	name = "Raul Domingos"
	picture = "Raul_Domingos.dds"
	ideology = moderate
}

create_country_leader = {
	name = "Daviz Simango"
	picture = "Daviz_Simango.dds"
	ideology = christian_democrat
}

create_country_leader = {
	name = "Armando Sapembe"
	ideology = green
}

create_country_leader = {
	name = "Casimiro Nhamithambo"
	ideology = social_democrat_ideology
}

create_corps_commander = {
	name = "Graca Chongo"
	picture = "generals/Graca_Chongo.dds"
	skill = 1
}


2014.10.15 = {
	set_politics = {
		parties = {
			fascist = {
				popularity = 1
			}
			nationalist = {
				popularity = 1
			}
			reactionary = {
				popularity = 32
			}
			conservative = {
				popularity = 8
			}
			social_liberal = {
				popularity = 1
			}
			social_democrat = {
				popularity = 1
			}
			democratic_socialist = {
				popularity = 56
			}

		}
		
		ruling_party = democratic_socialist
		last_election = "2014.10.15"
		election_frequency = 60
		elections_allowed = yes
	}
	create_country_leader = {
		name = "Felipe Nyusi"
		picture = "Felipe_Nyusi.dds"
		ideology = democratic_socialist_ideology
	}
}