﻿capital = 816

oob = "MAY_2000"

set_convoys = 350
set_stability = 0.5

set_country_flag = country_language_malay

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	AT_upgrade_1 = 1
	Heavy_Anti_tank_1 = 1
	Anti_tank_1 = 1 
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_rapid
	limited_conscription
	commonwealth_of_nations_member
}

set_country_flag = dominant_religion_islam
set_country_flag = sunni_islam

set_politics = {
	parties = {
		islamist = {
		popularity = 5
		}
		nationalist = {
		popularity = 0
		}
		reactionary = {
		popularity = 5
		}
		conservative = {
		popularity = 45
		}
		market_liberal = {
		popularity = 10
		}
		social_liberal = {
		popularity = 30
		}
		social_democrat = {
		popularity = 5
		}
		progressive = {
		popularity = 0
		}
		democratic_socialist = {
		popularity = 0
		}
		communist = {
		popularity = 0
		}
	}
	ruling_party = conservative
	last_election = "1999.11.29"
	election_frequency = 48
	elections_allowed = yes
}

create_country_leader = {
	name = "Mohd Nasir Hashim"
	picture = "Mohd_Nasir_Hashim.dds"
	ideology = democratic_socialist_ideology
}

create_country_leader = {
	name = "Syed Husin Ketuai"
	picture = "Syed_Husin_Ketuai.dds"
	ideology = progressive_ideology
}

create_country_leader = {
	name = "Mahathir Mohamad"
	picture = "Mahathir_Mohamad.dds"
	ideology = fiscal_conservative
}

create_country_leader = {
	name = "Abdul Taib Mahmud"
	picture = "Abdul_Taib_Mahmud.dds"
	ideology = counter_progressive_democrat
}

create_country_leader = {
	name = "Fadzil Noor"
	picture = "Fadzil_Noor.dds"
	ideology = islamic_republican
}

create_country_leader = {
	name = "Lim Kit Siang"
	picture = "Lim_Kit_Siang.dds"
	ideology = social_democrat_ideology
}

create_country_leader = {
	name = "Wan Azizah Wan Ismail"
	picture = "Wan_Azizah_Wan_Ismail.dds"
	ideology = liberalist
}

create_country_leader = {
	name = "Lim Keng Yaik"
	picture = "Lim_Keng_Yaik.dds"
	ideology = libertarian
}

create_country_leader = {
	name = "Joseph Kurup"
	picture = "Joseph_Kurup.dds"
	ideology = national_democrat
}

create_corps_commander = {
	name = "Zulkifeli Mohd Zin"
	picture = "Zulkifeli_Mohd_Zin.dds"
	skill = 2
}

create_corps_commander = {
	name = "Raja Mohamed Affandi Raja Mohamed Noor"
	picture = "Raja_Mohamed.dds"
	skill = 1
}

create_corps_commander = {
	name = "Zulkifi Zainal Abidin"
	picture = "Zulkifli_Zainal_Abidin.dds"
	skill = 1
}

create_navy_leader = {
	name = "Dato Anuwi"
	picture = "admirals/Dato_Anuwi.dds"
	skill = 1
}

2013.5.5 = {

	add_ideas = {
		volunteer_only
	}

	set_politics = {

		parties = {
			islamist = {
			popularity = 15
			}
			nationalist = {
			popularity = 5
			}
			reactionary = {
			popularity = 5
			}
			conservative = {
			popularity = 30
			}
			market_liberal = {
			popularity = 10
			}
			social_liberal = {
			popularity = 15
			}
			social_democrat = {
			popularity = 20
			}
			progressive = {
			popularity = 0
			}
			democratic_socialist = {
			popularity = 0
			}
			communist = {
			popularity = 0
			}
		}

	ruling_party = conservative
	last_election = "2013.5.5"
	election_frequency = 48
	elections_allowed = yes
}

create_country_leader = {
	name = "Najib Razak"
	desc = "POLITICIAN_MAY_NAJIB_RAZAK_DESC"
	picture = "Najib_Razak.dds"
	expire = "2020.1.1"
	ideology = fiscal_conservative
	
	traits = {
	
	}
}

create_country_leader = {
	name = "Adenan Satem"
	desc = "POLITICIAN_MAY_ADENAN_SATEM_DESC"
	picture = "Adenan_Saten.dds"
	expire = "2020.1.1"
	ideology = counter_progressive_democrat
	
	traits = {
	
	}
}

create_country_leader = {
	name = "Abdul Hadi Awang"
	desc = "POLITICIAN_MAY_ABDUL_HADI_AWANG_DESC"
	picture = "Abdul_Hadi_Awang.dds"
	expire = "2020.1.1"
	ideology = islamic_republican
	
	traits = {
	
	}
}

create_country_leader = {
	name = "Mah Siew Keong"
	desc = "POLITICIAN_MAY_MAH_SIEW_KEONG_DESC"
	picture = "Mah_Siew_Keong.dds"
	expire = "2020.1.1"
	ideology = libertarian
	
	traits = {
	
	}
}

}