﻿capital = 780

oob = "BUR_2000"

set_convoys = 20
set_stability = 0.5

set_country_flag = country_language_english
set_country_flag = country_language_french

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1

	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_explosion
	hutu_tutsi_tensions
	african_union_member
	extensive_conscription
}

set_politics = {

	parties = {
		nationalist = {
			popularity = 50
		}
		social_democrat = {
			popularity = 25
		}
		communist = {
			popularity = 20
		}
	}
	
	ruling_party = nationalist
	last_election = "1997.1.1"
	election_frequency = 48
	elections_allowed = yes
}

create_country_leader = {
	name = "Pierre Buyoya"
	picture = "Pierre_Buyoya.dds"
	ideology = autocrat
}
create_country_leader = {
	name = "Pierre Nkurunziza"
	picture = "Pierre_Nkurunziza.dds"
	ideology = social_democrat_ideology
}

2005.7.26 = {
	set_politics = {
		parties = {
			islamist = {
				popularity = 1
			}
			nationalist = {
				popularity = 30
			}
			reactionary = {
				popularity = 2
			}
			conservative = {
				popularity = 1
			}
			market_liberal = {
				popularity = 6
			}
			social_liberal = {
				popularity = 5
			}
			social_democrat = {
				popularity = 40
			}
			progressive = {
				popularity = 5
			}
			democratic_socialist = {
				popularity = 5
			}
			communist = {
				popularity = 5
			}
		}
		ruling_party = social_democrat
		last_election = "2008.6.23"
		election_frequency = 48
		elections_allowed = yes
	}
}

2017.1.1 = {

oob = "BUR_2017"

# Starting tech
set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1
	#For templates
	infantry_weapons = 1
	
	combat_eng_equipment = 1
	command_control_equipment = 1
	land_Drone_equipment = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	gw_artillery = 1
	Anti_Air_0 = 1
	Early_APC = 1
	APC_1 = 1
	util_vehicle_equipment_0 = 1
	Rec_tank_0 = 1
}

add_ideas = {
	pop_050
	unrestrained_corruption
	christian
	gdp_1
		stable_growth
		defence_02
	edu_03
	health_02
	social_01
	bureau_04
	police_05
	volunteer_army
	volunteer_women
	farmers
	international_bankers
	small_medium_business_owners
	hybrid
}
set_country_flag = gdp_1
set_country_flag = negative_farmers
set_country_flag = negative_small_medium_business_owners


#Nat focus
	complete_national_focus = bonus_tech_slots

	set_politics = {

	parties = {
		democratic = { 
			popularity = 10
		}

		fascism = {
			popularity = 0
		}
		
		communism = {
			popularity = 0
			#banned = no #default is no
		}
		
        neutrality = {
			popularity = 75
			#banned = no #default is no
		} 
		
		nationalist = {
			popularity = 15
			#banned = no #default is no
		} 
	}
	
	ruling_party = neutrality
	last_election = "2015.7.21"
	election_frequency = 60
	elections_allowed = yes
}

create_country_leader = {
	name = "Pierre Nkurunziza"
	desc = ""
	picture = "BUR_Pierre_Nkurunziza.dds"
	expire = "2065.1.1"
	ideology = neutral_autocracy
	traits = {
		#
	}
}
create_field_marshal = {
	name = "Prime Niyongabo"
	picture = "generals/Portrait_Prime_Niyongabo.dds"
	traits = {  }
	skill = 1
}

create_field_marshal = {
	name = "Athanase Kararuza"
	picture = "generals/Portrait_Athanase_Kararuza.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Adolphe Nshimirimana"
	picture = "generals/Portrait_Adolphe_Nshimirimana.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Gaspard Baratuza"
	picture = "generals/Portrait_Gaspard_Baratuza.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Godefroid Bizimana"
	picture = "generals/Portrait_Godefroid_Bizimana.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Adolphe Manirakiza"
	picture = "generals/Portrait_Adolphe_Manirakiza.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Pierre Nkurikiye"
	picture = "generals/Portrait_Pierre_Nkurikiye.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Venuste Nduwayo"
	picture = "generals/Portrait_Venuste_Nduwayo.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Domitien Kabisa"
	picture = "generals/Portrait_Domitien_Kabisa.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Godefroid Niyombare"
	picture = "generals/Portrait_Godefroid_Niyombare.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Silas Ntigurirwa"
	picture = "generals/Portrait_Silas_Ntigurirwa.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Prime Niyongabo"
	picture = "admirals/Portrait_Prime_Niyongabo.dds"
	traits = {  }
	skill = 1
}

}
