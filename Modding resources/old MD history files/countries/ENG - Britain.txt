﻿capital = 126

oob = "ENG_2000"

set_convoys = 1000
set_stability = 0.5

set_country_flag = country_language_english

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1 ##Chieftan
	ENG_MBT_2 = 1
	MBT_3 = 1 #Challanger
	ENG_MBT_3 = 1
	MBT_4 = 1 ##Challanger 2
	ENG_MBT_4 = 1
	
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	sp_artillery1 = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	AT_upgrade_1 = 1
	Heavy_Anti_tank_1 = 1
	Anti_tank_1 = 1 
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	corvette1 = 1
	corvette2 = 1
	frigate_1 = 1
	frigate_2 = 1
	missile_frigate_1 = 1
	missile_frigate_2 = 1
	missile_frigate_2 = 1
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	missile_destroyer_2 = 1
	missile_destroyer_3 = 1

	modern_carrier_0 = 1
	modern_carrier_1 = 1
	modern_carrier_2 = 1
	carrier3 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	submarine3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
	
}

add_ideas = {
	population_growth_stagnation
	ENG_queen_elizabeth_2
	idea_eu_member
	idea_united_nations_security_council_member
	commonwealth_of_nations_member
}

give_guarantee = BRU
give_guarantee = MLT
give_guarantee = CYP

give_military_access = CYP
give_military_access = BRU

set_politics = {

	parties = {
		social_democrat = {			#Labour + SNP + SDLP + Natural Law
			popularity = 45.7
		}
		conservative = {			#Tories + Liberal Party + ProLife + UK Unionists
			popularity = 31.0
		}
		social_liberal = {			#LibDems
			popularity = 16.2
		}
		reactionary = {				#Referendum Party + DUP + UKIP
			popularity = 3.2
		}
		democratic_socialist = {	#Sinn Fein + SLP + PUP + SSP
			popularity = 0.7
		}
		progressive = {				#Plaid Cymru + Green
			popularity = 0.8
		}
		market_liberal = {			#APNI
			popularity = 0.2
		}
		fascist = {					#BNP
			popularity = 0.1
		}
		nationalist = {				#National Democrats
			popularity = 0.1
		}
		communist = {				#Socialist Alternative + CPGB
			popularity = 0.1
		}
	}
	
	ruling_party = social_democrat
	last_election = "1997.5.1"
	election_frequency = 60
	elections_allowed = yes
}

add_opinion_modifier = {
	target = AST
	modifier = five_eyes_agreement
}

add_opinion_modifier = {
	target = CAN
	modifier = five_eyes_agreement
}

add_opinion_modifier = {
	target = FRA
	modifier = entente_cordiale
}

add_opinion_modifier = {
	target = NZL
	modifier = five_eyes_agreement
}

add_opinion_modifier = {
	target = USA
	modifier = five_eyes_agreement
}

create_country_leader = {
	name = "Nick Griffin"
	picture = "Nick_Griffin.dds"
	ideology = fascist_ideology
}

create_country_leader = {
	name = "Tom Holmes"
	picture = "Tom_Holmes.dds"
	ideology = autocrat
}

create_country_leader = {
	name = "Elizabeth II"
	picture = "Elizabeth.dds"
	ideology = absolute_monarchist
}

create_country_leader = {
	name = "Michael Holmes"
	picture = "Michael_Holmes.dds"
	ideology = counter_progressive_democrat
}

create_country_leader = {
	name = "William Hague"
	picture = "William_Hague.dds"
	ideology = fiscal_conservative
}

create_country_leader = {
	name = "Tony Blair"
	picture = "Tony_Blair.dds"
	ideology = social_democrat_ideology
}

create_country_leader = {
	name = "Anjem Choudary"
	picture = "Anjem_Choudary.dds"
	ideology = islamic_authoritarian
}
	
create_country_leader = {
	name = "Charles Kennedy"
	picture = "Charles_Kennedy.dds"
	ideology = centrist
}
	
create_country_leader = {
	name = "Natalie Bennett"
	picture = "Natalie_Bennett.dds"
	ideology = green
}
	
create_country_leader = {
	name = "Peter Taaffe"
	picture = "Peter_Taaffey.dds"
	ideology = democratic_socialist_ideology
}
	
create_country_leader = {
	name = "Robert Griffiths"
	picture = "Robert_Griffiths.dds"
	ideology = leninist
}

add_namespace = {
	name = "eng_unit_leader"
	type = unit_leader
}

create_field_marshal = {
	name = "Sir Stuart Peach"
	picture = "generals/Stuart_Peach.dds"
	traits = { old_guard defensive_doctrine }
	skill = 2
}

create_field_marshal = {
	name = "Patrick Sanders"
	picture = "generals/Patrick_Sanders.dds"
	traits = { offensive_doctrine }
	skill = 1
}

create_field_marshal = {
	name = "Sir Richard Shirreff"
	picture = "generals/Richard_Shirreff.dds"
	traits = { inspirational_leader }
	skill = 2
}

create_field_marshal = {
	name = "Sir Christopher Deverell"
	picture = "generals/Chris_Deverell.dds"
	traits = { logistics_wizard }
	skill = 2
}
	
create_corps_commander = {
	name = "Glenn Haughton"
	picture = "generals/Glenn_Haughton.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Paul Anthony Edward Nanson"
	picture = "generals/Edward_Nanson.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Andrew Hughes"
	picture = "generals/Andrew_Hughes.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Philip Napier"
	picture = "generals/Philip_Napier.dds"
	traits = { hill_fighter }
	skill = 1
}

create_corps_commander = {
	name = "Mike Elviss"
	picture = "generals/Mike_Elviss.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Tom Beckett"
	picture = "generals/Tom_Beckett.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Richard William Dennis"
	picture = "generals/Richard_Dennis.dds"
	traits = { trickster }
	skill = 1
}

create_corps_commander = {
	name = "Bob Bruce"
	picture = "generals/Bob_Bruce.dds"
	traits = { trickster }
	skill = 1
}

create_corps_commander = {
	name = "Colin Weir"
	picture = "generals/Colin_Weir.dds"
	traits = { commando ranger }
	skill = 1
}

create_corps_commander = {
	name = "Ralph Wooddisse"
	picture = "generals/Ralph_Wooddisse.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Charlie Herbert"
	picture = "generals/Charlie_Herbert.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Richard Felton"
	picture = "generals/Richard_Felton.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Tyrone Urch"
	picture = "generals/Tyrone_Urch.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Alastair Aitken"
	picture = "generals/Alastair_Aitken.dds"
	traits = { urban_assault_specialist }
	skill = 1
}

create_corps_commander = {
	name = "David Neal"
	picture = "generals/David_Neal.dds"
	traits = { urban_assault_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Benjamin Bathurst"
	picture = "generals/Ben_Bathurst.dds"
	traits = { urban_assault_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Mike Walliker"
	picture = "generals/Mike_Walliker.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "James Illingworth"
	picture = "generals/James_Illingworth.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Russell La Forte"
	picture = "generals/Russ_La_Forte.dds"
	traits = { urban_assault_specialist }
	skill = 1
}

create_corps_commander = {
	name = "James Robert Chiswell"
	picture = "generals/James_Chiswell.dds"
	traits = { commando }
	skill = 1
}

create_corps_commander = {
	name = "Oliver Stokes"
	picture = "generals/Oliver_Stokes.dds"
	traits = { fortress_buster }
	skill = 1
}

create_corps_commander = {
	name = "Andy Hadfield"
	picture = "generals/Andy_Hadfield.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "David Middleton"
	picture = "generals/David_Middleton.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Sir Stephen Hillier"
	picture = "generals/Stephen_Hillier.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Robert Magowan"
	picture = "generals/Rob_Magowan.dds"
	traits = { naval_invader }
	skill = 1
}

create_corps_commander = {
	name = "Matt Pierson"
	picture = "generals/Matt_Pierson.dds"
	traits = { commando naval_invader }
	skill = 1
}

create_corps_commander = {
	name = "Richard Cantrill"
	picture = "generals/Richard_Cantrill.dds"
	traits = { commando naval_invader }
	skill = 1
}

create_corps_commander = {
	name = "Graeme Fraser"
	picture = "generals/Graeme_Fraser.dds"
	traits = { commando naval_invader }
	skill = 1
}

create_corps_commander = {
	name = "Tony Turner"
	picture = "generals/Tony_Turner.dds"
	traits = { commando naval_invader }
	skill = 1
}

create_corps_commander = {
	name = "Paul Lynch"
	picture = "generals/Paul_Lynch.dds"
	traits = { commando trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Graeme Armour"
	picture = "generals/Graeme_Armour.dds"
	traits = { commando naval_invader }
	skill = 1
}

create_corps_commander = {
	name = "Ed Davis"
	picture = "generals/Ed_Davis.dds"
	traits = { naval_invader }
	skill = 1
}
	
create_corps_commander = {
	name = "Patrick Cordingley"
	picture = "generals/Patrick_Cordingley.dds"
	skill = 1
}
	
create_corps_commander = {
	name = "Michael Rose"
	picture = "generals/Michael_Rose.dds"
	skill = 1
}
	
create_corps_commander = {
	name = "Peter Wall"
	picture = "generals/Peter_Wall.dds"
	skill = 1
}

create_corps_commander = {
	name = "Richard Dannatt"
	picture = "generals/Richard_Dannatt.dds"
	skill = 1
}

create_corps_commander = {
	name = "Rupert Smith"
	picture = "generals/Rupert_Smith.dds"
	skill = 1
}

create_navy_leader = {
	name = "Jock Slater"
	picture = "admirals/Jock_Slater.dds"
	skill = 1
}

create_navy_leader = {
	name = "Mark Stanhope"
	picture = "admirals/Mark_Stanhope.dds"
	skill = 1
}

create_navy_leader = {
	name = "Michael Boyce"
	picture = "admirals/Michael_Boyce.dds"
	skill = 1
}

create_navy_leader = {
	name = "Philip Jones"
	picture = "admirals/Philip_Jones.dds"
	skill = 1
}

create_navy_leader = {
	name = "Duncan Potts"
	picture = "admirals/Duncan_Potts.dds"
	traits = { superior_tactician }
	skill = 1
}

create_navy_leader = {
	name = "Alasdair Walker"
	picture = "generals/Alasdair_Walker.dds"
	traits = { blockade_runner }
	skill = 1
}

create_navy_leader = {
	name = "Ben Key"
	picture = "generals/Ben_Key.dds"
	traits = { ironside }
	skill = 1
}

create_navy_leader = {
	name = "Jonathan Woodcock"
	picture = "generals/Jonathan_Woodcock.dds"
	traits = { seawolf }
	skill = 1
}

create_navy_leader = {
	name = "Tom Karsten"
	picture = "generals/Tom_Karsten.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Paul Bennett"
	picture = "generals/Paul_Bennett.dds"
	traits = { fly_swatter }
	skill = 1
}

create_navy_leader = {
	name = "Robert Tarrant"
	picture = "generals/Robert_Tarrant.dds"
	traits = { spotter }
	skill = 1
}

create_navy_leader = {
	name = "Alex Burton"
	picture = "generals/Alex_Burton.dds"
	traits = { seawolf }
	skill = 1
}

create_navy_leader = {
	name = "John Weale"
	picture = "generals/John_Weale.dds"
	traits = { air_controller }
	skill = 1
}

create_navy_leader = {
	name = "Simon Williams"
	picture = "generals/Simon_Williams.dds"
	traits = { ironside }
	skill = 1
}

create_navy_leader = {
	name = "John Clink"
	picture = "generals/John_Clink.dds"
	traits = { spotter }
	skill = 1
}

2001.1.1 = {
	complete_national_focus = ENG_vitalize_westminster
	complete_national_focus = ENG_the_future_of_britain
	complete_national_focus = ENG_british_armed_forces
	complete_national_focus = ENG_economic_stimulus
	complete_national_focus = ENG_political_reform
	complete_national_focus = ENG_democratic_tradition
	complete_national_focus = ENG_security_first
	complete_national_focus = ENG_public_CCTV_systems
	complete_national_focus = ENG_internet_surveillance
	complete_national_focus = ENG_english_industry
	complete_national_focus = ENG_scottish_industry
	complete_national_focus = ENG_expand_the_home_ministry
	complete_national_focus = ENG_expand_the_foreign_ministry
	complete_national_focus = ENG_go_with_europe
	complete_national_focus = ENG_careful_cooperation
	complete_national_focus = ENG_special_treatment
	complete_national_focus = ENG_investments_abroad_1
	complete_national_focus = ENG_investments_at_home
	complete_national_focus = ENG_investments_abroad_2
	complete_national_focus = ENG_develop_infrastructure
}
	
2016.6.1 = {
	set_politics = {
		ruling_party = conservative
		elections_allowed = yes
		last_election = "2015.5.7"
		
		parties = {
			conservative = {			#Tories + UUP + TUV
				popularity = 37.3
			}
			social_democrat = {			#Labour + SNP + SDLP + NHA
				popularity = 35.7
			}
			reactionary = {				#UKIP + DUP; buffed by like 2% so that Brexit is possible
				popularity = 15.1
			}
			social_liberal = {			#LibDems
				popularity = 7.9
			}
			progressive = {				#Green Party + Plaid Cymru
				popularity = 4.4
			}
			democratic_socialist = {	#Sinn Fein + TUSC + Respect + S(GB) + SSP
				popularity = 0.9
			}
			market_liberal = {			#APNI
				popularity = 0.2
			}
			nationalist = {				#English Democrat + Christian + NLP
				popularity = 0.2
			}
			communist = {				#Workers Party
				popularity = 0.1
			}
			fascist = {					#BNP
				popularity = 0.1 
			}
		}
	}
	
	create_country_leader = {
		name = "David MacDonald"
		picture = "david_macdonald.dds"
		ideology = national_socialist
	}
	
	create_country_leader = {
		name = "Adam Walker"
		picture = "adam_walker.dds"
		ideology = proto_fascist
	}
	
	create_country_leader = {
		name = "Nigel Farage"
		picture = "Nigel_Farage.dds"
		ideology = counter_progressive_democrat
	}
	
	create_country_leader = {
		name = "David Cameron"
		picture = "David_Cameron.dds"
		ideology = fiscal_conservative
	}
	
	create_country_leader = {
		name = "Adam Brown"
		picture = "Adam_Brown.dds"
		ideology = libertarian
	}
	
	create_country_leader = {
		name = "Tim Farron"
		picture = "Tim_Farron.dds"
		ideology = centrist
	}
	
	create_country_leader = {
		name = "Jeremy Corbyn"
		picture = "Jeremy_Corbyn.dds"
		ideology = social_democrat_ideology
	}
}
	
	

