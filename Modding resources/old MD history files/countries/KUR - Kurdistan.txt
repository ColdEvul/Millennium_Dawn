﻿capital = 815

oob = "KUR_2016"

set_convoys = 25
set_stability = 0.5

set_country_flag = country_language_kurdish

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_steady
	service_by_requirement
	partially_recognized_state
	war_economy
}
	
set_politics = {

	parties = {
		nationalist = {
		    popularity = 20
		}
		reactionary = {
			popularity = 3
		}
		conservative = {
			popularity = 40
		}
		social_liberal = {
			popularity = 4
		}
		communist = {
			popularity = 13
		}
		islamist = {
			popularity = 21
		}
	}
	
	ruling_party = conservative
	last_election = "2014.3.1"
	election_frequency = 48
	elections_allowed = yes
}

2015.6.2 = {
	add_to_war = {
		targeted_alliance = IRQ
		enemy = ISS
	}
}

create_country_leader = {
	name = "Masoud Barzani" 
	picture = "Masoud_Barzani.dds"
	ideology = fiscal_conservative
}

create_country_leader = {
    name = "Salaheddine Bahaaeddin"
	picture = "Salaheddine_Bahaaeddin.dds"
	ideology = islamic_republican
}

create_country_leader = { 
    name = "Nusherwan Mustafa"
	picture = "Nusherwan_Mustafa.dds"
	ideology = oligarchist 
}

create_country_leader = { 
    name = "Arif Bawecani"
	picture = "Arif_Bawecani.dds"
	ideology = libertarian
}

create_country_leader = { 
	name = "Hussein Mansor"     
	picture = "Hussein_Mansor.dds"
	ideology = national_democrat 
}

create_country_leader = { 
    name = "Jalal Talabani"
	picture = "Jalal_Talabani.dds"
 	ideology = 	democratic_socialist_ideology
}

create_country_leader = { 
	name = "Raouf Karimi"       
	picture = "Raouf_Karimi.dds"
	ideology = social_democrat_ideology 
}

create_country_leader = { 
    name = "Abdullah Ocalan"
	picture = "Abdullah_Ocalan.dds" 
	ideology = marxist 
}

create_corps_commander = {
	name = "Hussein Mansor"
	picture = "generals/Hussein_Mansor.dds"
	traits = { panzer_leader }
	skill = 2
}

create_corps_commander = {
	name = "Abdula Musla"
	picture = "generals/Abdula_Musla.dds"
	traits = { commando }
	skill = 1
}

create_corps_commander = {
	name = "Hossein Yazdan Bna"
	picture = "generals/Hossein_Yazdan_Bna.dds"
	traits = { trickster }
	skill = 1
}

create_corps_commander = {
	name = "Sirwan Barzani"
	picture = "generals/Sirwan_Barzani.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Karzan Mahmoud Ahmed"
	picture = "generals/Karzan.dds"
	traits = { desert_fox }
	skill = 2
}

create_corps_commander = {
	name = "Sherko Shwani"
	picture = "generals/Sherko.dds"
	traits = { trait_engineer }
	skill = 1
}
