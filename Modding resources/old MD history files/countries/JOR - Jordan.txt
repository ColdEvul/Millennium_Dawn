﻿capital = 455

oob = "JOR_2000"

set_convoys = 10
set_stability = 0.5	

set_country_flag = country_language_arabic

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

set_country_flag = dominant_religion_islam
set_country_flag = sunni_islam

add_ideas = {
	population_growth_rapid
	arab_league_member
	extensive_conscription
}

set_politics = {
	parties = {
		islamist = { popularity = 19 }
		nationalist = { popularity = 1 }
		monarchist = { popularity = 40 }
		reactionary = { popularity = 2 }
		conservative = { popularity = 18 }
		market_liberal = { popularity = 2 }
		social_liberal = { popularity = 2 }
		social_democrat = { popularity = 2 }
		progressive = { popularity = 2 }
		democratic_socialist = { popularity = 2 }
		communist = { popularity = 10 }
	}
	ruling_party = monarchist
	last_election = "1997.1.1"
	election_frequency = 48
	elections_allowed = no
}

create_country_leader = {
	name = "Abdullah II"
	picture = "Abdullah_II.dds"
	ideology = absolute_monarchist
}

create_country_leader = { 
    name = "Saeed Thyab"
	picture = "Saeed_Thyab.dds"
 	ideology = democratic_socialist_ideology
}

create_country_leader = { 
    name = "Mazin Riyal"
	picture = "Mazin_Riyal.dds"
 	ideology = libertarian
}

create_country_leader = {
	name = "Hamza Mansour"
	picture = "Hamza_Mansour.dds"
	ideology = islamic_republican
}

create_country_leader = { 
    name = "Akram al-Homsi"
	picture = "Akram_al-Homsi.dds"
 	ideology = national_democrat
}

create_country_leader = { 
    name = "Faraj Al-Tameezi"
	picture = "Faraj_Al-Tameezi.dds"
 	ideology = marxist
}

create_country_leader = { 
    name = "Ahmed al-Shunaq"
	picture = "Ahmed_Al-Shannaq.dds"
 	ideology = centrist
}

create_country_leader = { 
    name = "Fawaz Zou'bi"
	picture = "Fawaz_Zou'bi.dds"
 	ideology = progressive_ideology
}

create_country_leader = { 
    name = "Hazem Qashou"
	picture = "Hazem_Qashou.dds"
 	ideology = constitutionalist
}

create_country_leader = { 
    name = "Saleh Ersheidat"
	picture = "Saleh_Rsheedat.dds"
 	ideology = social_democrat_ideology
}

create_country_leader = { 
    name = "Rula Alhroob"
	picture = "Rula_Alhroob.dds"
 	ideology = oligarchist
}

create_country_leader = { 
    name = "Emhamed Abu Khalil"
	picture = "Emhamed_Abu_Khalil.dds"
 	ideology = falangist
}

create_field_marshal = {
	name = "Abdul Hafiz Al-Kaabnehn"
	picture = "Abdul_Hafiz_Al-Kaabnehn.dds"
	traits = { old_guard thorough_planner }
	skill = 2
}

create_corps_commander = {
	name = "Mohammed Yousef Al Malkawi"
	picture = "Mohammed_Yousef_Malkawi.dds"
	traits = { panzer_leader }
	skill = 1
}
create_corps_commander = {
	name = "Ma'ashal Al-Zabin"
	picture = "Ma'ashal_el-Zabin.dds"
	traits = { desert_fox }
	skill = 2
}
create_corps_commander = {
	name = "Mahmoud Abdel Halim Freihat"
	picture = "Mahmoud_Abdel_Halim_Freihat.dds"
	traits = { commando }
	skill = 1
}
create_corps_commander = {
	name = "Hussein al-Majali"
	picture = "Hussein_A_Majali.dds"
	traits = { trickster }
	skill = 1
}
create_corps_commander = {
	name = "Khaled Jamil el-Sarayreh"
	picture = "Khaled_Jamil_el-Sarayreh.dds"
	traits = { ranger }
	skill = 2
}
create_corps_commander = {
	name = "Khaled Al-Sharah"
	picture = "Khaled_Sarah.dds"
	traits = { urban_assault_specialist }
	skill = 1
}
create_navy_leader = {
	name = "Qasim Fadil Nahar"
	picture = "Qasim_Fadil_Nahar.dds"
	traits = { old_guard_navy superior_tactician }
	skill = 1
}
