﻿capital = 559

oob = "SOM_2000"

set_convoys = 30
set_stability = 0.5

set_country_flag = country_language_arabic
set_country_flag = country_language_somali

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

set_country_flag = dominant_religion_islam
set_country_flag = sunni_islam

add_ideas = {
	population_growth_rapid
	african_union_member
	arab_league_member
}

set_politics = {
	parties = {
		islamist = { popularity = 28 }
		nationalist = { popularity = 18 }
		reactionary = { popularity = 36 }        
		conservative = { popularity = 8 }
		social_liberal = { popularity = 10 }
	}
	
	ruling_party = reactionary
	last_election = "2000.8.27"
	election_frequency = 48
	elections_allowed = yes
}

create_country_leader = {
	name = "Abdiqasim Salad Hassan"
	picture = "Abdiqasim_S_Hassan.dds"
	ideology = counter_progressive_democrat
}

create_country_leader = {
	name = "Ahmed Omar"
	picture = "Ahmed_Omar.dds"
	ideology = islamic_authoritarian
}

create_country_leader = {
	name = "Mohamed Abdullahi Mohamed" 
	picture = "Mohamed_Abdullahi_Mohamed.dds"
	ideology = centrist
}

create_country_leader = {   
	name = "Hassan Moalim"
	picture = "Hassan_Moalim.dds"
	ideology = right_wing_conservative
}

create_country_leader = {   
	name = "Akhalfo Muhammad Bebeoukmole"
	picture = "Prince_Bebeoukmole.dds"
	ideology = absolute_monarchist
}

create_country_leader = {
	name = "Maslah Mohamed Siad"    
	picture = "Maslah_Mohamed_Siad.dds"
	ideology = national_democrat
}

create_country_leader = {
	name = "Abdirahman Ibrahim Bile"
	picture = "Abdirahman_Ibrahim_Bile.dds"
	ideology = libertarian
}

create_country_leader = {
	name = "Ali Mohammed Ghedi"
	picture = "Ali_Ghedi.dds"
	ideology = social_democrat_ideology
}

create_country_leader = {
	name = "Mohamed Abdi Hassan"
	picture = "Abdi_Hassan.dds"
	ideology = national_socialist
}

create_country_leader = {
	name = "Sa'iid Isse Mohamud"    
	picture = "Sa'iid_Isee_Mohamud.dds"
	ideology = marxist
}

create_corps_commander = {    
	name = "Mohamed Adam Ahmed"
	picture = "Mohamed_Adam_Ahmed.dds"
	traits = { panzer_leader ranger }
	skill = 2
}

create_corps_commander = {    
	name = "Ahmed Hashi Tajir" 
	picture = "Ahmed_Hashi_Tajir.dds"
	traits = { trickster }
	skill = 2
}

create_corps_commander = { 
	name = "Mahamed Nur Galaal" 
	picture = "Mahamed_Nur_Galaal.dds"
	traits = { panzer_leader commando }
	skill = 1
}

create_corps_commander = { 
	name = "Abdulkadir Sheikh Dini" 
	picture = "Abdulkadir_Sheikh_Dini.dds"
	traits = { desert_fox }
	skill = 1
}

create_navy_leader = { 
	name = "Mohamed Abukar Hassan" 
	picture = "Mohamed_Abukar_Hassan.dds"
	traits = { old_guard_navy superior_tactician }
	skill = 2
}

create_navy_leader = { 
	name = "Farah Ahmed Omar"  
	picture = "Farah_Ahmed_Omar.dds" 
	traits = { blockade_runner }
	skill = 1
}

create_navy_leader = { 
	name = "Mohamed Isse Ali"  
	picture = "Mohamed_Isse_Ali.dds" 
	traits = { spotter }
	skill = 2
}


2016.1.1 = {
	create_country_leader = {
		name = "Hassan Sheikh Mohamud"
		picture = "Hassan_S_Mohamud.dds"
		ideology = counter_progressive_democrat
	}
	
	create_country_leader = {   
		name = "Maryam Qaasim"
		picture = "Portrait_SOM_Maryam_Qaasim.dds"
		ideology = liberalist
	}
}