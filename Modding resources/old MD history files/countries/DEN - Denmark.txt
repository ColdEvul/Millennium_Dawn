﻿capital = 37

oob = "DEN_2000"

set_convoys = 80
set_stability = 0.5

set_country_flag = country_language_danish

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	cruiser_1 = 1
	cruiser_2 = 1
	missile_cruiser_1 = 1
	modern_carrier_0 = 1
	modern_carrier_1 = 1
	modern_carrier_2 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	frigate_1 = 1
	frigate_2 = 1
	missile_frigate_1 = 1
	missile_frigate_2 = 1
	missile_frigate_2 = 1
	frigate3 = 1
	corvette1 = 1
	corvette2 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	
	
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	gen5_mpa = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_stagnation
	idea_eu_member
}

set_politics = { ruling_party = social_democrat }
if = {
	limit = {
		has_dlc = "Together for Victory"
	}
	set_autonomy = {
		target = FAI
		autonomous_state = autonomy_dominion
	}
	set_autonomy = {
		target = GRN
		autonomous_state = autonomy_dominion
	}
	else = {
		puppet = FAI
		puppet = GRN
	}
}

set_politics = {

	parties = {
		social_democrat = { popularity = 35.9 }			#Social Democrats
		market_liberal = { popularity = 27.9 }			#Venstre
		conservative = { popularity = 8.9 }				#Conservative Peoples Party
		progressive = { popularity = 7.6 }				#Socialist Peoples Party
		reactionary = { popularity = 7.4 }				#Danish Peoples Party
		social_liberal = { popularity = 4.3 }			#Danish SocLib Party
		democratic_socialist = { popularity = 2.7 }		#Red-Greens
		nationalist = { popularity = 2.4 }          	#Danish Unity
	}
	
	ruling_party = social_democrat
	last_election = "1998.3.11"
	election_frequency = 48
	elections_allowed = yes
}

create_country_leader = {
	name = "Poul Nyrup Rasmussen"
	picture = "Poul_Nyrup_Rasmussen.dds"
	ideology = social_democrat_ideology
}

create_country_leader = {
	name = "Holger K. Nielsen"
	picture = "Holger_K_Nielsen.dds"
	ideology = progressive_ideology  
}

create_country_leader = {
	name = "Henrik Stamer Hedin"
	picture = "Henrik_Stamer_Hedin.dds"
	ideology = leninist  
}

create_country_leader = {
	name = "Collective Leadership"
	picture = "enhedslisten.dds"
	ideology = democratic_socialist_ideology  
}

create_country_leader = {
	name = "Anders Fogh Rasmussen"
	picture = "Anders_Fogh_Rasmussen.dds"
	ideology = libertarian
}

create_country_leader = {
	name = "Bendt Bendtsen"
	picture = "Bendt_Bendtsen.dds"
	ideology = right_wing_conservative 
}

create_country_leader = {
	name = "Pia Kjaersgaard"
	picture = "Pia_Kjaersgaard.dds"
	ideology = counter_progressive_democrat 
}

create_country_leader = {
	name = "Marianne Jelved"
	picture = "Marianne_Jelved.dds"
	ideology = centrist
}

create_country_leader = {
	name = "Jonni Hansen"
	picture = "Jonni_Hansen.dds"
	ideology = national_socialist
}

create_country_leader = {
	name = "Fadi Abdullatif"
	picture = "Fadi_Abdullatif.dds"
	ideology = islamic_authoritarian
}

create_country_leader = {
	name = "Adam Wagner"
	picture = "Adam_Wagner.dds"
	ideology = national_democrat  
}

create_country_leader = {
	name = "Margrethe II"
	picture = "Margrethe.dds"
	ideology = absolute_monarchist
}

2015.7.29 = {
	set_politics = {
		parties = {
			social_democrat = { popularity = 24.9 }		#Social Democrats
			market_liberal = { popularity = 26.7 }		#Venstre + DSLP
			conservative = { popularity = 4.9 }		#Conservative Peoples Party
			progressive = { popularity = 4.8 }			#The Alternative
			nationalist = { popularity = 2.3 }			# Danish Unity
			social_liberal = { popularity = 5 }			#Liberal Alliance
			democratic_socialist = { popularity = 15.9 }	#Red-Greens + Socialist Peoples Party
			reactionary = { popularity = 10.0 }            #Danish Peoples Party
		}
		ruling_party = market_liberal
		last_election = "2015.7.28"
		election_frequency = 48
		elections_allowed = yes
	}	
	
	create_country_leader = {
		name = "Lars Lokke Rasmussen"
		picture = "Lars_Lokke_Rasmussen.dds"
		ideology = libertarian
	}
	
	create_country_leader = {
		name = "Morten Ostergaard"
		picture = "Morten_Ostergaard.dds"
		ideology = centrist
	}

	create_country_leader = {
		name = "Soren Pape Poulsen"
		picture = "Soren_Pape_Poulsen.dds"
		ideology = right_wing_conservative
	}
	
	create_country_leader = {
		name = "Mette Frederiksen"
		picture = "Mette_Frederiksen.dds"
		ideology = social_democrat_ideology
	}
	
	create_country_leader = {
		name = "Pia Olsen Dyhr"
		picture = "Pia_Olsen_Dyhr.dds"
		ideology = progressive_ideology  
	}
	
	create_country_leader = {
		name = "Kristian Thulesen Dahl"
		picture = "Kristian_Thulesen_Dahl.dds"
		ideology = counter_progressive_democrat 
	}	

	create_country_leader = {
		name = "Esben Rohde Kristensen"
		picture = "Esben_Rohde_Kristensen.dds"
		ideology = national_socialist 
	}	

	create_country_leader = {
		name = "Junes Kock"
		picture = "Junes_Kock.dds"
		ideology = islamic_authoritarian 
	}	

	create_country_leader = {
		name = "Morten Uhrskov Jensen"
		picture = "Morten_Uhrskov.dds"
		ideology = national_democrat  
	}

	create_country_leader = {
		name = "Pernille Skipper"
		picture = "Pernille_Skipper.dds"
		ideology = democratic_socialist_ideology
	}

	create_country_leader = {
		name = "Margrethe II"
		picture = "Margrethe.dds"
		ideology = absolute_monarchist
	}
	
}