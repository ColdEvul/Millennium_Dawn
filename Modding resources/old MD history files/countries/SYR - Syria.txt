﻿capital = 554


oob = "SYR_2000"

set_convoys = 150
set_stability = 0.5

set_country_flag = country_language_arabic

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1

	frigate_1 = 1
	frigate_2 = 1
	missile_frigate_1 = 1
	missile_frigate_2 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_opinion_modifier = {
	target = ISR
	modifier = death_to_israel
}

set_country_flag = dominant_religion_islam
set_country_flag = shia_islam

add_ideas = {
	population_growth_rapid
	arab_league_member
	extensive_conscription
}

set_politics = {

	parties = {
		islamist = {
			popularity = 15
		}
		nationalist = {
			popularity = 40
		}
		reactionary = {
			popularity = 10
		}
		conservative = {
			popularity = 15
		}
		market_liberal = {
			popularity = 0
		}
		social_liberal = {
			popularity = 15
		}
		social_democrat = {
			popularity = 0
		}
		progressive = {
			popularity = 0
		}
		democratic_socialist = {
			popularity = 0
		}
		communist = {
			popularity = 5
		}
		monarchist = {
			popularity = 0
		}
		fascist = {
			popularity = 0
		}
	}
	
	ruling_party = nationalist
	last_election = "1997.1.1"
	election_frequency = 48
	elections_allowed = no
}

create_country_leader = {
	name = "Hafez al-Assad"
	ideology = autocrat
	picture = "Hafez_Al_Assad.dds"
}

create_country_leader = {
	name = "Mired I"
	ideology = absolute_monarchist
	picture = "Mired_The_First.dds"
}

create_country_leader = {
	name = "Ali Haidar"
	ideology = counter_progressive_democrat
	picture = "Ali_Haidar.dds"
}

create_country_leader = {
	name = "Anas al-Abdah"
	ideology = libertarian
	picture = "Anas_al_Abdah.dds"
}

create_country_leader = {
	name = "Hassan Abdul Azim"
	ideology = centrist
	picture = "Hassan_Abdul_Azim.dds"
}

create_country_leader = {
	name = "Riad al-Turk"
	ideology = social_democrat_ideology
	picture = "Riad_al_Turk.dds"
}

create_country_leader = {
	name = "Farid Ghadry"
	ideology = progressive_ideology
	picture = "Farid_Ghadry.dds"
}

create_country_leader = {
	name = "Ibrahim Makhous"
	ideology = democratic_socialist_ideology
	picture = "Ibrahim_Makhous.dds"
}

create_country_leader = {
	name = "Hanin Nimir"
	ideology = leninist
	picture = "Hanin_Nimir.dds"
}

create_country_leader = {
	name = "Ali Haidar"
	picture = "Ali_Haidar.dds"
	ideology = national_socialist
}

create_country_leader = {
	name = "Ali Sadreddine Al-Bayanouni"
	ideology = islamic_republican
	picture = "Ali_S_al_Bayanouni.dds"
}

create_country_leader = {
	name = "Khaled Khoja"
	picture = "Khaled_Khoja.dds"
	ideology = right_wing_conservative
}

create_field_marshal = {
	name = "Bashar Al Assad"
	picture = "Portrait_Bashar_Al_Assad.dds"
	traits = { thorough_planner }
	skill = 2
}

create_field_marshal = {
	name = "Fahd Jassem Al Frej"
	picture = "Portrait_Al_Frej.dds"
	traits = { old_guard organisational_leader }
	skill = 2
}

create_field_marshal = {
	name = "Dawoud Rajha"
	picture = "Portrait_Dawoud_Rajha.dds"
	traits = { old_guard defensive_doctrine }
	skill = 2
}

create_corps_commander = {
	name = "Ali Abdullah Ayyoub"
	picture = "Portrait_Ali_Ayyoub.dds"
	traits = { trait_engineer }
	skill = 2
}

create_corps_commander = {
	name = "Hawash Mohammed"
	picture = "Portrait_Hawash_Mohammed.dds"
	traits = { urban_assault_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Maher Al Assad"
	picture = "Portrait_Maher_Al_Assad.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Issam Zahreddine"
	picture = "Portrait_Issam_Zahreddine.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Suheil Salman Al Hassan"
	picture = "Portrait_Suheil_Salman_Al_Hassan.dds"
	traits = { commando desert_fox }
	skill = 1
}

create_corps_commander = {
	name = "Abdel Fatah Qudsiyeh"
	picture = "Portrait_Abdel_Fatah_Qudsiyeh.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Hassan Daaboul"
	picture = "Portrait_Hassan_Daaboul.dds"
	traits = { urban_assault_specialist }
	skill = 2
}

create_corps_commander = {
	name = "Hussein Isaac"
	picture = "Portrait_Hussein_Isaac.dds"
	traits = { trickster }
	skill = 1
}

create_corps_commander = {
	name = "Jameh Jameh"
	picture = "Portrait_Jameh_Jameh.dds"
	traits = { urban_assault_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Rustum Ghazaleh"
	picture = "Portrait_Rustum_Ghazaleh.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Hassan Turkmani"
	picture = "Portrait_Hassan_Turkmani.dds"
	traits = {  }
	skill = 1
}

2000.7.17 = {
    create_country_leader = {
	    name = "Bashar al-Assad"
	    ideology = autocrat
	    picture = "Bashar_Al_Assad.dds"
    }
}
	
2011.3.15 = {    #The Syrian Civil War Has Began
	add_ideas = {
		islamic_sectarian_conflicts
	}
}

2015.5.1 = {
	oob = "SYR_2016"
	add_ideas = {
		service_by_requirement
	}
	set_stability = 0.5
}

2017.3.6 = {
    create_country_leader = { #Riad Seif elected to leasd the Syrian National Coalition
	    name = "Riad Seif"
	    picture = "Riad_Seif.dds"
	    ideology = right_wing_conservative
    }
}