﻿capital = 7

oob = "HOL_2000"

set_convoys = 1000
set_stability = 0.5

set_country_flag = country_language_dutch

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	corvette1 = 1
	corvette2 = 1
	frigate_1 = 1
	frigate_2 = 1
	missile_frigate_1 = 1
	missile_frigate_2 = 1
	missile_frigate_2 = 1
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1


	modern_carrier_0 = 1
	modern_carrier_1 = 1
	modern_carrier_2 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_stagnation
	idea_eu_member
}

set_politics = {
	parties = {
		islamist = { popularity = 0.1 }
		nationalist = { popularity = 0.1 }
		reactionary = { popularity = 7.7 }
		conservative = { popularity = 16.4 }
		market_liberal = { popularity = 22.3 }
		social_liberal = { popularity = 11.3 }
		social_democrat = { popularity = 29.55 }
		progressive = { popularity = 7.5 }
		democratic_socialist = { popularity = 5.45 }
		communist = { popularity = 0.1 }
	}
	ruling_party = social_democrat
	last_election = "1998.5.8"
	election_frequency = 48
	elections_allowed = yes
}

add_opinion_modifier = {
	target = GER
	modifier = german_dutch_military_cooperation
}

create_country_leader = {
	name = "Wim Kok"
	picture = "Wim_Kok.dds"
	ideology = social_democrat_ideology
}

create_country_leader = {
	name = "Hans Dijkstal"
	picture = "Hans_Dijkstal.dds"
	ideology = libertarian
}

create_country_leader = {
	name = "Beatrix"
	picture = "Beatrix.dds"
	ideology = absolute_monarchist
}

create_country_leader = {
	name = "Jaap de Hoop Scheffer"
	picture = "Jaap_Scheffer.dds"
	ideology = christian_democrat
}

create_country_leader = {
	name = "Els Borst"
	picture = "Els_Borst.dds"
	ideology = liberalist
}

create_country_leader = {
	name = "Paul Rosenmöller"
	picture = "Paul_Rosenmoller.dds"
	ideology = green
}

create_country_leader = {
	name = "Jan Marijnissen"
	picture = "Jan_Marijnissen.dds"
	ideology = democratic_socialist_ideology
}

create_country_leader = {
	name = "Constant Kusters"
	picture = "Constant_Kusters.dds"
	ideology = national_socialist
}

#create_country_leader = {
#	name = "Bas van der Vlies"
#	picture = "Bas_Vlies.dds"
#	ideology = counter_progressive_democrat
#}

create_country_leader = {
	name = "Geert Wilders"
	picture = "Geert_Wilders.dds"
	ideology = counter_progressive_democrat
}

create_country_leader = {
	name = "Alejandro de Mello"
	picture = "Alejandro_Mello.dds"
	ideology = marxist
}

create_corps_commander = {
	name = "Tom Middendorp"
	picture = "generals/Tom_Middendorp.dds"
	skill = 2
}

create_corps_commander = {
	name = "Leo Beulen"
	picture = "generals/Leo_Beulen.dds"
	skill = 1
}

create_corps_commander = {
	name = "Marc van Uhm"
	picture = "generals/Marc_van_Uhm.dds"
	skill = 1
}

create_navy_leader = {
	name = "Rob Verkerk"
	picture = "admirals/Rob_Verkerk.dds"
	skill = 1
}

create_navy_leader = {
	name = "Matthieu Borsboom"
	picture = "admirals/Matthieu_Borsboom.dds"
	skill = 1
}

2002.1.1 = { add_ideas = the_euro }

2003.1.1 = {

	create_country_leader = {
		name = "Jan Paeter Balkenende"
		picture = "Jan_Paeter_Balkenende.dds"
		ideology = christian_democrat
	}

	create_country_leader = {
		name = "Wouter Bos"
		picture = "Wouter_Bos.dds"
		ideology = social_democrat_ideology
	}

	create_country_leader = {
		name = "Femke Halsema"
		picture = "Femke_Halsema.dds"
		ideology = green
	}

	create_country_leader = {
		name = "Thom de Graaf"
		picture = "Thom_de_Graaf.dds"
		ideology = liberalist
	}

	create_country_leader = {
		name = "Gerrit Zalm"
		picture = "Gerrit_Zalm.dds"
		ideology = libertarian
	}
	
}

2006.1.1 = {

	create_country_leader = {
		name = "Mark Rutte"
		picture = "Mark_Rutte.dds"
		ideology = libertarian
	}

	create_country_leader = {
		name = "Alexander Pechtold"
		picture = "Alexander_Pechtold.dds"
		ideology = liberalist
	}
	
}

2010.1.1 = {

	create_country_leader = {
		name = "Job Cohen"
		picture = "Job_Cohen.dds"
		ideology = social_democrat_ideology
	}

	create_country_leader = {
		name = "Emile Roemer"
		picture = "Emil_Roemer.dds"
		ideology = democratic_socialist_ideology
	}

	#create_country_leader = {
	#	name = "Kees van der Staaij"
	#	picture = "Kees_Staaij.dds"
	#	ideology = counter_progressive_democrat
	#}
}

2012.1.1 = {

	create_country_leader = {
		name = "Diederik Samsom"
		picture = "Diederik_Samsom.dds"
		ideology = social_democrat_ideology
	}

	create_country_leader = {
		name = "Sybrand van Haersma Buma"
		picture = "Sybrand_Buma.dds"
		ideology = christian_democrat
	}

	create_country_leader = {
		name = "Jolande Sap"
		picture = "Jolande_Sap.dds"
		ideology = green
	}
}

2013.4.30 = {
	create_country_leader = {
		name = "Willem-Alexander"
		picture = "Willem_Alexander.dds"
		ideology = absolute_monarchist
	}
}

2016.1.1 = {
	set_politics = {
		parties = {
			islamist = {
				popularity = 0
			}
			nationalist = {
				popularity = 10
			}
			reactionary = {
				popularity = 2
			}
			conservative = {
				popularity = 12
			}
			market_liberal = {
				popularity = 27
			}
			social_liberal = {
				popularity = 8
			}
			social_democrat = {
				popularity = 26
			}
			progressive = {
				popularity = 5
			}
			democratic_socialist = {
				popularity = 10
			}
			communist = {
				popularity = 0
			}
		}
		ruling_party = market_liberal
		last_election = "2013.3.15"
		election_frequency = 48
		elections_allowed = yes
	}
	create_country_leader = {
		name = "Lodewijk Asscher"
		picture = "Lodewijk_Asscher.dds"
		ideology = social_democrat_ideology
	}
	create_country_leader = {
		name = "Jesse Klaver"
		picture = "Jesse_Klaver.dds"
		ideology = green
	}
}