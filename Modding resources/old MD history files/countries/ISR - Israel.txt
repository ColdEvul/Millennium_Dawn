﻿capital = 454

oob = "ISR_2000"

set_convoys = 500
set_stability = 0.5
set_war_support = 0.7

set_country_flag = country_language_hebrew

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	AT_upgrade_1 = 1
	Heavy_Anti_tank_1 = 1
	Anti_tank_1 = 1 
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	corvette1 = 1
	corvette2 = 1

	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
	
}

add_ideas = {
	population_growth_rapid
	idea_ISR_vanguard_of_judaism
	low_economic_mobilisation
	extensive_conscription
}

set_politics = {

	parties = {
		
		islamist = {
			popularity = 10
		}
		nationalist = {
			popularity = 3
		}
		reactionary = {
			popularity = 3
		}
		conservative = {
			popularity = 35
		}
		social_democrat = {
			popularity = 35
		}
		market_liberal = {
			popularity = 4
		}
		communist = {
			popularity = 10
		}
	}
	
	ruling_party = social_democrat
	last_election = "1998.6.1"
	election_frequency = 48
	elections_allowed = yes
}

add_opinion_modifier = {
	target = PER
	modifier = death_to_israel_ISR
}

add_opinion_modifier = {
	target = SYR
	modifier = death_to_israel_ISR
}

add_opinion_modifier = {
	target = LBA
	modifier = death_to_israel_ISR
}

create_country_leader = {
	name = "Ehud Barak"
	ideology = social_democrat_ideology
	picture = "Ehud_Barak.dds"
}

create_country_leader = {
	name = "Benjamin Netanyahu"
	ideology = right_wing_conservative
	picture = "Benjamin_Netanyahu.dds"
}

create_country_leader = {
	name = "Aryeh Deri"
	ideology = absolute_monarchist
	picture = "Aryeh_Deri.dds"
}

create_country_leader = {
	name = "Rehavam Ze'evi"
	ideology = national_democrat
	picture = "Rehavam_Zeevi.dds"
}

create_country_leader = {
	name = "Dan Meridor"
	ideology = libertarian
	picture = "Dan_Meridor.dds"
}

create_country_leader = {
	name = "Yosef Lapid"
	ideology = libertarian
	picture = "Yosef_Lapid.dds"
}

create_country_leader = {
	name = "Yossi Sarid"
	ideology = democratic_socialist_ideology
	picture = "Yossi_Sarid.dds"
}

create_country_leader = {
	name = "Tawfik Toubi"
	ideology = marxist
	picture = "Tawfik_Toubi.dds"
}

create_country_leader = {
	name = "Abdulmalik Dehamshe"
	ideology = islamic_republican
	picture = "Abdulmalik_Dehamshe.dds"
}

create_country_leader = {
	name = "Bentzi Gophstein"
	ideology = national_socialist
	picture = "Bentzi_Gophstein.dds"
}

create_corps_commander = {
	name = "Gabi Ashkenazi"
	picture = "Gabi_Ashkenazi.dds"
	traits = { urban_assault_specialist }
	skill = 2
}

create_corps_commander = {
	name = "Benjamin Gantz"
	picture = "Benjamin_Gantz.dds"
	traits = { urban_assault_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Gadi Eizenkot"
	picture = "Gadi_Eizenkot.dds"
	traits = { commando }
	skill = 1
}

create_corps_commander = {
	name = "Dan Harel"
	picture = "Dan_Harel.dds"
	skill = 1
}

create_corps_commander = {
	name = "Dani Halutz"
	picture = "Dani_Halutz.dds"
	skill = 1
}

create_corps_commander = {
	name = "Yoav Galant"
	picture = "Yoav_Galant.dds"
	skill = 1
}

create_corps_commander = {
	name = "Doron Almog"
	picture = "Doron_Almog.dds"
	skill = 1
}

create_corps_commander = {
	name = "Shaul Mofaz"
	picture = "Shaul_Mofaz.dds"
	skill = 1
}

create_corps_commander = {
	name = "Moshe Yaalon"
	picture = "Moshe_Yaalon.dds"
	skill = 1
}

2016.1.1 = {
	set_politics = {
		parties = {
			islamist = {
				popularity = 10
			}
			nationalist = {
				popularity = 3
			}
			reactionary = {
				popularity = 8
			}
			conservative = {
				popularity = 34
			}
			social_democrat = {
				popularity = 31
			}
			market_liberal = {
				popularity = 4
			}
			communist = {
				popularity = 10
			}
		}
		
		ruling_party = conservative
		last_election = "2012.6.1"
		election_frequency = 48
		elections_allowed = yes
	}
}