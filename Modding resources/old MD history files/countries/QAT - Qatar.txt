﻿capital = 825

oob = "QAT_2000"

set_convoys = 100
set_stability = 0.5

set_country_flag = country_language_arabic

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

set_country_flag = dominant_religion_islam
set_country_flag = sunni_islam

add_ideas = {
	population_growth_rapid
	arab_league_member
}

set_politics = {

	parties = {
		islamist = {
			popularity = 40
		}
		monarchist = {
			popularity = 50
		}
		social_democrat = {
			popularity = 9
		}
		nationalist = {
			popularity = 1
		}
	}
	
	ruling_party = monarchist
	last_election = "1997.1.1"
	election_frequency = 48
	elections_allowed = no
}

create_country_leader = {
	name = "Hamad bin Khalifa"
	picture = "Hamad.dds"
	ideology = absolute_monarchist
}

create_country_leader = {
	name = "Hamad bin Jassim"
	picture = "Hamad_bin_Jassim.dds"
	ideology = constitutional_monarchist
}

create_country_leader = {
	name = "Yusuf al-Qaradawi"
	picture = "Yusuf_Qaradawi.dds"
	ideology = islamic_authoritarian
}

create_country_leader = {
	name = "Hamad bin Ali Al-Attiyah"
	picture = "Hamad_bin_Ali_Attiyah.dds"
	ideology = national_democrat
}

create_country_leader = {
	name = "Wadah Khanfar"
	picture = "Wadah_Khanfar.dds"
	ideology = social_democrat_ideology
}

create_corps_commander = { 
	name = "Hamad bin Ali Al-Attiyah" 
	picture = "Hamad_Al-Attiyah.dds"
	traits = {} 
	skill = 1
}

create_corps_commander = { 
	name = "Ghanem bin Shaheen Al-Ghanem" 
	picture = "Ghanem_bin_Shaheen.dds"
	traits = {} 
	skill = 1
}
2010.1.1 = {
	create_country_leader = {
		name = "Tamim bin Hamad"
		picture = "Tamim.dds"
		ideology = absolute_monarchist
	}
}