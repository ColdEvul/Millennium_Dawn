﻿capital = 271

oob = "ETH_2000"

set_convoys = 10
set_stability = 0.5

set_country_flag = country_language_amharic

#set_country_flag = country_language_oromifa
#set_country_flag = country_language_somali
#set_country_flag = country_language_tigrigna
#set_country_flag = country_language_afar
#set_country_flag = country_language_wolaytta
#set_country_flag = country_language_sidama
#set_country_flag = country_language_kunama
#set_country_flag = country_language_benshangul
#set_country_flag = country_language_gumuz
#set_country_flag = country_language_english
#set_country_flag = country_language_harari

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1

	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_explosion
	african_union_member
}

set_politics = {

	parties = {
		islamist = { popularity = 1 }
		fascist = { popularity = 0 }
		nationalist = { popularity = 1 }
		reactionary = { popularity = 1 }
		conservative = { popularity = 15 }
		market_liberal = { popularity = 1 }
		social_liberal = { popularity = 11 }
		social_democrat = { popularity = 6 }
		progressive = { popularity = 2 }
		democratic_socialist = { popularity = 35 }
		communist = { popularity = 27 }
	}
	
	ruling_party = democratic_socialist
	last_election = "1995.5.18"
	election_frequency = 60
	elections_allowed = yes
}

declare_war_on = {
	target = ERI
	type = annex_everything
}
add_named_threat = {
	name = ethiopia.5.t
	threat = 1
}
add_timed_idea = {
	idea = defensive_military_strategy
	days = 365
}

2000.6.18 = {
	white_peace = ERI
}

add_opinion_modifier = {
	target = ERI
	modifier = rival
}

add_opinion_modifier = {
	target = ERI
	modifier = rival_trade
}

create_country_leader = {
	name = "Abdifatah Sheikh Abdullahi"
	picture = "Abdifatah_Abdullahi.dds"
	ideology = democratic_socialist_ideology
}

create_country_leader = {
	name = "Meles Zenawi"
	picture = "Meles_Zenawi.dds"
	ideology = democratic_socialist_ideology
}

create_country_leader = {
	name = "Birtukan Mideksa"
	picture = "Birtukan_Mideksa.dds"
	ideology = liberalist
}

create_country_leader = {
	name = "Hailu Shawul"
	picture = "Hailu_Shawul.dds"
	ideology = social_democrat_ideology
}

create_country_leader = {
	name = "Dr. Chane Kebede"
	picture = "Chane_Kebede.dds"
	ideology = constitutionalist
}

create_country_leader = {
	name = "Zera Yacob Amha Selassie"
	picture = "Zera_Selassie.dds"
	ideology = absolute_monarchist
}

create_corps_commander = {
	name = "Birhanu Julla"
	picture = "generals/Birhanu_Julla.dds"
	skill = 1
}

create_corps_commander = {
	name = "Samora Yunis"
	picture = "generals/Samora_Yunis.dds"
	skill = 1
}

2001.1.1 = {
	complete_national_focus = ETH_invite_foreign_investors
	complete_national_focus = ETH_invite_coca_cola
	complete_national_focus = ETH_invite_karuturi_global
	complete_national_focus = ETH_invite_lifan_industry
	complete_national_focus = ETH_request_international_loans
	complete_national_focus = ETH_homegrown_companies
	
	complete_national_focus = ETH_international_diplomacy
	complete_national_focus = ETH_african_economic_relationships
	complete_national_focus = ETH_request_chinese_investments
	complete_national_focus = ETH_establish_connections_with_tongji
	complete_national_focus = ETH_expand_the_diplomatic_corps
	complete_national_focus = ETH_the_new_african_union_headquarters
	
	complete_national_focus = ETH_defense_of_ethiopia
	
	complete_national_focus = ETH_the_future_of_ethiopia
	complete_national_focus = ETH_strengthen_the_republic
	complete_national_focus = ETH_endorse_the_african_union
}

2015.5.24 = {
	set_politics = {
		parties = {
			islamist = { popularity = 0 }
			fascist = { popularity = 0 }
			nationalist = { popularity = 0 }
			reactionary = { popularity = 5 }
			conservative = { popularity = 5 }
			market_liberal = { popularity = 5 }
			social_liberal = { popularity = 5 }
			social_democrat = { popularity = 5 }
			progressive = { popularity = 5 }
			democratic_socialist = { popularity = 50 }
			communist = { popularity = 20 }
		}
		ruling_party = democratic_socialist
		last_election = "2015.5.24"
		election_frequency = 60
		elections_allowed = yes
	}
	create_country_leader = {
		name = "Hailemariam Desalegn"
		picture = "Hailemariam_Desalegn.dds"
		ideology = leninist
	}
}