﻿capital = 119

oob = "SOV_2000"

set_convoys = 1000
set_stability = 0.5

set_war_support = 0.8

set_country_flag = country_language_russian

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment = 1
	command_control_equipment = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	APC_3 = 1
	Air_APC_3 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	APC_3 = 1
	Air_APC_3 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	IFV_3 = 1
	Air_IFV_3 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	util_vehicle_equipment_2 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	AT_upgrade_1 = 1
	Heavy_Anti_tank_1 = 1
	Anti_tank_1 = 1 
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	corvette1 = 1
	corvette2 = 1
	frigate_1 = 1
	frigate_2 = 1
	missile_frigate_1 = 1
	missile_frigate_2 = 1
	missile_frigate_2 = 1
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	cruiser_1 = 1
	cruiser_2 = 1
	missile_cruiser_1 = 1
	modern_carrier_0 = 1
	modern_carrier_1 = 1
	modern_carrier_2 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	
	
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	
	
	
}

create_equipment_variant = { name = "Kirov-class Battlecruiser" type = missile_cruiser_2 upgrades = { ship_gun_upgrade = 5 ship_armor_upgrade = 5 }  }
create_equipment_variant = { name = "T-62" type = modern_tank_equipment_1 upgrades = { tank_armor_upgrade = 1 tank_gun_upgrade = 1 tank_reliability_upgrade = 1 tank_engine_upgrade = 1 } obsolete = yes }
create_equipment_variant = { name = "T-64" type = modern_tank_equipment_1 upgrades = { tank_armor_upgrade = 5 tank_gun_upgrade = 1 tank_reliability_upgrade = 1 tank_engine_upgrade = 5 } obsolete = yes }
create_equipment_variant = { name = "T-80" type = modern_tank_equipment_2 upgrades = { tank_armor_upgrade = 5 tank_gun_upgrade = 1 tank_reliability_upgrade = 1 tank_engine_upgrade = 5 } obsolete = yes }
create_equipment_variant = { name = "BMP-2" type = infantry_fighting_vehicle_1 obsolete = yes }
create_equipment_variant = { name = "MiG-31" type = jet_multirole_equipment_2 upgrades = { plane_engine_upgrade = 5 plane_reliability_upgrade = 1 } obsolete = yes }
create_equipment_variant = { name = "Su-27" type = jet_multirole_equipment_3 upgrades = { plane_engine_upgrade = 1 plane_reliability_upgrade = 1 } obsolete = no }
create_equipment_variant = { name = "Su-24" type = jet_attack_equipment_2 upgrades = { plane_engine_upgrade = 5 plane_reliability_upgrade = 1 } obsolete = yes }
create_equipment_variant = { name = "Tu-95MS" type = jet_bomber_equipment_1 upgrades = { plane_bomb_upgrade = 5 plane_reliability_upgrade = 4 } obsolete = yes }
create_equipment_variant = { name = "Tu-22" type = jet_bomber_equipment_1 upgrades = { plane_engine_upgrade = 4 plane_reliability_upgrade = 1 } obsolete = yes }
create_equipment_variant = { name = "Tu-22M3" type = jet_bomber_equipment_1 upgrades = { plane_bomb_upgrade = 1 plane_engine_upgrade = 5 plane_reliability_upgrade = 4 } obsolete = yes }

add_ideas = {
	population_growth_decline
	idea_united_nations_security_council_member
	SOV_corrupt_oligarchy
}

set_politics = {

	parties = {
		nationalist = {
			popularity = 7
		}
		reactionary = {
			popularity = 46
		}
		conservative = {
			popularity = 9
		}
		social_liberal = {
			popularity = 6
		}
		social_democrat = {
			popularity = 1
		}
		progressive = {
			popularity = 0
		}
		democratic_socialist = {
			popularity = 1
		}
		communist = {
			popularity = 10
		}
	}
	
	ruling_party = reactionary
	last_election = "1996.3.20"
	election_frequency = 48
	elections_allowed = yes
}

create_faction = CSTO
add_to_faction = SOV
add_to_faction = BLR
add_to_faction = ARM
add_to_faction = TJI
add_to_faction = KAZ
add_to_faction = KYR

give_guarantee = ABK
give_guarantee = SOS
give_guarantee = TRA

give_military_access = SOS
give_military_access = ABK
give_military_access = TRA
give_military_access = GEO
give_military_access = UKR

create_country_leader = {
	name = "Vladimir Putin"
	picture = "Vladimir_Putin.dds"
	ideology = oligarchist
	traits = {
		popular_figurehead
	}
}
create_country_leader = {
	name = "Gennady Zyuganov"
	picture = "Gennady_Zyuganov.dds"
	ideology = stalinist
}
create_country_leader = {
	name = "Nicholas Romanov"
	picture = "Nicholas_Romanov.dds"
	ideology = absolute_monarchist
}
create_country_leader = {
	name = "Sergey Kiriyenko"
	picture = "Sergey_Kiriyenko.dds"
	ideology = fiscal_conservative
}
create_country_leader = {
	name = "Grigory Yavlinski"
	picture = "Grigory_Yavlinski.dds"
	ideology = liberalist
}
create_country_leader = {
	name = "Alexei Navalny"
	picture = "Alexei_Navalny.dds"
	ideology = progressive_ideology
}
create_country_leader = {
	name = "Mikhail Kasyanov"
	picture = "Mikhail_Kasyanov.dds"
	ideology = libertarian
}
create_country_leader = {
	name = "Gennady Semigin"
	picture = "Gennady_Semigin.dds"
	ideology = democratic_socialist_ideology
}
create_country_leader = {
	name = "Sergei Mironov"
	picture = "Sergei_Mironov.dds"
	ideology = social_democrat_ideology
}
create_country_leader = {
	name = "Vladimir Zhirinovsky"
	picture = "Vladimir_Zhirinovsky.dds"
	ideology = proto_fascist
}
create_country_leader = {
	name = "Andrey Savelyev"
	picture = "Andrey_Savelyev.dds"
	ideology = national_socialist
}

add_namespace = {
	name = "sov_unit_leader"
	type = unit_leader
}

create_field_marshal = {
	name = "Igor Sergeyev"
	picture = "generals/Igor_Sergeyev.dds"
	skill = 1
	traits = { old_guard defensive_doctrine }
}
create_field_marshal = {
	name = "Valery Gerasimov"
	picture = "generals/Valery_Gerasimov.dds"
	skill = 1
	traits = { old_guard }
}
create_field_marshal = {
	name = "Oleg Salyukov"
	picture = "generals/Oleg_Salyukov.dds"
	skill = 1
	traits = { old_guard }
}

create_corps_commander = {
	name = "Aleksandr Dvornikov"
	picture = "generals/Alexander_Dvornikov.dds"
	traits = { ranger }
	skill = 1
}
create_corps_commander = {
	name = "Aleksandr Chayko"
	picture = "generals/Aleksandr_Chayko.dds"
	traits = { panzer_leader }
	skill = 1
}
create_corps_commander = {
	name = "Andrey Serdyukov"
	picture = "generals/Andrey_Serdyukov.dds"
	traits = { commando }
	skill = 1
}
create_corps_commander = {
	name = "Vadim Pankov"
	picture = "generals/Vadim_Pankov.dds"
	traits = { commando }
	skill = 1
}
create_corps_commander = {
	name = "Vladimir Shamanov"
	picture = "generals/Vladimir_Shamanov.dds"
	traits = { commando }
	skill = 1
}
create_corps_commander = {
	name = "Viktor Bondarev"
	picture = "generals/Viktor_Bondarev.dds"
	traits = {  }
	skill = 1
}
create_corps_commander = {
	name = "Aleksandr Golovko"
	picture = "generals/Aleksandr_Golovko.dds"
	traits = { ranger }
	skill = 1
}
create_corps_commander = {
	name = "Sergey Karakaev"
	picture = "generals/Sergey_Karakaev.dds"
	traits = { fortress_buster }
	skill = 1
}
create_corps_commander = {
	name = "Aleksandr Kolpachenko"
	picture = "generals/Aleksandr_Kolpachenko.dds"
	traits = { naval_invader }
	skill = 1
}
create_corps_commander = {
	name = "Aleksandr Fomin"
	picture = "generals/Alexander_Fomin.dds"
	traits = { bearer_of_artillery }
	skill = 1
}
create_corps_commander = {
	name = "Pavel Popov"
	picture = "generals/Pavel_Popov.dds"
	traits = { panzer_leader }
	skill = 1
}
create_corps_commander = {
	name = "Dmitry Bulgakov"
	picture = "generals/Dmitry_Bulgakov.dds"
	traits = { panzer_leader }
	skill = 1
}
create_corps_commander = {
	name = "Igor Jewgenjewitsch Konaschenkow"
	picture = "generals/Igor_J_Konaschenkow.dds"
	traits = { trickster }
	skill = 1
}
create_corps_commander = {
	name = "Yuryi Sadovenko"
	picture = "generals/Yuryi_Sadovenko.dds"
	traits = { trait_engineer }
	skill = 1
}
create_corps_commander = {
	name = "Sergey Shoygu"
	picture = "generals/Sergey_Shoygu.dds"
	skill = 4
	traits = { trickster urban_assault_specialist }
}
create_corps_commander = {
	name = "Igor Sergun"
	picture = "generals/Igor_Sergun.dds"
	skill = 1
}
create_corps_commander = {
	name = "Anatoly Antonov"
	picture = "generals/Anatoly_Antonov.dds"
	skill = 1
}
create_corps_commander = {
	name = "Pavel Grachev"
	picture = "generals/Pavel_Grachev.dds"
	skill = 1
	traits = { old_guard }
}
create_corps_commander = {
	name = "Andrey Kartapolov"
	picture = "generals/Andrey_Kartapolov.dds"
	skill = 1
}
create_corps_commander = {
	name = "Vladimir Zarudnitsky"
	picture = "generals/Vladimir_Zarudnitsky.dds"
	skill = 1
}
create_corps_commander = {
	name = "Sergei Surovikin"
	picture = "generals/Sergei_Surovikin.dds"
	skill = 1
}
create_corps_commander = {
	name = "Vladimir Bakin"
	picture = "generals/Vladimir_Bakin.dds"
	skill = 1
}

create_navy_leader = {
	name = "Vladimir Ivanovich Korolev"
	picture = "admirals/Vladimir_Korolev.dds"
	traits = { old_guard_navy superior_tactician }
	skill = 1
}
create_navy_leader = {
	name = "Aleksandr Nosatov"
	picture = "admirals/Aleksandr_Nosatov.dds"
	traits = { blockade_runner }
	skill = 1
}
create_navy_leader = {
	name = "Nikolay Anatolyevich Evmenov"
	picture = "admirals/Nikolay_Evmenov.dds"
	traits = { ironside }
	skill = 1
}
create_navy_leader = {
	name = "Aleksandr Vitko"
	picture = "admirals/Aleksandr_Vitko.dds"
	traits = { spotter }
	skill = 1
}
create_navy_leader = {
	name = "Sergey Pinchuk"
	picture = "admirals/Sergey_Pinchuk.dds"
	traits = { seawolf }
	skill = 1
}
create_navy_leader = {
	name = "Viktor Chirkov"
	picture = "admirals/Viktor_Cherkov.dds"
	skill = 1
}
create_navy_leader = {
	name = "Vladimir Vysotskiy"
	picture = "admirals/Vladimir_Vysotskiy.dds"
	skill = 1
}


2000.8.12 = { set_country_flag = sov_kursk_sinking }

2001.1.1 = {
	complete_national_focus = SOV_economic_focus
	complete_national_focus = SOV_industrial_project
	complete_national_focus = SOV_focus_on_the_oligarchy
	complete_national_focus = SOV_nationalistic_sentiment
	complete_national_focus = SOV_diplomacy_focus
	complete_national_focus = SOV_modernize_the_military
	complete_national_focus = SOV_support_gazprom
	complete_national_focus = SOV_fight_the_oligarchs
	complete_national_focus = SOV_russia_first
	complete_national_focus = SOV_path_of_the_bear
	complete_national_focus = SOV_path_of_the_dragon
	complete_national_focus = SOV_stand_with_china
	complete_national_focus = SOV_stand_with_autocracy
	complete_national_focus = SOV_support_assad
	complete_national_focus = SOV_economic_intervention
	complete_national_focus = SOV_southern_strategy
	complete_national_focus = SOV_political_control
	complete_national_focus = SOV_secure_the_georgian_republics
	complete_national_focus = SOV_reassure_armenia
	complete_national_focus = SOV_greater_russia
	complete_national_focus = SOV_putinism
	complete_national_focus = SOV_strengthen_CSTO
	complete_national_focus = SOV_focus_on_ukraine
}

2008.1.1 = {
	
	set_party_name = {
		ideology = conservative
		long_name = SOV_conservative_party_PD_long
		name = SOV_conservative_party_PD
	}

}

2010.1.1 = {
	remove_ideas = population_growth_decline
	add_ideas = population_growth_stagnation
}

2012.1.1 = {
	set_politics = {

		parties = {
			nationalist = {
				popularity = 11
			}
			reactionary = {
				popularity = 49
			}
			conservative = {
				popularity = 1
			}
			social_liberal = {
				popularity = 1
			}
			social_democrat = {
				popularity = 13
			}
			progressive = {
				popularity = 1
			}
			democratic_socialist = {
				popularity = 1
			}
			communist = {
				popularity = 19
			}
		}
	
		ruling_party = reactionary
		last_election = "2011.12.4"
		election_frequency = 48
		elections_allowed = yes
	}

	create_country_leader = {
		name = "Emilia Slabunova"
		picture = "Emilia_Slabunova.dds"
		ideology = liberalist
	}

	create_country_leader = {
		name = "Boris Titov"
		picture = "Boris_Titov.dds"
		ideology = fiscal_conservative
	}
}

2014.3.1 = {
	add_opinion_modifier = {
		target = UKR
		modifier = annexed_ukraine_trade
	}
	add_ideas = {
		idea_eu_sanctions
	}
}

2014.3.18 = {
	add_named_threat = {
		threat = 6
		name = threat_SOV_annexiation_of_crimea
	}
}