capital = 199

oob = "AQY_2017"

# Starting tech
set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1
	infantry_weapons = 1
	command_control_equipment = 1
	Anti_tank_0 = 1
	Anti_Air_0 = 1
	util_vehicle_equipment_0 = 1
	Heavy_Anti_tank_0 = 1
}
set_convoys = 20

2017.1.1 = {

add_ideas = {
	pop_050
	crippling_corruption
	gdp_1
	sunni
	defence_09
	edu_01
	health_01
	social_01
	bureau_02
	police_05
	al_jazeera_banned
	volunteer_army
	no_women_in_military
	wahabi_ulema
	foreign_jihadis
	The_Ulema
	tribalism
}
set_country_flag = gdp_1
set_country_flag = enthusiastic_wahabi_ulema
set_country_flag = positive_The_Ulema

#Nat focus
	complete_national_focus = bonus_tech_slots
	declare_war_on = {
		target = YEM
		type = annex_everything
	}

	declare_war_on = {
		target = HOU
		type = annex_everything
	}
	set_politics = {
	parties = {
		democratic = { 
			popularity = 0
		}
		fascism = {
			popularity = 100
		}
		
		communism = {
			popularity = 0
			#banned = no #default is no
		}
		neutrality = { 
			popularity = 0
		}
	}
	
	ruling_party = fascism
	last_election = "2012.3.25"
	election_frequency = 60
	elections_allowed = no
}
create_country_leader = {
	name = "Qasim al-Raymi"
	desc = "POLITICS_AGUSTIN_PEDRO_JUSTO_DESC"
	picture = "AQY_qashim.dds"
	expire = "2065.1.1"
	ideology = Caliphate
	traits = {
		#
	}
}

create_corps_commander = { 
	name = "Khalid Saeed Batarfi" 
	picture = "Gen_Khalid_Batarfi.dds"
	traits = { desert_fox trickster }
	skill = 2
}

create_corps_commander = { 
	name = "Ibrahim al Qosi" 
	picture = "Gen_Ibrahim_Qosi.dds"
	traits = { trait_mountaineer }
	skill = 1
}

add_opinion_modifier = { target = HOU modifier = hostile_status }
add_opinion_modifier = { target = YEM modifier = hostile_status }

}