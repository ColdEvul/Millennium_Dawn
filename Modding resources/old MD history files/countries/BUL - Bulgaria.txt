﻿capital = 48

oob = "BUL_2000"

set_convoys = 21
set_stability = 0.5

set_country_flag = country_language_bulgarian

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	AT_upgrade_1 = 1
	Heavy_Anti_tank_1 = 1
	Anti_tank_1 = 1 
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1 
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	corvette1 = 1
	corvette2 = 1
	frigate_1 = 1
	frigate_2 = 1
	missile_frigate_1 = 1
	missile_frigate_2 = 1
	missile_frigate_2 = 1
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1

	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_decline
}

set_politics = {

	parties = {
		islamist = {
			popularity = 1
		}
		nationalist = {
			popularity = 1
		}
		reactionary = {
			popularity = 2
		}
		conservative = {
			popularity = 30
		}
		market_liberal = {
			popularity = 6
		}
		social_liberal = {
			popularity = 25
		}
		social_democrat = {
			popularity = 5
		}
		progressive = {
			popularity = 10
		}
		democratic_socialist = {
			popularity = 10
		}
		communist = {
			popularity = 10
		}
	}
	
	ruling_party = conservative
	last_election = "1997.1.1"
	election_frequency = 48
	elections_allowed = yes
}

2007.1.1 = {
	add_ideas = {
		idea_eu_member
	}
}

create_country_leader = {
	name = "Ivan Kostov"
	picture = "Ivan_Kostov.dds"
	ideology = christian_democrat 
}

create_country_leader = {
	name = "Boyko Borisov"
	picture = "Boyko_Borisov.dds"
	ideology = centrist  
}

create_country_leader = {
	name = "Simeon Sakskoburggotski"
	picture = "Simeon_Sakskoburggotski.dds"
	ideology = absolute_monarchist 
}

create_country_leader = {
	name = "Georgi Parvanov"
	picture = "Georgi_Parvanov.dds"
	ideology = social_democrat_ideology 
}

create_country_leader = {
	name = "Margarita Mileva"
	picture = "Margarita_Mileva.dds"
	ideology = democratic_socialist_ideology 
}

create_country_leader = {
	name = "Stoyan Panchev"
	picture = "Stoyan_Panchev.dds"
	ideology = libertarian  
}

create_country_leader = {
	name = "Aleksandar Karakachanov"
	picture = "Aleksandar_Karakachanov.dds"
	ideology = green  
}

create_country_leader = {
	name = "Aleksandar Paunov"
	picture = "Aleksandar_Paunov.dds"
	ideology = leninist   
}

create_country_leader = {
	name = "Krasimir Karakachanov"
	picture = "Krasimir_Karakachanov.dds"
	ideology = counter_progressive_democrat    
}

create_country_leader = {
	name = "Volen Siderov "
	picture = "Volen_Siderov.dds"
	ideology = proto_fascist      
}

create_country_leader = {
	name = "Boyan Rasate "
	picture = "Boyan_Rasate.dds"
	ideology = national_socialist      
}


2014.10.5 = {
set_politics = {

	parties = {
		islamist = {
			popularity = 1
		}
		nationalist = {
			popularity = 1
		}
		reactionary = {
			popularity = 2
		}
		conservative = {
			popularity = 25
		}
		market_liberal = {
			popularity = 6
		}
		social_liberal = {
			popularity = 30
		}
		social_democrat = {
			popularity = 5
		}
		progressive = {
			popularity = 10
		}
		democratic_socialist = {
			popularity = 10
		}
		communist = {
			popularity = 10
		}
	}
	
	ruling_party = social_liberal
	last_election = "2013.5.12"
	election_frequency = 48
	elections_allowed = yes
}
}

2017.1.1 = {

oob = "BUL_2017"

set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1

	night_vision_1 = 1
	night_vision_2 = 1
	night_vision_3 = 1
	
	IFV_1 = 1
	IFV_2 = 1
	IFV_3 = 1
	
	SP_arty_equipment_0 = 1	

	gw_artillery = 1	
	
	 #1975

	Heavy_Anti_tank_0 = 1 #1965

	Anti_tank_0 = 1

	SP_Anti_Air_0 = 1 #1965

	Anti_Air_0 = 1 #1965

	Early_APC = 1
	APC_1 = 1 #1965

	combat_eng_equipment = 1

	infantry_weapons = 1
	infantry_weapons1 = 1

	landing_craft = 1
	
	command_control_equipment = 1
	
	ENG_MBT_1 = 1
	
	util_vehicle_equipment_0 = 1
	Rec_tank_0 = 1
	MBT_1 = 1
	
}

add_ideas = {
	pop_050

	small_medium_business_owners
	Labour_Unions
	Industrial_Conglomerates
	orthodox_christian
	unrestrained_corruption
	gdp_4
		stable_growth
		defence_01
	edu_03
	health_03
	social_03
	bureau_03
	police_05
	volunteer_army
	volunteer_women
	NATO_member
	landowners
	small_medium_business_owners
	industrial_conglomerates
	civil_law
}
set_country_flag = gdp_4
set_country_flag = positive_landowners
set_country_flag = positive_small_medium_business_owners
set_country_flag = positive_industrial_conglomerates

#NATO military access
diplomatic_relation = {
	country = ALB
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = BEL
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = CAN
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = CRO
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = CZE
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = DEN
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = EST
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = FRA
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = GER
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = GRE
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = HUN
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = ICE
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = ITA
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = LAT
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = LIT
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = LUX
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = HOL
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = NOR
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = POL
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = POR
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = ROM
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = SLO
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = SLV
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = SPR
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = TUR
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = ENG
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = USA
	relation = military_access
	active = yes
}

#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_4_IC_slot
	complete_national_focus = Generic_8_IC_slot

	set_politics = {

	parties = {
		democratic = { 
			popularity = 77
		}

		fascism = {
			popularity = 0
		}
		
		communism = {
			popularity = 10
		}
		
		neutrality = {
			popularity = 8
		}
		
		nationalist = {
			popularity = 5
		}
	}
	
	ruling_party = democratic
	last_election = "2014.10.5"
	election_frequency = 48
	elections_allowed = yes
}


set_convoys = 5


create_country_leader = {
	name = "Mustafa Karadaya"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "mustafa_karadaya.dds"
	expire = "2065.1.1"
	ideology = liberalism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Mihail Mikov"   
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "mihail_mikov.dds"
	expire = "2065.1.1"
	ideology = socialism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Volen Siderov"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "volen_siderov.dds"
	expire = "2065.1.1"
	ideology = Conservative
	traits = {
		#
	}
}

create_country_leader = {
	name = "Nikolay Barekov"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "nikolay_barekov.dds"
	expire = "2065.1.1"
	ideology = Neutral_conservatism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Krasimir Karakachanov"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "krasimir_karakachanov.dds"
	expire = "2065.1.1"
	ideology = Nat_Populism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Boyko Borissov"
	desc = ""
	picture = "BUL_Boyko_Borissov.dds"
	expire = "2065.1.1"
	ideology = conservatism
	traits = {
		#
	}
}

create_field_marshal = {
	name = "Konstantin Popov"
	picture = "Portrait_Konstantin_Popov.dds"
	traits = { old_guard organisational_leader }
	skill = 3
}

create_field_marshal = {
	name = "Andrei Botsev"
	picture = "Portrait_Andrei_Botsev.dds"
	traits = { thorough_planner }
	skill = 2
}

create_corps_commander = {
	name = "Yavor Mateev"
	picture = "Portrait_Yavor_Mateev.dds"
	traits = { commando urban_assault_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Tsanko Ivanov Stoykov"
	picture = "Portrait_Tsanko_Stoykov.dds"
	traits = {  }
	skill = 2
}

create_corps_commander = {
	name = "Petyo Mirchev"
	picture = "Portrait_Petyo_Mirchev.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Anatoliy Krustev"
	picture = "Portrait_Anatolyi_Krustev.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Lyubcho Spasov Todorov"
	picture = "Portrait_Lyubcho_Todorov.dds"
	traits = { panzer_leader }
	skill = 3
}

create_corps_commander = {
	name = "Todor Tzonev Dochev"
	picture = "Portrait_Todor_Donchev.dds"
	traits = { trait_engineer }
	skill = 2
}

create_corps_commander = {
	name = "Mihail Popov"
	picture = "Portrait_Mihail_Popov.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Rumen Radev"
	picture = "Portrait_Rumen_Radev.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Valeri Konstantin Tsolov"
	picture = "Portrait_Valerie_Tsolov.dds"
	traits = { fortress_buster }
	skill = 1
}

create_corps_commander = {
	name = "Kostadin Kuzmov"
	picture = "Portrait_Kostadin_Kuzmov.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Dimitar Iliev"
	picture = "Portrait_Dimitar_Iliev.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Rusi Rusev"
	picture = "Portrait_Rusi_Rusev.dds"
	traits = { trait_mountaineer }
	skill = 1
}

create_corps_commander = {
	name = "Boyan Stavrev"
	picture = "Portrait_Bojan_Stavrev.dds"
	traits = { urban_assault_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Plamen Yordanov"
	picture = "Portrait_Plamen_Yordanov.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Gruda Angelov"
	picture = "Portrait_Gruda_Angelov.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Yordan Yordanov"
	picture = "Portrait_Yordan_Yordanov.dds"
	traits = { trickster }
	skill = 1
}

create_corps_commander = {
	name = "Atanas Lefterov"
	picture = "Portrait_Atanas_Lefterov.dds"
	traits = { hill_fighter }
	skill = 1
}

create_corps_commander = {
	name = "Georgi Petkov"
	picture = "Portrait_Georgi_Petkov.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Georgi Dimov"
	picture = "Portrait_Georgi_Dimov.dds"
	traits = { trait_engineer }
	skill = 1
}

create_navy_leader = {
	name = "Emil Eftimov"
	picture = "Portrait_Emil_Eftimov.dds"
	traits = { old_guard_navy superior_tactician }
	skill = 3
}

create_navy_leader = {
	name = "Rumen Nikolov"
	picture = "Portrait_Rumen_Nikolov.dds"
	traits = { blockade_runner }
	skill = 2
}

create_navy_leader = {
	name = "Dimitar Vasilev Yordanov"
	picture = "Portrait_Dimitar_Yordanov.dds"
	traits = { spotter }
	skill = 1
}

create_navy_leader = {
	name = "Kosta Andreev"
	picture = "Portrait_Kosta_Andreev.dds"
	traits = { seawolf }
	skill = 1
}

create_navy_leader = {
	name = "Mitko Alexander Petev"
	picture = "Portrait_Mitko_Petev.dds"
	traits = { ironside }
	skill = 2
}

}