﻿capital = 112

oob = "POR_2000"

set_convoys = 240
set_stability = 0.5

set_country_flag = country_language_portuguese

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	corvette1 = 1
	corvette2 = 1
	frigate_1 = 1
	frigate_2 = 1
	missile_frigate_1 = 1
	missile_frigate_2 = 1
	missile_frigate_2 = 1


	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_steady
	idea_eu_member
}

set_politics = {

	parties = {
		social_democrat = {
			popularity = 44.06
		}
		market_liberal = {
			popularity = 32.32
		}
		communist = {
			popularity = 8.99
		}
		conservative = {
			popularity = 8.34
		}
		democratic_socialist = {
			popularity = 2.44
		}
		progressive = {
			popularity = 0.37
		}
		monarchist = {
			popularity = 0.31
		}
	}
	
	ruling_party = social_democrat
	last_election = "1999.10.10"
	election_frequency = 36
	elections_allowed = yes
}

create_country_leader = {
	name = "António Guterres"
	picture = "Antonio_Guterres.dds"
	ideology = social_democrat_ideology
}

create_country_leader = {
	name = "Duarte III Pio de Bragança"
	picture = "Duarte_Pio.dds"
	ideology = absolute_monarchist
}

create_country_leader = {
	name = "Mario Machado"
	picture = "Mario_Machado.dds"
	ideology = national_socialist
}

create_country_leader = {
	name = "Jose Pinto Coelho"
	picture = "Jose_Pinto_Coelho.dds"
	ideology = national_democrat 
}

create_country_leader = {
	name = "Jeronimo de Sousa"
	picture = "Jeronimo_de_Sousa.dds"
	ideology = leninist  
}

create_country_leader = {
	name = "Catarina Martins"
	picture = "Catarina_Martins.dds"
	ideology = democratic_socialist_ideology   
}

create_country_leader = {
	name = "Andre Silva"
	picture = "Andre_Silva.dds"
	ideology = green   
}

create_country_leader = {
	name = "Antonio Marinho e Pinto"
	picture = "Antonio_Marinho_Pinto.dds"
	ideology = centrist    
}

create_country_leader = {
	name = "Jose Manuel Barroso"
	picture = "Jose_Manuel_Barroso.dds"
	ideology = libertarian     
}

create_country_leader = {
	name = "Pablo Portas"
	picture = "Pablo_Portas.dds"
	ideology = christian_democrat      
}

create_country_leader = {
	name = "Joana Camara Pereira"
	picture = "Joana_Camara_Pereira.dds"
	ideology = counter_progressive_democrat     
}

2014.9.1 = {
	create_country_leader = {
		name = "António Costa"
		picture = "Antonio_Costa.dds"
		ideology = social_democrat_ideology
	}
	create_country_leader = {
		name = "Pedro Passos Coelho"
		picture = "Pedro_Passos_Coelho.dds"
		ideology = libertarian 
	}
}