﻿capital = 778

oob = "CAF_2000"

set_convoys = 10
set_stability = 0.5

set_country_flag = country_language_french
set_country_flag = country_language_sango

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1

	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_rapid
	african_union_member
}

set_politics = {

	parties = {
		islamist = {
			popularity = 3
		}
		nationalist = {
			popularity = 0
		}
		reactionary = {
			popularity = 0
		}
		conservative = {
			popularity = 21
		}
		market_liberal = {
			popularity = 3
		}
		social_liberal = {
			popularity = 11
		}
		social_democrat = {
			popularity = 6
		}
		progressive = {
			popularity = 4
		}
		democratic_socialist = {
			popularity = 52
		}
		communist = {
			popularity = 0
		}
	}
	
	ruling_party = democratic_socialist
	last_election = "1999.9.19"
	election_frequency = 72
	elections_allowed = yes
}

create_country_leader = {
	name = "Ange-Félix Patassé"
	picture = "Ange_Patasse.dds"
	ideology = democratic_socialist_ideology
}

create_country_leader = {
	name = "Andre Kolingba"
	picture = "Andre_Kolingba.dds"
	ideology = constitutionalist
}

create_country_leader = {
	name = "David Dacko"
	picture = "David_Dacko.dds"
	ideology = moderate
}

create_country_leader = {
	name = "Jean-Paul Ngoupandé"
	picture = "Jean_Ngoupande.dds"
	ideology = progressive_ideology
}

create_country_leader = {
	name = "Enoch Derant Lakoue"
	picture = "Enoch_Lakoue.dds"
	ideology = social_democrat_ideology
}

create_country_leader = {
	name = "Charles Massi"
	picture = "Charles_Massi.dds"
	ideology = libertarian
}

create_country_leader = {
	name = "Michel Djotodia"
	picture = "Michel_Djotodia.dds"
	ideology = islamic_authoritarian
}

create_country_leader = {
	name = "Anicet-Georges Dologuélé"
	picture = "Anicet_Dologuele.dds"
	ideology = counter_progressive_democrat
}

create_corps_commander = {
	name = "Marcel Mombeka"
	picture = "generals/Marcel_Mombeka.dds"
	skill = 1
}

2013.1.1 = {
	set_party_name = {
		ideology = social_liberal
		name = CAF_social_liberal_party_IND
	}
}

2015.12.30 = {
	set_politics = {
		parties = {
			islamist = {
				popularity = 5
			}
			nationalist = {
				popularity = 0
			}
			reactionary = {
				popularity = 13
			}
			conservative = {
				popularity = 10
			}
			market_liberal = {
				popularity = 0
			}
			social_liberal = {
				popularity = 61
			}
			social_democrat = {
				popularity = 0
			}
			progressive = {
				popularity = 1
			}
			democratic_socialist = {
				popularity = 10
			}
			communist = {
				popularity = 0
			}
		}
		ruling_party = social_liberal
		last_election = "2015.12.30"
		election_frequency = 60
		elections_allowed = yes
	}
	create_country_leader = {
		name = "Faustin-Archange Touadéra"
		picture = "Faustin_Touadera.dds"
		ideology = moderate
	}
	create_country_leader = {
		name = "Désiré Kolingba"
		picture = "Desire_Kolingba.dds"
		ideology = constitutionalist
	}
	create_country_leader = {
		name = "Martin Ziguélé"
		picture = "Martin_Ziguele.dds"
		ideology = democratic_socialist_ideology
	}
}