﻿capital = 307

oob = "VEN_2000"

set_convoys = 350
set_stability = 0.5

set_country_flag = country_language_spanish

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	corvette1 = 1
	corvette2 = 2
	frigate_1 = 1
	frigate_2 = 1
	missile_frigate_1 = 1
	missile_frigate_2 = 1
	frigate2 = 2
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1

	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_rapid
}

set_politics = {

	parties = {
		
		islamist = {
			popularity = 0
		}
		nationalist = {
			popularity = 10
		}
		reactionary = {
			popularity = 0
		}
		conservative = {
			popularity = 10
		}
		market_liberal = {
			popularity = 0
		}
		social_liberal = {
			popularity = 10
		}
		social_democrat = {
			popularity = 0
		}
		progressive = {
			popularity = 10
		}
		democratic_socialist = {
			popularity = 50
		}
		communist = {
			popularity = 10
		}
	}
	
	ruling_party = democratic_socialist
	last_election = "1997.1.1"
	election_frequency = 48
	elections_allowed = yes
}

create_country_leader = {
	name = "Hugo Chavez"
	picture = "gfx/leaders/VEN/Hugo_Chavez.dds"
	ideology = democratic_socialist_ideology
}

create_country_leader = {
	name = "Henrique Capriles"
	picture = "Henrique_C_Radonski.dds"
	ideology = libertarian 
}

create_country_leader = {
	name = "Henry Ramos Allup"
	picture = "Henry_Ramos_Allup.dds"
	ideology = centrist 
}

create_country_leader = {
	name = "Omar Barboza"
	picture = "Omar_Barboza.dds"
	ideology = social_democrat_ideology  
}

create_country_leader = {
	name = "Leopoldo Lopez"
	picture = "Leopoldo_Lopez.dds"
	ideology = progressive_ideology   
}

create_country_leader = {
	name = "Henrique Salas Romer"
	picture = "Henrique_Salas_Romer.dds"
	ideology = fiscal_conservative    
}

create_country_leader = {
	name = "Oscar Figuera"
	picture = "Oscar_Figuera.dds"
	ideology = marxist    
}

create_field_marshal = {
	name = "Vladimir Padrino Lopez"
	picture = "Portrait_Vladimir_Padrino_Lopez.dds"
	traits = { old_guard thorough_planner }
	skill = 2
}

create_field_marshal = {
	name = "Juan de Jesus Garcia Toussaintt"
	picture = "Portrait_Juan_Garcia_Barrios_Toussaintt.dds"
	traits = { organisational_leader }
	skill = 1
}

create_corps_commander = {
	name = "Jesus Alberto Milano Mendoza"
	picture = "Portrait_Jesus_Alberto_Milano_Mendoza.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Edgar Valentin Cruz Arteaga"
	picture = "Portrait_Edgar_Valentin_Cruz_Arteaga.dds"
	traits = { commando }
	skill = 1
}

create_corps_commander = {
	name = "Nestor Luis Reverol Torres"
	picture = "Portrait_Nestor_Luis_Reverol_Torres.dds"
	traits = { urban_assault_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Antonio Benavidez Torrez"
	picture = "Portrait_Antonio_Benavido_Torres.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Cecar Vega Gonzalez"
	picture = "Portrait_Cesar_Vega_Gonzalez.dds"
	traits = { trickster }
	skill = 1
}

create_corps_commander = {
	name = "Henry Rangel Silva"
	picture = "Portrait_Henry_Rangel_Silva.dds"
	traits = { hill_fighter }
	skill = 1
}

create_corps_commander = {
	name = "Jose Morantes Torres"
	picture = "Portrait_Jose_Morantes_Torres.dds"
	traits = { jungle_rat }
	skill = 1
}

create_corps_commander = {
	name = "Luis Medina Fernandes"
	picture = "Portrait_Luis_Medina_Fernandes.dds"
	traits = { trait_engineer }
	skill = 1
}

create_navy_leader = {
	name = "Orlando Miguel Maneiro Gaspar"
	picture = "Portrait_Orlando_Miguel_Maneiro.dds"
	traits = { old_guard_navy superior_tactician }
	skill = 2
}

create_navy_leader = {
	name = "Carlos Jose Vieira Acevedo"
	picture = "Portrait_Carlos_Viera_Acevedo.dds"
	traits = { blockade_runner }
	skill = 1
}

2013.1.1 = {
	create_country_leader = {
		name = "Nicolas Maduro"
		picture = "nicolas_maduro.dds"
		ideology = democratic_socialist_ideology
	}
}

2016.1.1 = {

	set_politics = {

	parties = {
		
		islamist = {
			popularity = 0
		}
		nationalist = {
			popularity = 10
		}
		reactionary = {
			popularity = 0
		}
		conservative = {
			popularity = 10
		}
		market_liberal = {
			popularity = 0
		}
		social_liberal = {
			popularity = 10
		}
		social_democrat = {
			popularity = 0
		}
		progressive = {
			popularity = 10
		}
		democratic_socialist = {
			popularity = 50
		}
		communist = {
			popularity = 10
		}
	}
	
	ruling_party = democratic_socialist
	last_election = "2013.1.1"
	election_frequency = 48
	elections_allowed = yes
	}
}