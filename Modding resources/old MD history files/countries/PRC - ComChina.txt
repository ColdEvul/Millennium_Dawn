﻿capital = 608

oob = "PRC_2000"

set_convoys = 1150
set_stability = 0.5

set_country_flag = country_language_chinese

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	modern_gen3_light = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	APC_3 = 1
	Air_APC_3 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	IFV_3 = 1
	Air_IFV_3 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	util_vehicle_equipment_2 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	AT_upgrade_1 = 1
	Heavy_Anti_tank_1 = 1
	Anti_tank_1 = 1 
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	corvette1 = 1
	corvette2 = 1
	frigate_1 = 1
	frigate_2 = 1
	missile_frigate_1 = 1
	missile_frigate_2 = 1
	missile_frigate_2 = 1
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	missile_destroyer_2 = 1
	missile_destroyer_3 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	
	
	
}

create_equipment_variant = { name = "J-6" type = jet_multirole_equipment_1 upgrades = { plane_gun_upgrade = 2 plane_engine_upgrade = 2 plane_reliability_upgrade = 2 } obsolete = yes }
create_equipment_variant = { name = "J-7" type = jet_multirole_equipment_1 upgrades = { plane_gun_upgrade = 5 plane_engine_upgrade = 5 plane_reliability_upgrade = 5 } obsolete = yes }
create_equipment_variant = { name = "H-5" type = jet_bomber_equipment_1  obsolete = yes }



add_ideas = {
	population_growth_stagnation
	idea_PRC_chinese_socialism
	idea_united_nations_security_council_member
	free_trade
}

give_guarantee = PRK

#Time to puppet the folks.
set_politics = {
	ruling_party = conservative
}
if = {
	limit = {
		has_dlc = "Together for Victory"
	}
	set_autonomy = {
		target = HKN
		autonomous_state = autonomy_integrated_puppet
	}
	set_autonomy = {
		target = MCU
		autonomous_state = autonomy_integrated_puppet
	}
	else = {
		puppet = HKN
		puppet = MCU
	}
}

add_opinion_modifier = {
	target = JAP
	modifier = past_japanese_war_crimes
}

add_opinion_modifier = {
	target = USA
	modifier = chinese_american_rivalry
}

set_politics = {
	parties = {
		fascist = { popularity = 0.2 }
		nationalist = { popularity = 0.2 }
		monarchist = { popularity = 1.1 }
		reactionary = { popularity = 0.5 }
		conservative = { popularity = 8.95 }
		market_liberal = { popularity = 9.65 }
		social_liberal = { popularity = 7.1 }
		social_democrat = { popularity = 12.3 }
		progressive = { popularity = 2.05 }
		democratic_socialist = { popularity = 9.55 }
		communist = { popularity = 48.4 }
	}
	ruling_party = communist
	last_election = "1997.1.1"
	election_frequency = 48
	elections_allowed = no
}

create_country_leader = {
	name = "Jiang Zemin"
	picture = "Jiang_Zemin.dds"
	ideology = maoist
}

create_country_leader = {
	name = "Ding Shisun"
	picture = "Ding_Shisun.dds"
	ideology = moderate
}

create_country_leader = {
	name = "Cheng Siwei"
	picture = "Cheng_Siwei.dds"
	ideology = libertarian
}

create_country_leader = {
	name = "Jiang Zhenghua"
	picture = "Jiang_Zhenghua.dds"
	ideology = progressive_ideology
}

create_country_leader = {
	name = "He Luli"
	picture = "He_Luli.dds"
	ideology = democratic_socialist_ideology
}

create_country_leader = {
	name = "Jin Youzhi"
	picture = "Jin_Youzhi.dds"
	ideology = absolute_monarchist
}

create_country_leader = {
	name = "Xu Jialu"
	picture = "Xu_Jialu.dds"
	ideology = social_democrat_ideology
}

create_field_marshal = {
	name = "Chen Bingde"
	picture = "generals/Chen_Bingde.dds"
	skill = 3
}

create_field_marshal = {
	name = "Liu Zuocheng"
	picture = "generals/Liu_Zuocheng.dds"
	skill = 2
}

create_field_marshal = {
	name = "Guo Boxiong"
	picture = "generals/Guo_Boxiong.dds"
	skill = 1
}

create_field_marshal = {
	name = "Cao Gangchuan"
	picture = "generals/Cao_Gangchuan.dds"
	skill = 1
}

create_corps_commander = {
	name = "Liang Guanglie"
	picture = "generals/Liang_Guanglie.dds"
	skill = 3
}

#Gao Xiaoyan is a former icon of both Chinese women and the Chinese military.
#She was disgraced in 2012 - possible future event?
create_corps_commander = {
	name = "Gao Xiaoyan"
	picture = "generals/Gao_Xiaoyan.dds"
	skill = 3
}

create_corps_commander = {
	name = "Xu Caihou"
	picture = "generals/Xu_Caihou.dds"
	skill = 2
}

create_corps_commander = {
	name = "Chang Wanquan"
	picture = "generals/Chang_Wanquan.dds"
	skill = 2
}

create_corps_commander = {
	name = "Fang Fenghui"
	picture = "generals/Fang_Fenghui.dds"
	skill = 2
}

create_corps_commander = {
	name = "Li Xianyu"
	picture = "generals/Li_Xianyu.dds"
	skill = 1
}

create_corps_commander = {
	name = "Fan Changlong"
	picture = "generals/Fan_Changlong.dds"
	skill = 1
}

create_corps_commander = {
	name = "Wei Fenghe"
	picture = "generals/Wei_Fenghe.dds"
	skill = 1
}

create_corps_commander = {
	name = "Wang Ning"
	picture = "generals/Wang_Ning.dds"
	skill = 1
}

create_corps_commander = {
	name = "Liu Yuejun"
	picture = "generals/Liu_Yuejun.dds"
	skill = 1
}

create_corps_commander = {
	name = "Qu Jiangguo"
	picture = "generals/Qi_Jiangguo.dds"
	skill = 1
}

create_corps_commander = {
	name = "Wang Jiaocheng"
	picture = "generals/Wang_Jiaocheng.dds"
	skill = 1
}

create_corps_commander = {
	name = "Zhang Shibo"
	picture = "generals/Zhang_Shibo.dds"
	skill = 1
}

create_corps_commander = {
	name = "Zhao Zongqi"
	picture = "generals/Zhao_Zongqi.dds"
	skill = 1
}

create_navy_leader = {
	name = "Sun Jianguo"
	picture = "admirals/Sun_Jianguo.dds"
	skill = 2
}

create_navy_leader = {
	name = "Wu Shengli"
	picture = "admirals/Wu_Shengli.dds"
	skill = 1
}

create_navy_leader = {
	name = "Yao Zhilou"
	picture = "admirals/Yao_Zhilou.dds"
	skill = 1
}

create_navy_leader = {
	name = "Yuan Yubai"
	picture = "admirals/Yuan_Yubai.dds"
	skill = 1
}

2002.1.1 = {
	complete_national_focus = socialism_with_chinese_characteristics
	complete_national_focus = PRC_economic_revitalization
	complete_national_focus = PRC_political_reform
	complete_national_focus = PRC_military_reorganization
	complete_national_focus = PRC_industrial_safety
	complete_national_focus = PRC_modern_weapons_systems
}

2004.1.1 = {
	complete_national_focus = PRC_central_infrastructure
	complete_national_focus = PRC_modern_heavy_weapons
	complete_national_focus = PRC_army_motorization
	complete_national_focus = PRC_modern_fighter_aircraft
	complete_national_focus = PRC_naval_rearmament
}

2008.1.1 = {
	complete_national_focus = PRC_northern_infrastructure
	complete_national_focus = PRC_northern_industry
	complete_national_focus = PRC_western_infrastructure
	complete_national_focus = PRC_armor_design_and_production
	complete_national_focus = PRC_modern_attack_aircraft
	complete_national_focus = PRC_littoral_defense
}

2010.1.1 = {
	create_country_leader = {
		name = "Xi Jinping"
		picture = "Xi_Jinping.dds"
		ideology = maoist
	}
	create_country_leader = {
		name = "Zhang Baowen"
		picture = "Zhang_Baowen.dds"
		ideology = moderate
	}
	create_country_leader = {
		name = "Chen Changzhi"
		picture = "Chen_Changzhi.dds"
		ideology = libertarian
	}
	create_country_leader = {
		name = "Chen Zhu"
		picture = "Chen_Zhu.dds"
		ideology = progressive_ideology
	}
	create_country_leader = {
		name = "Wan Exiang"
		picture = "Wan_Exiang.dds"
		ideology = democratic_socialist_ideology
	}
	create_country_leader = {
		name = "Yan Junqi"
		ideology = social_democrat_ideology
	}
}

2015.4.10 = {
create_country_leader = {
	name = "Jin Yuzhang"
	picture = "Jin_Yuzhang.dds"
	ideology = absolute_monarchist
}
}



2015.6.1 = {
	add_named_threat = {
		threat = 2
		name = threat_PRC_south_china_sea
	}
}