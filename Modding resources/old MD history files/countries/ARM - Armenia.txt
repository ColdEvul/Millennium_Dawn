﻿capital = 230

oob = "ARM_2000"

set_convoys = 20
set_stability = 0.5

set_country_flag = country_language_armenian

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

give_guarantee = NGK

add_ideas = {
	population_growth_stagnation
	limited_conscription
}

add_opinion_modifier = { target = TUR modifier = rival }
add_opinion_modifier = { target = AZR modifier = rival }
give_military_access = NGK

set_politics = {
	parties = {
		reactionary = { popularity = 30 }
		conservative = { popularity = 40 }
		market_liberal = { popularity = 15 }
		progressive = { popularity = 3 }
		democratic_socialist = { popularity = 2 }
		communist = { popularity = 10 }
	}
	
	ruling_party = conservative
	last_election = "1998.3.30"
	election_frequency = 60
	elections_allowed = yes
}

create_country_leader = {
	name = "Robert Kocharyan"
	picture = "Robert_Kocharyan.dds"
	ideology = fiscal_conservative
}

create_field_marshal = {
	name = "Movses Hakobyan"
	picture = "Portrait_Movses_Hakobyan.dds"
	traits = { old_guard organisational_leader }
	skill = 1
}

create_corps_commander = {
	name = "Haykaz Papik Baghmanyan"
	picture = "Portrait_Haykaz_Baghmanyan.dds"
	traits = { bearer_of_artillery }
	skill = 1
}

create_corps_commander = {
	name = "Stepan Robert Galstyan"
	picture = "Portrait_Stepan_Galstyan.dds"
	traits = { commando }
	skill = 1
}

create_corps_commander = {
	name = "Onik viktor Gasparyan"
	picture = "Portrait_Onik_Gasparyan.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Aris Brutyan"
	picture = "Portrait_Aris_Brutyan.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Artak Davtyan"
	picture = "Portrait_Artak_Davtyan.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Albert Mardoyan"
	picture = "Portrait_Albert_Mardoyan.dds"
	traits = { fortress_buster }
	skill = 1
}

create_corps_commander = {
	name = "Avetik Muradyan"
	picture = "Portrait_Avetik_Muradyan.dds"
	traits = { commando }
	skill = 1
}

create_corps_commander = {
	name = "Armen Vardanyan"
	picture = "Portrait_Armen_Vardanyan.dds"
	traits = { fortress_buster }
	skill = 1
}

create_corps_commander = {
	name = "Temur Shahnazaryan"
	picture = "Portrait_Temur_Shahnazaryan.dds"
	traits = { trickster }
	skill = 1
}

create_corps_commander = {
	name = "Ishkan Matevosyan"
	picture = "Portrait_Ishkan_Matevosyan.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Vardan Nshanyan"
	picture = "Portrait_Vardan_Nshanyan.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Poghos Poghosyan"
	picture = "Portrait_Poghos_Poghosyan.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Norayr Yolchyan"
	picture = "Portrait_Norayr_Yolchyan.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Tiran Khacatryan"
	picture = "Portrait_Tiran_Khacatryan.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Kamo Qochunts"
	picture = "Portrait_Kamo_Qochunts.dds"
	traits = { urban_assault_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Hayk Karapetyan"
	picture = "Portrait_Hayk_Karapetyan.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Aleksan Aleksanyan"
	picture = "Portrait_Aleksan_Aleksanyan.dds"
	traits = { hill_fighter }
	skill = 1
}

create_corps_commander = {
	name = "Genadi Tavaratsyan"
	picture = "Portrait_Genadi_Tavaratsyan.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Henrik Muradyan"
	picture = "Portrait_Henrik_Muradyan.dds"
	traits = { urban_assault_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Kamavor Khachatryan"
	picture = "Portrait_Kamavor_Khachatryan.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Vahan Avetisyan"
	picture = "Portrait_Vahan_Avetisyan.dds"
	traits = { trickster }
	skill = 1
}

create_corps_commander = {
	name = "Artur Chatyan"
	picture = "Portrait_Artur_Chatyan.dds"
	traits = { trickster }
	skill = 1
}

create_corps_commander = {
	name = "Yuri Khachaturov"
	picture = "Portrait_Yuri_Khachaturov.dds"
	traits = {  }
	skill = 1
}
2017.1.1 = {

oob = "ARM_2017"



set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1
	#K-3 Rifle
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	
	#N-2 MRLS
	gw_artillery = 1
	SP_arty_equipment_0 = 1
	SP_arty_equipment_1 = 1
	SP_arty_equipment_2 = 1
	SP_R_arty_equipment_0 = 1
	SP_R_arty_equipment_1 = 1
	SP_R_arty_equipment_2 = 1
	
	night_vision_1 = 1
	
	#For templates
	
	combat_eng_equipment = 1
	command_control_equipment = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	Early_APC = 1
	APC_1 = 1
	IFV_1 = 1
	util_vehicle_equipment_0 = 1
	Rec_tank_0 = 1
	MBT_1 = 1
}

add_ideas = {
	pop_050
	#small_medium_business_owners
	#Labour_Unions
	#Mass_Media
	orthodox_christian
	unrestrained_corruption
	gdp_3
	    stable_growth
		defence_04
	edu_02
	health_02
	social_01
	bureau_02
	police_03
	draft_army
	volunteer_women
	international_bankers
	landowners
	The_Clergy
	civil_law
	}
set_country_flag = gdp_3
set_country_flag = positive_international_bankers
set_country_flag = negative_landowners

#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot

	set_politics = {

	parties = {
		democratic = { 
			popularity = 20
		}

		fascism = {
			popularity = 0
		}
		
		communism = {
			popularity = 65
		}
		
		neutrality = {
			popularity = 10
		}
		
		nationalist = {
			popularity = 5
		}
	}
	
	ruling_party = communism
	last_election = "2013.2.18"
	election_frequency = 60
	elections_allowed = yes
}

create_country_leader = {
	name = "Raffi Hovannisian"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "raffi_hovannisian.dds"
	expire = "2065.1.1"
	ideology = liberalism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Hrant Markarian"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "hrant_markarian.dds"
	expire = "2065.1.1"
	ideology = neutral_Social
	traits = {
		#
	}
}

create_country_leader = {
	name = "Paruyr Hayrikyan"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "paruyr_hayrikyan.dds"
	expire = "2065.1.1"
	ideology = Nat_Populism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Hovik Abrahamyan"
	desc = "POLITICS_KING_ZOG_DESC"
	picture = "ARM_Hovik_Abrahamyan.dds"
	expire = "2065.1.1"
	ideology = Conservative
	traits = {
		#
	}
}

create_country_leader = {
	name = "Serzh Sargsyan"
	desc = "POLITICS_KING_ZOG_DESC"
	picture = "serzh_sargsyan.dds"
	expire = "2065.1.1"
	ideology = Conservative
	traits = {
		#
	}
}

create_field_marshal = {
	name = "Movses Hakobyan"
	picture = "Portrait_Movses_Hakobyan.dds"
	traits = { old_guard organisational_leader }
	skill = 3
}

create_corps_commander = {
	name = "Haykaz Papik Baghmanyan"
	picture = "Portrait_Haykaz_Baghmanyan.dds"
	traits = { bearer_of_artillery }
	skill = 3
}

create_corps_commander = {
	name = "Stepan Robert Galstyan"
	picture = "Portrait_Stepan_Galstyan.dds"
	traits = { commando }
	skill = 3
}

create_corps_commander = {
	name = "Onik viktor Gasparyan"
	picture = "Portrait_Onik_Gasparyan.dds"
	traits = { trait_engineer }
	skill = 3
}

create_corps_commander = {
	name = "Aris Brutyan"
	picture = "Portrait_Aris_Brutyan.dds"
	traits = {  }
	skill = 2
}

create_corps_commander = {
	name = "Artak Davtyan"
	picture = "Portrait_Artak_Davtyan.dds"
	traits = { panzer_leader }
	skill = 2
}

create_corps_commander = {
	name = "Albert Mardoyan"
	picture = "Portrait_Albert_Mardoyan.dds"
	traits = { fortress_buster }
	skill = 2
}

create_corps_commander = {
	name = "Avetik Muradyan"
	picture = "Portrait_Avetik_Muradyan.dds"
	traits = { commando }
	skill = 2
}

create_corps_commander = {
	name = "Armen Vardanyan"
	picture = "Portrait_Armen_Vardanyan.dds"
	traits = { fortress_buster }
	skill = 1
}

create_corps_commander = {
	name = "Temur Shahnazaryan"
	picture = "Portrait_Temur_Shahnazaryan.dds"
	traits = { trickster }
	skill = 1
}

create_corps_commander = {
	name = "Ishkan Matevosyan"
	picture = "Portrait_Ishkan_Matevosyan.dds"
	traits = { trait_engineer }
	skill = 2
}

create_corps_commander = {
	name = "Vardan Nshanyan"
	picture = "Portrait_Vardan_Nshanyan.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Poghos Poghosyan"
	picture = "Portrait_Poghos_Poghosyan.dds"
	traits = { ranger }
	skill = 2
}

create_corps_commander = {
	name = "Norayr Yolchyan"
	picture = "Portrait_Norayr_Yolchyan.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Tiran Khacatryan"
	picture = "Portrait_Tiran_Khacatryan.dds"
	traits = { ranger }
	skill = 2
}

create_corps_commander = {
	name = "Kamo Qochunts"
	picture = "Portrait_Kamo_Qochunts.dds"
	traits = { urban_assault_specialist }
	skill = 2
}

create_corps_commander = {
	name = "Hayk Karapetyan"
	picture = "Portrait_Hayk_Karapetyan.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Aleksan Aleksanyan"
	picture = "Portrait_Aleksan_Aleksanyan.dds"
	traits = { hill_fighter }
	skill = 2
}

create_corps_commander = {
	name = "Genadi Tavaratsyan"
	picture = "Portrait_Genadi_Tavaratsyan.dds"
	traits = { panzer_leader }
	skill = 2
}

create_corps_commander = {
	name = "Henrik Muradyan"
	picture = "Portrait_Henrik_Muradyan.dds"
	traits = { urban_assault_specialist }
	skill = 2
}

create_corps_commander = {
	name = "Kamavor Khachatryan"
	picture = "Portrait_Kamavor_Khachatryan.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Vahan Avetisyan"
	picture = "Portrait_Vahan_Avetisyan.dds"
	traits = { trickster }
	skill = 1
}

create_corps_commander = {
	name = "Artur Chatyan"
	picture = "Portrait_Artur_Chatyan.dds"
	traits = { trickster }
	skill = 1
}

create_corps_commander = {
	name = "Yuri Khachaturov"
	picture = "Portrait_Yuri_Khachaturov.dds"
	traits = {  }
	skill = 1
}

}