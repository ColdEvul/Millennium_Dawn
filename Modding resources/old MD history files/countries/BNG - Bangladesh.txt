﻿capital = 430

oob = "BNG_2000"

set_convoys = 20
set_stability = 0.5

set_country_flag = country_language_bengali

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	AT_upgrade_1 = 1
	Heavy_Anti_tank_1 = 1
	Anti_tank_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	corvette1 = 1
	corvette2 = 1
	corvette3 = 1
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	frigate_1 = 1
	frigate_2 = 1
	missile_frigate_1 = 1
	missile_frigate_2 = 1
	missile_frigate_2 = 1


	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	submarine3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_rapid
	commonwealth_of_nations_member
}

set_country_flag = dominant_religion_islam
set_country_flag = sunni_islam

set_politics = {

	parties = {
		islamist = {
			popularity = 16
		}
		conservative = {
			popularity = 5
		}
		social_democrat = {
			popularity = 44
		}
		social_liberal = {
			popularity = 10
		}
		communist = {
			popularity = 20
		}
	}
	
	ruling_party = social_democrat
	last_election = "1997.1.1"
	election_frequency = 48
	elections_allowed = yes
}

create_country_leader = {
	name = "Sheikh Hasina"
	picture = "Sheikh_Hasina.dds"
	ideology = social_democrat_ideology
}
2017.1.1 = {

oob = "BAN_2017"

set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1

	#Durjoy-class
	corvette_1 = 1
	corvette_2 = 1
	missile_corvette_1 = 1
	missile_corvette_2 = 1
	missile_corvette_3 = 1
	missile_corvette_4 = 1
	
	night_vision_1 = 1
	night_vision_2 = 1
	
	#For Templates
	infantry_weapons = 1
	
	combat_eng_equipment = 1
	command_control_equipment = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	gw_artillery = 1
	SP_arty_equipment_0 = 1
	Anti_Air_0 = 1
	Early_APC = 1
	APC_1 = 1
	IFV_1 = 1
	MBT_1 = 1
	util_vehicle_equipment_0 = 1
	
	landing_craft = 1
	
}

add_ideas = {
	pop_050
	systematic_corruption
	gdp_2
	sunni
	saudi_aid
	    fast_growth
		defence_02
	edu_01
	health_01
	social_01
	bureau_01
	police_01
	volunteer_army
	volunteer_women
	small_medium_business_owners
	industrial_conglomerates
	The_Ulema
	hybrid
}
set_country_flag = gdp_2
set_country_flag = Major_Importer_CHI_Arms
set_country_flag = positive_small_medium_business_owners
set_country_flag = positive_industrial_conglomerates
set_country_flag = positive_The_Ulema


#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4_IC_slot
	complete_national_focus = Generic_8_IC_slot
	complete_national_focus = Generic_16_IC_slot
	set_politics = {

	parties = {
		democratic = { 
			popularity = 14
		}

		fascism = {
			popularity = 17
		}
		
		communism = {
			popularity = 13
			#banned = no #default is no
		}
		
		neutrality = { 
			popularity = 56
		}
	}
	
	ruling_party = neutrality
	last_election = "2014.1.5"
	election_frequency = 60
	elections_allowed = yes
}

create_country_leader = {
	name = "Muhammad Asadullah Al-Ghalib"
	desc = ""
	picture = "BAN_Salafi_Muhammad_Asadullah_Al-Ghalib"
	expire = "2065.1.1"
	ideology = caliphate
	traits = {
		#
	}
}

create_country_leader = {
	name = "Sheikh Hasina"
	desc = ""
	picture = "BAN_Sheikh_Hasina.dds"
	expire = "2065.1.1"
	ideology = neutral_Social
	traits = {
		#
	}
}
create_field_marshal = {
	name = "Abu Belal Muhammad Shafiul Huq"
	picture = "generals/Portrait_Huq.dds"
	traits = { old_guard organisational_leader }
	skill = 4
}

create_field_marshal = {
	name = "Aziz Ahmed"
	picture = "generals/Portrait_Aziz_Ahmed.dds"
	traits = { fast_planner }
	skill = 3
}

create_field_marshal = {
	name = "Abu Esrar"
	picture = "generals/Portrait_Abu_Esrar.dds"
	traits = { logistics_wizard }
	skill = 3
}

create_corps_commander = {
	name = "Shah Noor Jilani"
	picture = "generals/Portrait_Shah_Noor_Jilani.dds"
	traits = { commando }
	skill = 1
}

create_corps_commander = {
	name = "Fakhrul Ahsan"
	picture = "generals/Portrait_Fakhrul_Ahsan.dds"
	traits = { trickster }
	skill = 1
}

create_corps_commander = {
	name = "Mohammad Maksudur Rahman"
	picture = "generals/Portrait_Rahman.dds"
	traits = { ranger }
	skill = 2
}

create_corps_commander = {
	name = "Jahangir Kabir Talukder"
	picture = "generals/Portrait_Talukder.dds"
	traits = { swamp_fox }
	skill = 2
}

create_corps_commander = {
	name = "Ibne Fazal Shayekhuzzaman"
	picture = "generals/Portrait_Shayekhuzzaman.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Abul Mansur Ashraf Khan"
	picture = "generals/Portrait_Ashraf_Khan.dds"
	traits = { hill_fighter }
	skill = 2
}

create_corps_commander = {
	name = "Sheikh Pasha Habib Uddin"
	picture = "generals/Portrait_Sheikh_Pasha.dds"
	traits = { urban_assault_specialist }
	skill = 2
}

create_corps_commander = {
	name = "Saiful Abedin"
	picture = "generals/Portrait_Saiful_Abedin.dds"
	traits = { trickster }
	skill = 2
}

create_navy_leader = {
	name = "Mohammad Nizamuddin Ahmed"
	picture = "admirals/Portrait_Nizamuddin.dds"
	traits = { old_guard_navy superior_tactician }
	skill = 4
}

create_navy_leader = {
	name = "Makbul Hossain"
	picture = "admirals/Portrait_Makbul_Hossain.dds"
	traits = { seawolf }
	skill = 2
}

create_navy_leader = {
	name = "Shafiul Azam"
	picture = "admirals/Portrait_Shafiul_Azam.dds"
	traits = { ironside }
	skill = 2
}

create_navy_leader = {
	name = "Lokmanur Rahman"
	picture = "admirals/Portrait_Lokmanur_Rahman.dds"
	traits = { blockade_runner }
	skill = 2
}

create_navy_leader = {
	name = "Abu Ashraf"
	picture = "admirals/Portrait_Abu_Ashraf.dds"
	traits = { air_controller }
	skill = 2
}

create_navy_leader = {
	name = "Aurangzeb Chowdhury"
	picture = "admirals/Portrait_Chowdhury.dds"
	traits = { spotter }
	skill = 2
}

}