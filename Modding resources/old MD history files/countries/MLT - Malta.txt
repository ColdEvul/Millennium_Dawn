﻿capital = 116

oob = "MLT_2000"

set_convoys = 100
set_stability = 0.5

set_country_flag = country_language_english

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1



	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_steady
	commonwealth_of_nations_member
}

set_politics = {
	parties = {
		conservative = {
			popularity = 51.8
		}
		social_democrat = {
			popularity = 47.4
		}
		progressive = {
			popularity = 0.7	
		}
		social_liberal = {
			popularity = 0.1
		}
	}
	
	ruling_party = conservative
	last_election = "1998.9.5"
	election_frequency = 48
	elections_allowed = yes
}

create_country_leader = {
	name = "Edward Fenech Adami"
	picture = "Edward_Fenech_Adami.dds"
	ideology = christian_democrat
}

create_country_leader = {
	name = "Alfred Sant"
	picture = "Alfred_Sant.dds"
	ideology = social_democrat_ideology
}

create_country_leader = {
	name = "Henry Vassallo"
	picture = "Harry_Vassallo.dds"
	ideology = green
}

create_country_leader = {
	name = "Marlene Farrugia"
	picture = "Marlene_Farrugia.dds"
	ideology = centrist
}

create_country_leader = {
	name = "Josie Muscat"
	picture = "Josie_Muscat.dds"
	ideology = national_democrat
}

create_country_leader = {
	name = "Norman Lowell"
	picture = "Norman_Lowell.dds"
	ideology = fascist_ideology
}

create_country_leader = {
	name = "Victor Degiovanni"
	picture = "Victor_Degiovanni.dds"
	ideology = marxist
}

create_country_leader = {
	name = "Mohammed Elsadi"
	picture = "Mohammed_Elsadi.dds"
	ideology = islamic_republican
}

2016.6.1 = {

	set_politics = {
        last_election = "2013.3.9"
		ruling_party = social_democrat
		elections_allowed = yes
		parties = {
			social_liberal = {
				popularity = 0.8
			}
			conservative = {
				popularity = 28
			}
			progressive = {
				popularity = 2
			}
            social_democrat = {
				popularity = 40
			}
		}
	}
	
	create_country_leader = {
		name = "Simon Busuttil"
		picture = "Simon_Busuttil.dds"
		ideology = christian_democrat
	}

	create_country_leader = {
		name = "Joseph Muscat"
		picture = "Joseph_Muscat.dds"
		ideology = social_democrat_ideology
	}

	create_country_leader = {
		name = "Arnold Cassola"
		picture = "Arnold_Cassola.dds"
		ideology = green
	}

}

2004.1.1 = {
	add_ideas = {
		idea_eu_member
	}
}