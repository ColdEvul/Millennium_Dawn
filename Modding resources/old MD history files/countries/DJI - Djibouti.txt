﻿capital = 268

oob = "DJI_2000"

set_convoys = 70
set_stability = 0.5

set_country_flag = country_language_arabic
set_country_flag = country_language_french

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_explosion
	african_union_member
	arab_league_member
}

set_politics = {
	parties = {
		islamist = {
			popularity = 29
		}
		social_liberal = {
			popularity = 4
		}
		conservative = {
			popularity = 27
		}
		social_democrat = {
			popularity = 38
		}
		communist = {
			popularity = 2
		}
	}
	
	ruling_party = social_democrat
	last_election = "1999.12.4"
	election_frequency = 48
	elections_allowed = yes
}

create_country_leader = {
	name = "Ismail Omar Guelleh"
	picture = "Ismail_Omar_Guelleh.dds"
	ideology = social_democrat_ideology
}

create_country_leader = {
	name = "Daher Ahmed Farah"
	picture = "Daher_Ahmed_Farah.dds"
	ideology = right_wing_conservative
}

create_country_leader = {
	name = "Ahmed Dini Ahmed"
	picture = "Ahmed_D_Ahmed.dds"
	ideology = moderate
}

create_country_leader = {
	name = "Sheikh Abdourahman Barkhad"
	picture = "Abdourahman_Barkhad.dds"
	ideology = islamic_authoritarian
} 

create_country_leader = {
	name = "Council of Emirs"
	picture = "Council_of_Emirs.dds"
	ideology = absolute_monarchist
}

create_corps_commander = {    
	name = "Zakaria Cheikh Ibrahim"
	picture = "Zakaria_Cheikh_Ibrahim.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {    
	name = "Osman Nour Soubagleh" 
	picture = "Osman_Nour_Soubagleh.dds"
	traits = { trickster }
	skill = 2
}

create_corps_commander = { 
	name = "Daher Ali Mohamed"
	picture = "Daher_Mohamed.dds"
	traits = { commando ranger}
	skill = 1
}

create_corps_commander = { 
	name = "Ali Aden Houmed"
	picture = "Ali_Houmed.dds"
	traits = { desert_fox }
	skill = 1
}

create_navy_leader = { 
	name = "Abdourahman Aden Cher"
	picture = "Abdourahman_A_Cher.dds" 
	traits = { blockade_runner }
	skill = 1
}

2004.9.11 = {       #Ahmed Dini Ahmed is Dead!
	create_country_leader = {
	    name = "Mohamed Kadamy"
	    picture = "Mohamed_K_Kadamy.dds"
	    ideology = centrist
    }
}