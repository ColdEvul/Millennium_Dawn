capital = 924

oob = "AGF_2017"



# Starting tech
set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1
	infantry_weapons = 1
	infantry_weapons1 = 1
	
	
	
	command_control_equipment = 1
	
	land_Drone_equipment = 1
	
	Anti_tank_0 = 1
	
	Heavy_Anti_tank_0 = 1
	
	Anti_Air_0 = 1
	
	combat_eng_equipment = 1
	
	Early_APC = 1
	MBT_1 = 1
	
	IFV_1 = 1
	
	APC_1 = 1
	
	Air_APC_1 = 1
	
	
	Rec_tank_0 = 1
	
	util_vehicle_equipment_0 = 1
	
	SP_arty_equipment_0 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	
	SP_R_arty_equipment_0 = 1
	
	early_helicopter = 1
	
	SP_Anti_Air_0 = 1
	
	ENG_MBT_1 = 1
	
	early_fighter = 1
	
	L_Strike_fighter1 = 1
   
	early_bomber = 1
	
	night_vision_1 = 1
}


set_convoys = 5

2017.1.1 = {

add_ideas = {
	pop_050
	crippling_corruption
	christian
	gdp_1
	defence_08
	edu_01
	health_01
	social_01
	bureau_05
	police_04
	volunteer_army
	volunteer_women
	the_military
	farmers
	fossil_fuel_industry
	tribalism
}
set_country_flag = gdp_1

#Nat focus
	complete_national_focus = bonus_tech_slots

	declare_war_on = {
	target = SSU
	type = civil_war
	}
	set_politics = {

	parties = {
		democratic = { 
			popularity = 10
		}

		fascism = {
			popularity = 5
		}
		
		communism = {
			popularity = 10
			#banned = no #default is no
		}
		
		neutrality = { 
			popularity = 80
		}
	}
	
	ruling_party = neutrality
	last_election = "2013.7.9" #first election actually scheduled for 2018.7.9 so this didnt actually happen
	election_frequency = 60
	elections_allowed = yes
}

create_country_leader = {
	name = "Riek Machar"
	desc = "POLITICS_Mohamed Cheikh Biadillah_DESC" 
	picture = "Riek_Machar.dds"
	expire = "2050.1.1"
	ideology = neutral_Social
	traits = {
		#
	}
}

}