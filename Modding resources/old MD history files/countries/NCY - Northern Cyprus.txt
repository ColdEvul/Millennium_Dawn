﻿capital = 836

oob = "NCY_2000"

set_convoys = 15
set_stability = 0.5

set_country_flag = country_language_turkish

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	
}

add_ideas = {
	population_growth_steady
	partially_recognized_state
	limited_conscription
	closed_economy
}
add_opinion_modifier = { target = TUR modifier = rival }
set_politics = {

	parties = {
		reactionary = { #Rauf Denktaş, Independent
			popularity = 40
		}		
		conservative = { #UBP
			popularity = 30
		}
		social_liberal = { #CTP
			popularity = 10
		}
		social_democrat = { #TKP
			popularity = 12
		}
		nationalist = { #MAP
			popularity = 0
		}
		islamist = {
			popularity = 0
		}
		market_liberal = {
			popularity = 0
		}

		progressive = {
			popularity = 0
		}
		democratic_socialist = {
			popularity = 8
		}
		communist = {
			popularity = 0
		}
	}
	
	ruling_party = reactionary
	last_election = "1995.04.22"
	election_frequency = 60
	elections_allowed = yes
}

create_country_leader = {
	name = "Rauf Denktaş"
	picture = "Rauf_Denktas.dds"
	ideology = counter_progressive_democrat
}

	create_country_leader = {
		name = "Mustafa Akıncı"
		picture = "Mustafa_Akinci.dds"
		ideology = social_democrat_ideology
}

2016.1.1 = {
	set_politics = {
		parties = {
			islamist = {
				popularity = 0
			}
			nationalist = {
				popularity = 0
			}
			reactionary = {
				popularity = 0
			}
			conservative = {
				popularity = 27
			}
			market_liberal = {
				popularity = 23
			}
			social_liberal = {
				popularity = 38
			}
			social_democrat = {
				popularity = 7
			}
			progressive = {
				popularity = 3
			}
			democratic_socialist = {
				popularity = 0
			}
			communist = {
				popularity = 0
			}
		}
		ruling_party = social_democrat
		last_election = "2015.4.30"
		election_frequency = 60
		elections_allowed = yes
	}
}