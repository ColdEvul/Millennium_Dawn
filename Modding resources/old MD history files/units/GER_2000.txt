﻿division_template = {
	name = "Gebirgsbrigade"
	
	regiments = {
		L_Recce_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 }
		L_Inf_Bat = { x = 0 y = 2 }
		L_Inf_Bat = { x = 0 y = 3 }
		L_Engi_Bat = { x = 1 y = 0 }
	}
	support = {
	}
}
division_template = {
	name = "Panzerbrigade"

	regiments = {
		Mech_Recce_Bat = { x = 0 y = 0 }
		armor_Bat = { x = 0 y = 1 }
		Arm_Inf_Bat = { x = 0 y = 2 }
		Arm_Inf_Bat = { x = 0 y = 3 }
		Mech_Inf_Bat = { x = 0 y = 4 }
		H_Engi_Bat = { x = 1 y = 0 }
	}
	
	support = {
		SP_Arty_Battery = { x = 0 y = 0 }
	}
}

division_template = {
	name = "Panzergrenadierbrigade"

	regiments = {
		Mech_Recce_Bat = { x = 0 y = 0 }
		Arm_Inf_Bat = { x = 0 y = 1 }
		Mech_Inf_Bat = { x = 0 y = 2 }
		Mech_Inf_Bat = { x = 0 y = 3 }
		H_Engi_Bat = { x = 0 y = 4 }

	}
	support = {
		SP_Arty_Battery = { x = 0 y = 0 }
	}
}

division_template = {
	name = "Luftlandebrigade"

	regiments = {
		L_Air_Inf_Bat = { x = 0 y = 0 }
		L_Air_Inf_Bat = { x = 0 y = 1 }
		L_Recce_Bat = { x = 0 y = 2 }
		L_Engi_Bat = { x = 1 y = 0 }
	}
	support = {
	}
}

division_template = {
	name = "Kommandobataillon"

	regiments = {
		Special_Forces = { x = 0 y = 0 }
	}
	support = {
	}
	priority = 2
}

division_template = {
	name = "Deutsch-Französische Brigade"

	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 0 y = 1 }
		SP_Arty_Bat = { x = 0 y = 2 }
		H_Engi_Bat = { x = 0 y = 3 }
		
	}
}
units = {

	division = {
		name = "Panzerbrigade 21"
		location = 3355
		division_template = "Panzerbrigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	division = {
		name = "Panzerlehrbrigade 9"
		location = 526
		division_template = "Panzerbrigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	
	division = {
		name = "Panzergrenadierbrigade 13"
		location = 3535
		division_template = "Panzerbrigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	
	division = {
		name = "Panzergrenadierbrigade 41"
		location = 6542
		division_template = "Panzerbrigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	division = {
		name = "Panzerbrigade 12"
		location = 11544
		division_template = "Panzerbrigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	division = {
		name = "Gebirgsjägerbrigade 23"
		location = 11544
		division_template = "Gebirgsbrigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	
	division = {
		name = "Panzergrenadierbrigade 37"
		location = 692
		division_template = "Panzerbrigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	division = {
		name = "DSK"
		location = 6488
		division_template = "Fallschirmdivision"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	
	division = {
		name = "Dt.-Frz. Brigade"
		location = 6542
		division_template = "Panzerbrigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	
	division = {
		name = "Dt.-Nl. Korps"
		location = 11360
		division_template = "Panzergrenadierdivision"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	
	division = {
		name = "Eurokorps Deutsch"
		location = 11640
		division_template = "Panzergrenadierdivision"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	
	division = {
		name = "MNK Deutschland"
		location = 3340
		division_template = "Panzergrenadierdivision"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	
	division = {
		name = "Ausbildungskommando"
		location = 3535
		division_template = "Panzergrenadierdivision"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	
navy = {
		name = "1. Ubootgeschwader"
		base = 6389
		location = 6389
		ship = { name = "U-22" definition = diesel_attack_submarine experience = 75 equipment = { diesel_attack_submarine_2 = { amount = 1 owner = GER } } }
		ship = { name = "U-23" definition = diesel_attack_submarine experience = 75 equipment = { diesel_attack_submarine_2 = { amount = 1 owner = GER } } }
		ship = { name = "U-24" definition = diesel_attack_submarine experience = 75 equipment = { diesel_attack_submarine_2 = { amount = 1 owner = GER } } }
		ship = { name = "U-25" definition = diesel_attack_submarine experience = 75 equipment = { diesel_attack_submarine_2 = { amount = 1 owner = GER } } }
		ship = { name = "U-26" definition = diesel_attack_submarine experience = 75 equipment = { diesel_attack_submarine_2 = { amount = 1 owner = GER } } }
		ship = { name = "U-28" definition = diesel_attack_submarine experience = 75 equipment = { diesel_attack_submarine_2 = { amount = 1 owner = GER } } }
		ship = { name = "U-29" definition = diesel_attack_submarine experience = 75 equipment = { diesel_attack_submarine_2 = { amount = 1 owner = GER } } }
		ship = { name = "U-30" definition = diesel_attack_submarine experience = 75 equipment = { diesel_attack_submarine_2 = { amount = 1 owner = GER } } }
		ship = { name = "U-11" definition = diesel_attack_submarine experience = 75 equipment = { diesel_attack_submarine_2 = { amount = 1 owner = GER } } }
		ship = { name = "U-12" definition = diesel_attack_submarine experience = 75 equipment = { diesel_attack_submarine_2 = { amount = 1 owner = GER } } }
		ship = { name = "U-15" definition = diesel_attack_submarine experience = 75 equipment = { diesel_attack_submarine_2 = { amount = 1 owner = GER } } }
		ship = { name = "U-16" definition = diesel_attack_submarine experience = 75 equipment = { diesel_attack_submarine_2 = { amount = 1 owner = GER } } }
		ship = { name = "U-17" definition = diesel_attack_submarine experience = 75 equipment = { diesel_attack_submarine_2 = { amount = 1 owner = GER } } }
		ship = { name = "U-18" definition = diesel_attack_submarine experience = 75 equipment = { diesel_attack_submarine_2 = { amount = 1 owner = GER } } }
		ship = { name = "U-20" definition = diesel_attack_submarine experience = 75 equipment = { diesel_attack_submarine_2 = { amount = 1 owner = GER } } }
	}
	navy = {
		name = "2. Fregattengeschwader"
		base = 241
		location = 241
		ship = { name = "Brandenburg" definition = frigate experience = 75 equipment = { missile_frigate_2 = { amount = 1 owner = GER } } }
		ship = { name = "Schleswig-Holstein" definition = frigate experience = 75 equipment = { missile_frigate_2 = { amount = 1 owner = GER } } }
		ship = { name = "Bayern" definition = frigate experience = 75 equipment = { missile_frigate_2 = { amount = 1 owner = GER } } }
		ship = { name = "Mecklenburg-Vorpommern" definition = frigate experience = 75 equipment = { missile_frigate_2 = { amount = 1 owner = GER } } }
	}
	navy = {
		name = "4. Fregattengeschwader"
		base = 241
		location = 241
		ship = { name = "Bremen" definition = frigate experience = 75 equipment = { missile_frigate_4 = { amount = 1 owner = GER } } }
		ship = { name = "Niedersachsen" definition = frigate experience = 75 equipment = { missile_frigate_4 = { amount = 1 owner = GER } } }
		ship = { name = "Rheinland-Pfalz" definition = frigate experience = 75 equipment = { missile_frigate_4 = { amount = 1 owner = GER } } }
		ship = { name = "Emden" definition = frigate experience = 75 equipment = { missile_frigate_4 = { amount = 1 owner = GER } } }
		ship = { name = "Köln" definition = frigate experience = 75 equipment = { missile_frigate_4 = { amount = 1 owner = GER } } }
		ship = { name = "Karlsruhe" definition = frigate experience = 75 equipment = { missile_frigate_4 = { amount = 1 owner = GER } } }
		ship = { name = "Augsburg" definition = frigate experience = 75 equipment = { missile_frigate_4 = { amount = 1 owner = GER } } }
		ship = { name = "Lübeck" definition = frigate experience = 75 equipment = { missile_frigate_4 = { amount = 1 owner = GER } } }
	}	
	navy = {
		name = "1. Zerstörergeschwader"
		base = 6389
		location = 6389
		ship = { name = "Lübeck" definition = destroyer experience = 75 equipment = { destroyer_1 = { amount = 1 owner = GER creator = USA version_name = "Lütjens-class" } } }
		ship = { name = "Lübeck" definition = destroyer experience = 75 equipment = { destroyer_1 = { amount = 1 owner = GER creator = USA version_name = "Lütjens-class" } } }
	}
	
}

instant_effect = {
	
	add_equipment_to_stockpile = {
		type = MBT_Equipment_2 #Leopard 1 
		amount = 670
    }
	add_equipment_to_stockpile = {
		type = MBT_Equipment_4 #Leopard 2A4
		amount = 1728
    }
	add_equipment_to_stockpile = {
		type = Rec_tank_Equipment_0 #SPz 11-2 Kurz
		amount = 409
		#version_name = "SPz 11-2 Kurz"
    }
	add_equipment_to_stockpile = {
		type = Rec_tank_Equipment_0 #Tpz-1 Fuchs
		amount = 114
		#version_name = "TPz-1"
    }
	add_equipment_to_stockpile = {
		type = IFV_Equipment_4 #Marder 1 A3
		amount = 2122
    }
	add_equipment_to_stockpile = {
		type = APC_Equipment_3 #Tpz-1 Fuchs
		amount = 909
    }
	add_equipment_to_stockpile = {
		type = APC_Equipment_1 #M113 APC
		amount = 2167
		producer = USA
    }
	add_equipment_to_stockpile = {
		type = artillery_equipment_0 #M101
		amount = 118
		producer = USA
    }
	add_equipment_to_stockpile = {
		type = artillery_equipment_1 #FH-70
		amount = 196
    }
	add_equipment_to_stockpile = {
		type = SP_arty_equipment_1 #M109A3
		amount = 499
		#version_name = "M109A3"
		producer = USA
    }
	add_equipment_to_stockpile = {
		type = SP_arty_equipment_2 #Panzerhaubitze 2000
		amount = 165
    }
	add_equipment_to_stockpile = {
		type = SP_R_arty_equipment_0 #LARS 110
		amount = 50
    }
	add_equipment_to_stockpile = {
		type = SP_R_arty_equipment_1 #M270
		amount = 150
    }
	add_equipment_to_stockpile = {
		type = SP_AA_Equipment_0 #Flakpanzer Gepard
		amount = 143
    }
	add_equipment_to_stockpile = {
		type = attack_helicopter1 #MBB Bo 105
		amount = 199
    }
	add_equipment_to_stockpile = {
		type = transport_helicopter_equipment_1 #Bell UH-1 Iroquois
		amount = 188
		producer = USA
    }
	add_equipment_to_stockpile = {
		type = transport_helicopter_equipment_1 #Sikorsky CH-53 Sea Stallion
		amount = 107
		producer = USA
		#version_name = "Sikorsky CH-53 Sea Stallion"
    }
	add_equipment_to_stockpile = {
		type = transport_helicopter_equipment_1 #Bo 105
		amount = 60
    }
	add_equipment_to_stockpile = {
		type = transport_helicopter_equipment_1 #Aérospatiale SA-316
		amount = 28
		producer = FRA
    }
	add_equipment_to_stockpile = {
		type = transport_helicopter_equipment_3
		amount = 13
		producer = FRA
    }
	add_equipment_to_stockpile = {
		type = Strike_fighter_equipment_2 #Tornado
		amount = 311
    }
	add_equipment_to_stockpile = {
		type = nav_plane_equipment_2 #Atlantique 2
		amount = 16
		producer = FRA
    }
	add_equipment_to_stockpile = {
		type = transport_helicopter_equipment_2 #Westland WG-13 Lynx
		amount = 22
		producer = ENG
    }
	add_equipment_to_stockpile = {
		type = transport_helicopter_equipment_1 #Sea King HU5
		amount = 21
		version_name = "Sea King HU5"
		producer = USA
    }
	add_equipment_to_stockpile = {
		type = AS_Fighter_equipment_1 #F-4 Phantom II
		amount = 152
		producer = USA
    }
	add_equipment_to_stockpile = {
		type = AS_Fighter_equipment_3 #EF-2000 Typhoon
		amount = 8
    }
	add_equipment_to_stockpile = {
		type = MR_Fighter_equipment_1 #MiG-21 Fishbed
		amount = 1
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = AS_Fighter_equipment_1 #MiG-23 Flogger
		amount = 2
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = Strike_fighter_equipment_1 #Su-22 Fitter
		amount = 1
		producer = SOV
		version_name = "Su-22 Fitter"
    }
	add_equipment_to_stockpile = {
		type = transport_plane_equipment_1 #C-160R Transall
		amount = 83
		producer = FRA
    }	
	
}
